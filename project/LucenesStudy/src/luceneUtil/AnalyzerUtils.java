/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luceneUtil;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.AttributeSource.State;

/**
 *
 * @author Administrator
 */
public class AnalyzerUtils {

    public static AttributeSource[] tokensFromAnalysis(Analyzer analyzer,
            String text) throws IOException {
        TokenStream stream = //1
                analyzer.tokenStream("contents", new StringReader(text)); //1
        ArrayList<AttributeSource> tokenList = new ArrayList<AttributeSource>();
        while (true) {
            if (!stream.incrementToken()) {
                break;
            }
            tokenList.add(stream.cloneAttributes());
        }
        return (AttributeSource[]) tokenList.toArray(new AttributeSource[0]);
    }

    public static AttributeSource[] tokensFromAnalysisState(Analyzer analyzer,
            String text) throws IOException {
        TokenStream stream = //1
                analyzer.tokenStream("contents", new StringReader(text)); //1
        ArrayList<AttributeSource.State> stateList = new ArrayList<State>();
        while (true) {
            if (!stream.incrementToken()) {
                break;
            }
            stateList.add(stream.captureState());
        }
        List<AttributeSource> tokenList = new ArrayList<AttributeSource>();

        for (AttributeSource.State state : stateList) {
            AttributeSource as = new AttributeSource();
            as.restoreState(state);
            tokenList.add(as);
        }
        return (AttributeSource[]) tokenList.toArray(new AttributeSource[0]);
    }

    public static void displayTokens(Analyzer analyzer,
            String text) throws IOException {
        AttributeSource[] tokens = tokensFromAnalysis(analyzer, text);
        for (int i = 0; i < tokens.length; i++) {
            AttributeSource token = tokens[i];
            TermAttribute term = token.addAttribute(TermAttribute.class);
            System.out.print("[" + term.term() + "] "); //2
        }
    }

    public static void displayTokensWithFullDetails(Analyzer analyzer,
            String text) throws IOException {
        AttributeSource[] tokens = tokensFromAnalysisState(analyzer, text);
        int position = 0;
        for (int i = 0; i < tokens.length; i++) {
            AttributeSource token = tokens[i];
            TermAttribute term = token.addAttribute(TermAttribute.class);
            PositionIncrementAttribute posIncr = token.addAttribute(PositionIncrementAttribute.class);
            OffsetAttribute offset = token.addAttribute(OffsetAttribute.class);
            TypeAttribute type = token.addAttribute(TypeAttribute.class);
            int increment = posIncr.getPositionIncrement();
            if (increment > 0) {
                position = position + increment;
                System.out.println();
                System.out.print(position + ": ");
            }
            System.out.print("[" +
                    term.term() + ":" +
                    offset.startOffset() + "->" +
                    offset.endOffset() + ":" +
                    type.type() + "] ");
        }
        System.out.println();
    }

    public static void displayTokensWithFullDetails2(Analyzer analyzer,
            String text) throws IOException {
        AttributeSource[] tokens = tokensFromAnalysis(analyzer, text);
        int position = 0;
        for (int i = 0; i < tokens.length; i++) {
            AttributeSource token = tokens[i];
            TermAttribute term = token.addAttribute(TermAttribute.class);
            PositionIncrementAttribute posIncr = token.addAttribute(PositionIncrementAttribute.class);
            OffsetAttribute offset = token.addAttribute(OffsetAttribute.class);
            TypeAttribute type = token.addAttribute(TypeAttribute.class);
            int increment = posIncr.getPositionIncrement();
            if (increment > 0) {
                position = position + increment;
                System.out.println();
                System.out.print(position + ": ");
            }
            System.out.print("[" +
                    term.term() + ":" +
                    offset.startOffset() + "->" +
                    offset.endOffset() + ":" +
                    type.type() + "] ");
        }
        System.out.println();
    }

	public static String getTerm(AttributeSource attributeSource) {
		TermAttribute term = attributeSource.addAttribute(TermAttribute.class);
		return term.term();
	}
}
