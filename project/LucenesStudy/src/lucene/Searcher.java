package lucene;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.MultiTermQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class Searcher {

	private IndexSearcher indexSearcher;
	private Directory directory = null;

	public Searcher() {
		directory = Lucenes.getFSDirectory(Indexer.INDEX_PATH);
		try {
			indexSearcher = new IndexSearcher(directory);
		} catch (CorruptIndexException ex) {
			Logger.getLogger(Searcher.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(Searcher.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public Searcher(IndexReader indexReader) {
		indexSearcher = new IndexSearcher(indexReader);
	}

	public IndexReader getIndexReader() {
		return indexSearcher.getIndexReader();
	}

	public TopDocs searchByPath(String path) throws IOException {
		Query query = new TermQuery(new Term("path", path));
		return indexSearcher.search(query, 1000);
	}

	public TopDocs search(String name, String value) throws IOException {
		Query query = new TermQuery(new Term(name, value));
		return indexSearcher.search(query, 1);
	}

	public TopDocs searchByContent(String content, Query query)
			throws IOException {
		query = new TermQuery(new Term("content", content));
		return indexSearcher.search(query, 1000);
	}

	public TopDocs searchByYear(int year) throws IOException {
		Query query = new PrefixQuery(new Term("date", String.valueOf(year)));
		return indexSearcher.search(query, 100);
	}

	public Document doc(int doc) throws CorruptIndexException, IOException {
		return indexSearcher.doc(doc);
	}

	public Explanation explain(Query query, int doc) throws IOException {
		return indexSearcher.explain(query, doc);
	}

	public TopDocs executeQuery(Query query, int top) throws IOException {
		return indexSearcher.search(query, top);
	}

	public TopDocs executeQuery(Query query, Filter filter, int top)
			throws IOException {
		return indexSearcher.search(query, filter, top);
	}

	public TopDocs executeQuery(Query query, Filter filter, int top, Sort sort)
			throws IOException {
		return indexSearcher.search(query, filter, top, sort);
	}

	public static Query createQuery(String fieldName, String content) {
		return new TermQuery(new Term(fieldName, content));
	}

	public static Query createTermRangeQuery(String fieldName,
			String lowerKeyWord, String upperKeyWord) {
		TermRangeQuery query = null;
		query = new TermRangeQuery(fieldName, lowerKeyWord, upperKeyWord, true,
				true);
		System.out.println(query.getRewriteMethod());
		query
				.setRewriteMethod(MultiTermQuery.CONSTANT_SCORE_AUTO_REWRITE_DEFAULT);
		System.out.println(query.getRewriteMethod());
		return query;
	}

	public static Query createQuery(Query[] queries,
			BooleanClause.Occur[] occurs) {
		if (queries.length != occurs.length) {
			throw new IllegalArgumentException();
		}
		BooleanQuery query = new BooleanQuery();
		int length = queries.length;
		for (int i = 0; i < length; i++) {
			query.add(queries[i], occurs[i]);
		}
		return query;
	}

	public static Query createParsedQuery(String field, String word)
			throws ParseException {
		QueryParser parser = new QueryParser(Version.LUCENE_30, field, Lucenes
				.getStandardAnalyzer());
		return parser.parse(word);
	}

	public static Query createMultiPhraseQuery(String[] fieldName,
			String[] keyWords) {
		MultiPhraseQuery query = new MultiPhraseQuery();
		query.setSlop(100);
		int size = fieldName.length;
		for (int i = 0; i < size; i++) {
			query.add(new Term(fieldName[i], keyWords[i]));
		}
		return query;
	}

	public static QueryParser createMultiFieldQueryParser(String[] fields) {
		MultiFieldQueryParser mfQueryParser = new MultiFieldQueryParser(
				Version.LUCENE_30, fields, Lucenes.getStandardAnalyzer());
		return mfQueryParser;
	}

	public void close() throws IOException {
		indexSearcher.close();
	}
}
