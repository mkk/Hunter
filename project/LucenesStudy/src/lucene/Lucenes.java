package lucene;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexDeletionPolicy;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class Lucenes {

	public static Directory getFSDirectory(String path) {
		Directory directory = null;
		File indexFile = new File(path);
		try {
			directory = FSDirectory.open(indexFile);
		} catch (IOException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		return directory;
	}

	public static IndexDeletionPolicy getMyIndexDeletionPolicy() {
		return new KeepOnlyLastTwoCommitDeletionPolicy();
	}

	public static Analyzer getStandardAnalyzer() {
		return new StandardAnalyzer(Version.LUCENE_30);
	}

	public static void showTopDocs(TopDocs topDocs, IndexReader reader) throws CorruptIndexException, IOException {
		System.out.println("hits: "+topDocs.totalHits);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = reader.document(scoreDoc.doc);
			System.out.println(doc.getField("path").stringValue());
			System.out.println(" "+doc.getField("title").stringValue());
			System.out.println(" "+doc.getField("date").stringValue());
			System.out.println(" "+doc.getField("postfix").stringValue());
			//System.out.println(" "+doc.getField("totalSpace").stringValue());
			//System.out.println(" "+doc.getField("sequence").stringValue());
			
			System.out.println();
		}
	}
	public static void showTopDocsWithExplanation(TopDocs topDocs, Query query,Searcher searcher) throws CorruptIndexException, IOException {
		System.out.println("hits: "+topDocs.totalHits);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println(doc.getField("path").stringValue());
			System.out.println(" "+doc.getField("title").stringValue());
			System.out.println(" "+doc.getField("date").stringValue());
			System.out.println(" "+doc.getField("postfix").stringValue());
			System.out.println(" "+doc.getField("totalSpace").stringValue());
			System.out.println(" "+doc.getField("sequence").stringValue());
			System.out.println("==explanation==");
			Explanation explanation=searcher.explain(query, scoreDoc.doc);
			System.out.println(explanation);
			System.out.println();
		}
	}
}
