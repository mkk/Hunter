package lucene;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.DateTools.Resolution;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexDeletionPolicy;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class Indexer {

	public static final String INDEX_PATH = "E:\\index";
	IndexWriter indexWriter = null;
	Directory directory = null;
	static int count = 0;

	public Indexer(boolean create) {

		directory = Lucenes.getFSDirectory(INDEX_PATH);

		try {
			IndexWriter.setDefaultInfoStream(System.out);
			indexWriter = new IndexWriter(directory, new StandardAnalyzer(
					Version.LUCENE_30), create,
					IndexWriter.MaxFieldLength.UNLIMITED);
			// indexWriter.setInfoStream(System.out);
			indexWriter.setUseCompoundFile(false);
		} catch (CorruptIndexException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (LockObtainFailedException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public Indexer(Directory directory, boolean create) {

		this.directory = directory;

		try {
			IndexWriter.setDefaultInfoStream(System.out);
			indexWriter = new IndexWriter(this.directory, new StandardAnalyzer(
					Version.LUCENE_30), create,
					IndexWriter.MaxFieldLength.UNLIMITED);
			// indexWriter.setInfoStream(System.out);
			indexWriter.setUseCompoundFile(false);
		} catch (CorruptIndexException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (LockObtainFailedException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public Indexer(Directory directory, boolean create,
			IndexDeletionPolicy deletionPolicy) {

		this.directory = directory;

		try {
			IndexWriter.setDefaultInfoStream(System.out);
			indexWriter = new IndexWriter(this.directory, new StandardAnalyzer(
					Version.LUCENE_30), create, deletionPolicy,
					IndexWriter.MaxFieldLength.UNLIMITED);
			// indexWriter.setInfoStream(System.out);
			indexWriter.setUseCompoundFile(false);
		} catch (CorruptIndexException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (LockObtainFailedException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public void addDocument(File file) throws FileNotFoundException,
			CorruptIndexException, IOException {
		Field title = new Field("title", file.getName(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field titleTokenized = new Field("title2", file.getName(),
				Field.Store.YES, Field.Index.ANALYZED,
				Field.TermVector.WITH_POSITIONS_OFFSETS);
		Field path = new Field("path", file.getAbsolutePath(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field date = new Field("date", DateTools.timeToString(file
				.lastModified(), Resolution.MINUTE), Field.Store.YES,
				Field.Index.NOT_ANALYZED);

		Field totalSpace = new Field("totalSpace", String.valueOf(file
				.getTotalSpace()), Field.Store.YES, Field.Index.NOT_ANALYZED);
		Field sequence = new Field("sequence", String.valueOf(count++),
				Field.Store.YES, Field.Index.NOT_ANALYZED);
		String post = "";
		int index = file.getName().lastIndexOf(".");
		if (index != -1) {
			post = file.getName().substring(index + 1);
		}
		Field postfix = new Field("postfix", post, Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field content = null;
		if (post.equals("txt")) {

			FileReader fileReader = new FileReader(file);
			char[] buffer = new char[1024];
			StringBuilder builder = new StringBuilder();
			for (int i = fileReader.read(buffer); i != -1; i = fileReader
					.read(buffer)) {
				builder.append(buffer, 0, i);
			}
			content = new Field("content", fileReader,
					Field.TermVector.WITH_POSITIONS_OFFSETS);
		} else {
			content = new Field("content", "", Field.Store.NO,
					Field.Index.ANALYZED);
		}
		Document doc = new Document();
		doc.add(title);
		doc.add(titleTokenized);
		doc.add(content);
		doc.add(path);
		doc.add(date);
		doc.add(postfix);
		doc.add(totalSpace);
		doc.add(sequence);
		indexWriter.addDocument(doc);
	}

	public void addDocumentWithoutContent(File file)
			throws FileNotFoundException, CorruptIndexException, IOException {
		Field title = new Field("title", file.getName(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field path = new Field("path", file.getAbsolutePath(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field date = new Field("date", DateTools.timeToString(file
				.lastModified(), Resolution.MINUTE), Field.Store.YES,
				Field.Index.ANALYZED);
		String post = "";
		int index = file.getName().lastIndexOf(".");
		if (index != -1) {
			post = file.getName().substring(index + 1);
		}
		Field postfix = new Field("postfix", post, Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		Field content = null;

		Document doc = new Document();
		doc.add(title);
		doc.add(path);
		doc.add(date);
		doc.add(postfix);
		indexWriter.addDocument(doc);
	}

	public void deleteByPath(String path) throws CorruptIndexException,
			IOException {
		Term term = new Term("path", path);
		indexWriter.deleteDocuments(term);
	}

	public void updateDocument(String path, String newTitle, String newPath,
			String newContent) throws CorruptIndexException, IOException {
		Document document = new Document();
		document.add(new Field("title", newTitle, Field.Store.YES,
				Field.Index.NOT_ANALYZED));
		document.add(new Field("path", newPath, Field.Store.YES,
				Field.Index.NOT_ANALYZED));
		document
				.add(new Field("date", newPath, Field.Store.YES, Field.Index.NO));
		document.add(new Field("content", newPath, Field.Store.NO,
				Field.Index.ANALYZED, Field.TermVector.NO));
		indexWriter.updateDocument(new Term("path", path), document);
	}

	public void commit() throws CorruptIndexException, IOException {
		indexWriter.commit();
	}

	public void commit(Map<String, String> commitUserData)
			throws CorruptIndexException, IOException {
		indexWriter.commit(commitUserData);
	}

	public void optimize() throws CorruptIndexException, IOException {
		indexWriter.optimize();
	}

	public void setUseCompoundFile(boolean value) {
		indexWriter.setUseCompoundFile(value);
	}

	public int getMaxDoc() {
		return indexWriter.maxDoc();
	}

	public int getNumDoc() throws IOException {
		return indexWriter.numDocs();
	}

	public boolean hasDeletions() throws IOException {
		return indexWriter.hasDeletions();
	}

	public void close() throws CorruptIndexException, IOException {
		System.out.println(indexWriter.numDocs());
		indexWriter.close();
	}
}
