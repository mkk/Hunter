/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package extend;

import java.io.IOException;
import java.util.List;
import org.apache.lucene.index.IndexCommit;
import org.apache.lucene.index.IndexDeletionPolicy;

/**
 *
 * @author Administrator
 */
public class KeepOnlyLastTwoCommitDeletionPolicy implements IndexDeletionPolicy {

    @Override
    public void onInit(List<? extends IndexCommit> commits) throws IOException {
        onCommit(commits);
    }

    @Override
    public void onCommit(List<? extends IndexCommit> commits) throws IOException {
        int size = commits.size();
        for (int i = 0; i < size - 2; i++) {
            commits.get(i).delete();
        }
    }
}
