package test.spellChecker;

import java.io.File;
import java.io.IOException;

import lucene.Lucenes;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;

public class TestSpellChecker {
	public static void main(String[] args) throws IOException {
		SpellChecker spellchecker = new SpellChecker(Lucenes
				.getFSDirectory("E:\\index"));

		// To index a field of a user index:
		spellchecker.indexDictionary(new LuceneDictionary(IndexReader
				.open(Lucenes.getFSDirectory("E:\\index")), "content"));
		// To index a file containing words:
		//spellchecker.indexDictionary(new PlainTextDictionary(new File(
		//		"myfile.txt")));
		String[] suggestions = spellchecker.suggestSimilar("the", 200);
		for (String s : suggestions) {
			System.out.println(s);
		}
	}
}
