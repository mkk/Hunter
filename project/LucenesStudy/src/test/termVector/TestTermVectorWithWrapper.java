package test.termVector;

import java.io.IOException;
import java.util.Comparator;
import java.util.SortedSet;

import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.FieldSortedTermVectorMapper;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermVectorEntry;

public class TestTermVectorWithWrapper {
	public static void main(String[] args) throws CorruptIndexException,
			IOException {
		IndexReader reader = IndexReader.open(Lucenes
				.getFSDirectory("E:\\index"));

		FieldSortedTermVectorMapper fsTermVector = new FieldSortedTermVectorMapper(
				new FrequencyComparator());
		reader.getTermFreqVector(5, fsTermVector);
		SortedSet<TermVectorEntry> ss = fsTermVector.getFieldToTerms().get(
				"content");
		for (TermVectorEntry term : ss) {
			System.out.print("term: [" + term.getTerm());
			System.out.println("] Frequence: " + term.getFrequency());

		}
	}

	public static class FrequencyComparator implements
			Comparator<TermVectorEntry> {

		@Override
		public int compare(TermVectorEntry o1, TermVectorEntry o2) {
			System.out.println(o1.getTerm());
			System.out.println(o2.getTerm());

			return -(o1.getFrequency() - o2.getFrequency());
		}

	}
}
