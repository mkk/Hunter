package test.termVector;

import java.io.IOException;

import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermFreqVector;

public class TestTermVector {
	public static void main(String[] args) throws CorruptIndexException,
			IOException {
		IndexReader indexReader = IndexReader.open(Lucenes
				.getFSDirectory("E:\\index"));

		TermFreqVector termFreqVector = indexReader.getTermFreqVector(2,
				"content");
		String fieldName = termFreqVector.getField();
		System.out.println("fileName: " + fieldName);
		int size = termFreqVector.size();
		String[] terms = termFreqVector.getTerms();
		int[] freqs = termFreqVector.getTermFrequencies();
		for (int i = 0; i < size; i++) {
			System.out.print("term: [" + terms[i]);
			System.out.println("] Frequence: " + freqs[i]);

		}
	}
}
