package test;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.index.CorruptIndexException;
import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class ReindexWithCommit {

	public static void main(String[] args) {
		Indexer indexer = new Indexer(Lucenes.getFSDirectory("E:\\index"),
				true, new KeepOnlyLastTwoCommitDeletionPolicy());
		File file = new File("D:\\learn\\Born to Win");

		Reindex.add(file.listFiles(), indexer);

		Map<String, String> commitUserData = new HashMap();
		commitUserData.put("myCommit", "reindex");
		try {
			System.out.println("commit");
			indexer.commit(commitUserData);
			System.out.println("close");
			indexer.close();
		} catch (CorruptIndexException ex) {
			Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}
}
