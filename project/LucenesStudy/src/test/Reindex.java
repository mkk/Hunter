package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class Reindex {

    public static void main(String[] args) {

        Indexer indexer = new Indexer(true);
        File file = new File("D:\\learn\\Born to Win");

        add(file.listFiles(), indexer);
        try {
            indexer.close();
        } catch (CorruptIndexException ex) {
            Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public static void add(File[] files, Indexer indexWriter) {
        for (File file : files) {
            if (file.isFile()) {
                try {
                    indexWriter.addDocument(file);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null, ex);
                } catch (CorruptIndexException ex) {
                    Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Reindex.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                File[] children = file.listFiles();
                if (children != null) {
                    add(children, indexWriter);
                }
            }
        }
    }
}
