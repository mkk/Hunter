package test;

import java.io.IOException;

import lucene.Indexer;
import lucene.Lucenes;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class CompoundIndexFiles {

    public static void main(String[] args) throws IOException {
        Indexer indexer = new Indexer(Lucenes.getFSDirectory("E:\\index"), false, Lucenes.getMyIndexDeletionPolicy());
        indexer.setUseCompoundFile(true);
        indexer.optimize();
        indexer.close();
    }
}
