package test;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.lucene.index.IndexCommit;
import org.apache.lucene.index.IndexReader;
import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class LookCommit {

    public static void main(String[] args) throws IOException {
//       IndexReader indexReader=IndexReader.open(Lucenes.getFSDirectory("E:\\index"));
        Collection<IndexCommit> commits = IndexReader.listCommits(Lucenes.getFSDirectory("E:\\index"));
        for (IndexCommit commit : commits) {
            System.out.println(commit);
            System.out.println("isDeleted: " + commit.isDeleted());
            System.out.println("isOptimized: " + commit.isOptimized());
            System.out.println("getSegmentsFileName: " + commit.getSegmentsFileName());
            System.out.println("getGeneration: " + commit.getGeneration());
            System.out.println("getTimestamp: " + commit.getTimestamp());
            System.out.println("getVersion: " + commit.getVersion());
            Map userData = commit.getUserData();

            Set<Entry<String, String>> entries = userData.entrySet();
            for (Entry entry : entries) {
                System.out.println("key: " + entry.getKey());
                System.out.println("value: " + entry.getValue());
            }
        }
    }
}
