package test;

import java.io.IOException;
import java.util.Collection;

import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexCommit;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class BuildSearchWithIndexCommit {

    public static void main(String[] args) throws IOException {
        Collection<IndexCommit> commits = IndexReader.listCommits(Lucenes.getFSDirectory("E:\\index"));
        for (IndexCommit commit : commits) {
            System.out.println(commit.getSegmentsFileName());
            IndexReader indexReader = IndexReader.open(commit, Lucenes.getMyIndexDeletionPolicy(), true);
            Searcher searcher = new Searcher(indexReader);
            TopDocs topDocs = searcher.searchByPath("D:\\learn\\Born to Win\\Ŀ¼.txt");
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document doc = searcher.doc(scoreDoc.doc);
                System.out.println(doc.getField("title").stringValue());
                System.out.println(doc.getField("path").stringValue());
                System.out.println(doc.getField("date").stringValue());
//                System.out.println(doc.getField("content").stringValue());
            }
        }
    }
}
