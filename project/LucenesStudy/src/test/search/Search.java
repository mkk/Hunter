package test.search;

import java.io.IOException;
import java.io.Reader;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class Search {

    public static void main(String[] args) throws IOException {

        Searcher searcher = new Searcher();

        TopDocs topDocs = searcher.searchByPath("D:\\learn\\Born to Win\\Ŀ¼.txt");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = searcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
            System.out.println(doc.getField("postfix").stringValue());
            /*Field field=doc.getField("content");
            String reader=field.stringValue();
            System.out.println(reader);*/
        }
        searcher.close();
    }
}
