package test.search.customScore;

import java.io.IOException;

import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.function.CustomScoreQuery;
import org.apache.lucene.search.function.FieldScoreQuery;

public class CustomScoreSearch {
	public static void main(String[] args) throws CorruptIndexException, IOException {
		FieldScoreQuery qf = new FieldScoreQuery("score",
				FieldScoreQuery.Type.BYTE);
		Query term = new TermQuery(new Term("content", "good"));

		CustomScoreQuery cq = new CustomScoreQuery(term, qf);
		Searcher searcher = new Searcher();
		Lucenes.showTopDocsWithExplanation(searcher.executeQuery(term, 100), term,searcher);
		Lucenes.showTopDocsWithExplanation(searcher.executeQuery(cq, 100), cq,searcher);

	}
}
