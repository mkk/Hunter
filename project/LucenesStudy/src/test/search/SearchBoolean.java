package test.search;

import java.io.IOException;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.BooleanClause.Occur;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class SearchBoolean {

	public static void main(String[] args) throws IOException, ParseException {
		Searcher searcher = new Searcher();

		Query query1 = Searcher.createQuery("content", "happy");
		Query query2 = Searcher.createQuery("content", "good");
		Query query = Searcher.createQuery(new Query[] { query1, query2 },
				new Occur[] { Occur.MUST, Occur.MUST });
		TopDocs topDocs = searcher.executeQuery(query, 1000);
		System.out.println("hits: " + topDocs.scoreDocs.length);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println(doc.getField("title").stringValue());
			System.out.println(doc.getField("path").stringValue());
			System.out.println(doc.getField("date").stringValue());
			Explanation explanation = searcher.explain(query, scoreDoc.doc);
			System.out.println(explanation);
		}
		searcher.close();
	}
}
