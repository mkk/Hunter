package test.search;

import java.io.IOException;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

public class SearchMultiFieldQueryParser {
	public static void main(String[] args) throws ParseException, IOException {
		Searcher searcher = new Searcher();
		QueryParser queryparser = Searcher
				.createMultiFieldQueryParser(new String[] { "content", "title" });
		Query query = queryparser.parse("happy");
		TopDocs topDocs = searcher.executeQuery(query, 1000);
		showTopDocs(topDocs, searcher);
	}

	public static void showTopDocs(TopDocs topDocs, Searcher searcher)
			throws CorruptIndexException, IOException {
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println(doc.getField("title").stringValue());
			System.out.println(doc.getField("path").stringValue());
			System.out.println(doc.getField("date").stringValue());
			// System.out.println(doc.getField("content").stringValue());
		}
	}
}
