package test.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import lucene.Indexer;
import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class SearchNoIndex {

    public static void main(String[] args) {
        Searcher searcher = new Searcher();

        try {
            TopDocs topDocs = searcher.search("date", "1231772144000");
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document doc = searcher.doc(scoreDoc.doc);
                System.out.println(doc.getField("title").stringValue());
                System.out.println(doc.getField("path").stringValue());
                System.out.println(doc.getField("date").stringValue());
//                System.out.println(doc.getField("content").stringValue());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
