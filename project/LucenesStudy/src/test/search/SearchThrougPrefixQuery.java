package test.search;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class SearchThrougPrefixQuery {

    public static void main(String[] args) {
        Searcher searcher = new Searcher();
        try {
            TopDocs topDocs = searcher.searchByYear(2010);
            System.out.println("num: "+topDocs.totalHits);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document doc = searcher.doc(scoreDoc.doc);
                System.out.println(doc.getField("title").stringValue());
                System.out.println(doc.getField("path").stringValue());
                System.out.println(doc.getField("date").stringValue());
//                System.out.println(doc.getField("content").stringValue());
            }
        } catch (IOException ex) {
            Logger.getLogger(SearchThrougPrefixQuery.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
