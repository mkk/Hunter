package test.search;

import java.io.IOException;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

public class MultiPhraseQueryTest {
	public static void main(String[] args) throws IOException {
		Searcher searcher = new Searcher();
		String fieldName = "content";
		String[] keyWords = new String[] { "good", "happy" ,"will"};
		Query query = Searcher.createMultiPhraseQuery(new String[] { fieldName,
				fieldName }, keyWords);
		MultiPhraseQuery mpQuery=new MultiPhraseQuery();
		mpQuery.add(new Term[] {new Term("content","good"),new Term("content","happy")});
		mpQuery.add(new Term("content","will"));
		mpQuery.setSlop(100);
		System.out.println(mpQuery);
        TopDocs topDocs=searcher.executeQuery(query, 1000);
        showTopDocs(topDocs, searcher);
	}

	public static void showTopDocs(TopDocs topDocs, Searcher searcher)
			throws CorruptIndexException, IOException {
		System.out.println("hits: " + topDocs.scoreDocs.length);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			System.out.println(scoreDoc.doc);
			Document doc = searcher.doc(scoreDoc.doc);
			System.out.println(doc.getField("title").stringValue());
			// System.out.println(doc.getField("path").stringValue());
			// System.out.println(doc.getField("date").stringValue());
		}

	}
}
