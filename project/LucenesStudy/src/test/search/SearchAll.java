package test.search;

import java.io.IOException;

import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

public class SearchAll {
	public static void main(String[] args) throws CorruptIndexException,
			IOException {
		Query query = new TermQuery(new Term("postfix", "txt"));
		Searcher searcher = new Searcher();
		Lucenes.showTopDocs(searcher.executeQuery(query, 100), IndexReader
				.open(Lucenes.getFSDirectory("E:\\index")));
	}
}
