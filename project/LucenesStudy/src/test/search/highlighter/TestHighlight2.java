package test.search.highlighter;

import java.io.IOException;

import lucene.Searcher;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermFreqVector;
import org.apache.lucene.index.TermPositionVector;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Encoder;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLEncoder;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenSources;

public class TestHighlight2 {
	public static void main(String[] args) throws CorruptIndexException,
			IOException, InvalidTokenOffsetsException {

		Searcher searcher = new Searcher();
		IndexReader indexReader = searcher.getIndexReader();
		Query query = searcher.createQuery("content", "happy");

		TopDocs topDocs = searcher.executeQuery(query, 100);

		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			
				Document doc=searcher.doc(scoreDoc.doc);
			String path = doc.get("path");
			System.out.println(path);
			TermFreqVector tpv = indexReader.getTermFreqVector(scoreDoc.doc,
					"content");
			TokenStream tokenStream = TokenSources
					.getTokenStream((TermPositionVector) tpv);

			QueryScorer queryScorer = new QueryScorer(query);
			Fragmenter fragmenter = new SimpleSpanFragmenter(queryScorer, 50);
			Formatter formatter = new SimpleHTMLFormatter();
			Encoder encoder = new SimpleHTMLEncoder();

			/*
			 * Highlighter highlighter = new Highlighter(formatter, encoder,
			 * queryScorer);
			 */
			Highlighter highlighter = new Highlighter(queryScorer);
			highlighter.setTextFragmenter(fragmenter);

			
			String text = doc.get("content");

			String highlight = highlighter.getBestFragment(tokenStream, text);
			System.out.println(highlight);
		}
	}
}
