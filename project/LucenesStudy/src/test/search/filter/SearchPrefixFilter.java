package test.search.filter;

import java.io.IOException;

import lucene.Lucenes;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.PrefixFilter;

public class SearchPrefixFilter {
public static void main(String[] args) throws CorruptIndexException, IOException {
	IndexReader reader=IndexReader.open(Lucenes.getFSDirectory("E:\\index"));
	Filter filter=new PrefixFilter(new Term("date","2010"));
	DocIdSet docs=filter.getDocIdSet(reader);
	for(DocIdSetIterator it=docs.iterator();it.nextDoc()!=DocIdSetIterator.NO_MORE_DOCS;) {
		int id=it.docID();
		Document doc=reader.document(id);
		System.out.println(doc.get("title"));
	}
}
}
