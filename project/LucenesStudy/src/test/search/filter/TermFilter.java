package test.search.filter;

import java.io.IOException;

import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.FieldCacheTermsFilter;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermsFilter;
import org.apache.lucene.search.TopDocs;

public class TermFilter {
	public static void main(String[] args) throws ParseException, IOException {
//		testFieldCacheTermsFieter();
		testTermsFilter();
	}
	
	public static void testFieldCacheTermsFieter() throws ParseException, IOException {
		Filter filter = new FieldCacheTermsFilter("title", new String[] {
				"Be Happy.txt", "Giving Life Meaning.txt" });
		Query query = Searcher.createQuery("content", "happy");
		Searcher searcher = new Searcher();
		TopDocs topDocs = searcher.executeQuery(query, filter, 100);
		Lucenes.showTopDocs(topDocs, IndexReader.open(Lucenes.getFSDirectory("E:\\index")));
	}
	
	public static void testTermsFilter() throws ParseException, IOException {
		IndexReader reader=IndexReader.open(Lucenes.getFSDirectory("E:\\index"));
		TermsFilter tf=new TermsFilter();
		tf.addTerm(new Term("title2","life"));
		tf.addTerm(new Term("title2","good"));
		Filter filter =tf;
		DocIdSet docs=filter.getDocIdSet(reader);
		for(DocIdSetIterator it=docs.iterator();it.nextDoc()!=DocIdSetIterator.NO_MORE_DOCS;) {
			int id=it.docID();
			Document doc=reader.document(id);
			System.out.println(doc.get("title"));
		}
		System.out.println();
		
		Query query=Searcher.createQuery("content", "good");
		
		Searcher searcher=new Searcher();
		TopDocs topDocs=searcher.executeQuery(query, filter, 100);
		Lucenes.showTopDocs(topDocs,reader );
	}
}
