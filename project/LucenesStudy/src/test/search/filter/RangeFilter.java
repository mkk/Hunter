package test.search.filter;

import java.io.IOException;

import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermRangeFilter;
import org.apache.lucene.search.TopDocs;

public class RangeFilter {
	public static void main(String[] args) throws IOException {
		IndexReader reader=IndexReader.open(Lucenes.getFSDirectory("E:\\index"));
		Filter filter=new TermRangeFilter("date","200901","201010",true,true);
		Searcher searcher=new Searcher();
		Query query=Searcher.createTermRangeQuery("date", "200901", "200910");
		TopDocs topDocs=searcher.executeQuery(query, filter, 100);
		Lucenes.showTopDocs(topDocs, reader);
	}
}
