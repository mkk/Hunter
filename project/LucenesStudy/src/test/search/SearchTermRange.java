package test.search;

import java.io.IOException;

import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class SearchTermRange {

    public static void main(String[] args) throws IOException {
        Searcher searcher = new Searcher();

        Query query = Searcher.createTermRangeQuery("date", "200901121400", "200901121505");
        query = Searcher.createTermRangeQuery("date", "200901", "200902");
        TopDocs topDocs = searcher.executeQuery(query, 1000);
        System.out.println("hits: " + topDocs.scoreDocs.length);
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = searcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
        }
        searcher.close();
    }
}
