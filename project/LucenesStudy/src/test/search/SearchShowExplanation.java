package test.search;

import java.io.IOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import lucene.Indexer;
import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class SearchShowExplanation {

    public static void main(String[] args) throws IOException, ParseException {
        Searcher searcher = new Searcher();
        Query query = searcher.createQuery("content", "good");
        TopDocs topDocs = searcher.executeQuery(query, 100);
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = searcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
//            System.out.println(doc.getField("content").stringValue());
            Explanation explanation = searcher.explain(query, scoreDoc.doc);
            System.out.println(explanation);
        }
    }
}
