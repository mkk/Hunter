package test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class DeleteWithCommit {

    public static void main(String[] args) throws IOException {
        Indexer indexer = new Indexer(Lucenes.getFSDirectory("E:\\index"), false, Lucenes.getMyIndexDeletionPolicy());

        indexer.deleteByPath("D:\\learn\\Born to Win\\Ŀ¼.txt");

        Map<String, String> commitUserData = new HashMap();
        commitUserData.put("myCommit", "delete");
        indexer.commit(commitUserData);

        indexer.close();
    }
}
