package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class AllReindex {

	public static void main(String[] args) throws FileNotFoundException,
			CorruptIndexException, IOException {
		Indexer indexer = new Indexer(Lucenes.getFSDirectory("E:\\index"),
				true, new KeepOnlyLastTwoCommitDeletionPolicy());
		File file = new File("E:\\baby");
		indexer.setUseCompoundFile(true);
		add(new File[] { file }, indexer);
		Map<String, String> commitUserData = new HashMap();
		commitUserData.put("myCommit", "reindex");
		System.out.println("commit");
		indexer.commit(commitUserData);
		System.out.println("close");
		indexer.close();

	}

	public static void add(File[] files, Indexer indexWriter)
			throws FileNotFoundException, CorruptIndexException, IOException {
		for (File file : files) {
			indexWriter.addDocument(file);
			if (file.isFile()) {
			} else {
				File[] children = file.listFiles();
				if (children != null) {
					add(children, indexWriter);
				}
			}
		}
	}
}
