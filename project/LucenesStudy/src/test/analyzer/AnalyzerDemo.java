/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.analyzer;

import java.io.IOException;
import luceneUtil.AnalyzerUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

/**
 * 
 * @author Administrator
 */

public class AnalyzerDemo {

	private static final String[] examples = {
			"The quick brown fox jumped over the lazy dogs",
			"XY&Z Corporation - xyz@example.com" };
	private static final Analyzer[] analyzers = new Analyzer[] {
			new WhitespaceAnalyzer(), new SimpleAnalyzer(),
			new StopAnalyzer(Version.LUCENE_30),
			new StandardAnalyzer(Version.LUCENE_30) };

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// Use the embedded example strings, unless
		// command line arguments are specified, then use those.
		String[] strings = examples;
		if (args.length > 0) {
			strings = args;
		}
		for (int i = 0; i < strings.length; i++) {
			analyze(strings[i]);
		}
	}

	private static void analyze(String text) throws IOException {
		System.out.println("Analyzing \"" + text + "\"");
		for (int i = 0; i < analyzers.length; i++) {
			Analyzer analyzer = analyzers[i];
			String name = analyzer.getClass().getName();
			name = name.substring(name.lastIndexOf(".") + 1);
			System.out.println(" " + name + ":");
			System.out.print(" ");
			AnalyzerUtils.displayTokens(analyzer, text);
			AnalyzerUtils.displayTokensWithFullDetails2(analyzer, text);
			System.out.println("\n");
		}
	}
}
