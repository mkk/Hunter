package test;

import java.io.IOException;

import lucene.Indexer;
import lucene.Lucenes;
import lucene.Searcher;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class Cooperate {

    public static void main(String[] args) throws CorruptIndexException, IOException {
        Directory directory = Lucenes.getFSDirectory("E:\\index");
        Indexer indexWriter = new Indexer(directory, false);

        IndexReader indexReader = IndexReader.open(directory);
        Searcher indexSearcher = new Searcher(indexReader);

        System.out.println("before delete and commit");
        TopDocs topDocs = indexSearcher.searchByPath("D:\\learn\\Born to Win\\目录.txt");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
        }

        indexWriter.deleteByPath("D:\\learn\\Born to Win\\目录.txt");

        System.out.println("after delete before commit");
        topDocs = indexSearcher.searchByPath("D:\\learn\\Born to Win\\目录.txt");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
        }


        indexWriter.commit();


        System.out.println("after delete after commit");
        topDocs = indexSearcher.searchByPath("D:\\learn\\Born to Win\\目录.txt");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
        }


        System.out.println("reopen after commit");
        indexReader = IndexReader.open(directory);
        indexSearcher = new Searcher(indexReader);
        topDocs = indexSearcher.searchByPath("D:\\learn\\Born to Win\\目录.txt");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc);
            System.out.println(doc.getField("title").stringValue());
            System.out.println(doc.getField("path").stringValue());
            System.out.println(doc.getField("date").stringValue());
        }

        indexWriter.close();
    }
}
