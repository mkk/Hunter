package test;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.index.CorruptIndexException;
import lucene.Indexer;
import lucene.Lucenes;

import org.apache.lucene.index.CorruptIndexException;

import extend.KeepOnlyLastTwoCommitDeletionPolicy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Administrator
 */
public class Delete {

	public static void main(String[] args) throws IOException {
		Indexer indexWriter = new Indexer(false);

		System.out.println("before delete");
		System.out.println(indexWriter.hasDeletions());

		indexWriter.deleteByPath("D:\\learn\\Born to Win\\Ŀ¼.txt");

		System.out.println("after delete  before commit");
		System.out.println(indexWriter.hasDeletions());

		System.out.println("before commit");
		System.out.println("maxDoc: " + indexWriter.getMaxDoc());
		System.out.println("numDoc: " + indexWriter.getNumDoc());

		indexWriter.commit();

		System.out.println("after delete  after commit");
		System.out.println(indexWriter.hasDeletions());

		System.out.println("after commit");
		System.out.println("maxDoc: " + indexWriter.getMaxDoc());
		System.out.println("numDoc: " + indexWriter.getNumDoc());
		indexWriter.close();
	}
}
