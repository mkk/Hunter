package test.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.LockObtainFailedException;
import org.junit.Ignore;
import org.junit.Test;

import com.gh.deskTopSearch.file.FileData;
import com.gh.deskTopSearch.file.FileTools;
import com.gh.deskTopSearch.file.RealFile;
import com.gh.deskTopSearch.file.fetch.Chooser;
import com.gh.deskTopSearch.file.fetch.Fecther;
import com.gh.deskTopSearch.file.fetch.FileFetcher;
import com.gh.deskTopSearch.file.fetch.support.ChooserClause;
import com.gh.deskTopSearch.file.fetch.support.ChooserGroup;
import com.gh.deskTopSearch.file.fetch.support.DirectoryOnly;
import com.gh.deskTopSearch.file.fetch.support.FileOnly;
import com.gh.deskTopSearch.file.fetch.support.PostfixFileChooser;
import com.gh.deskTopSearch.index.FileIndexer;
import com.gh.deskTopSearch.search.FileSearcher;
import com.gh.deskTopSearch.search.SearchTools;
import com.gh.lucene.util.Lucenes;
import com.gh.lucene.util.UserData;
import com.gh.lucene.util.index.IndexException;
import com.gh.lucene.util.search.Queries;
import com.gh.lucene.util.search.QuerySession;
import com.gh.lucene.util.search.SearchException;

public class SearchTest {

	@Ignore
	@Test
	public void testSearchTxtFile() throws SearchException {
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");

		Query query = SearchTools.createPostfixQuery("java");

		QuerySession session = searcher.createQuerySession(query);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(" Postfix[%-3s] Name[%-20s] Path[%s]%n", file
						.getPostfix(), file.getFileName(), file
						.getAbsolutePath());
			}
		}
	}

	@Ignore
	@Test
	public void testSearchDataRange() throws SearchException {
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");

		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 3, 13);
		Date lower = calendar.getTime();
		calendar.set(2010, 7, 1);
		Date upper = calendar.getTime();

		Query query = SearchTools.createDateRangeQuery(lower, upper);

		QuerySession session = searcher.createQuerySession(query);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(
						" Postfix:%-3s%n Name:%-20s%n Path:%s%n Date:%s%n%n ",
						file.getPostfix(), file.getFileName(), file
								.getAbsolutePath(), DateTools
								.timeToString(file.getLastModified(),
										DateTools.Resolution.MINUTE));
			}
		}
	}

	@Ignore
	@Test
	public void testSearchDataRangeAndTxt() throws SearchException {
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");

		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 3, 13);
		Date lower = calendar.getTime();
		calendar.set(2010, 6, 1);
		Date upper = calendar.getTime();

		Query dataRange = SearchTools.createDateRangeQuery(lower, upper);
		Query java = SearchTools.createPostfixQuery("java");

		Query query = Queries.andQuery(new Query[] { dataRange, java });

		QuerySession session = searcher.createQuerySession(query);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(
						" Postfix:%-3s%n Name:%-20s%n Path:%s%n Date:%s%n%n ",
						file.getPostfix(), file.getFileName(), file
								.getAbsolutePath(), DateTools
								.timeToString(file.getLastModified(),
										DateTools.Resolution.MINUTE));
			}
		}
	}

	@Ignore
	@Test
	public void testSearchPartFileName() throws SearchException {
		// TODO
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");

		Query query = SearchTools.createPartFileNameQuery("menu");

		QuerySession session = searcher.createQuerySession(query);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(
						" Postfix:%-3s%n Name:%-20s%n Path:%s%n Date:%s%n%n ",
						file.getPostfix(), file.getFileName(), file
								.getAbsolutePath(), DateTools
								.timeToString(file.getLastModified(),
										DateTools.Resolution.MINUTE));
			}
		}
	}

	@Ignore
	@Test
	public void testSearchDatePoint() throws SearchException {
		// TODO
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");
		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 3, 13);
		Query query = SearchTools.createDatePointQuery(calendar.getTime(),
				DateTools.Resolution.MONTH);

		QuerySession session = searcher.createQuerySession(query);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(
						" Postfix:%-3s%n Name:%-20s%n Path:%s%n Date:%s%n%n ",
						file.getPostfix(), file.getFileName(), file
								.getAbsolutePath(), DateTools
								.timeToString(file.getLastModified(),
										DateTools.Resolution.MINUTE));
			}
		}
	}

	@Ignore
	@Test
	public void testIKAnaylyzer() throws IOException {

		long start = System.currentTimeMillis();
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				"C:\\Documents and Settings\\rjgc\\桌面\\book\\七界传说全集.txt")));

		Analyzer ikAnalyzer = Lucenes.getChineseAnaylzer();

		TokenStream tokenStream = ikAnalyzer.tokenStream("content", reader);

		while (tokenStream.incrementToken()) {

			// TermAttribute term =
			// tokenStream.getAttribute(TermAttribute.class);
			// System.out.printf("term[%s]%n", term.term());

		}

		long end = System.currentTimeMillis();

		long duration = end - start;
		System.out.println(duration);
		// 经过测试,IKAnalyzer 能达到 一秒 35万字.

	}

	@Ignore
	@Test
	public void testIndex() throws IOException {
		File file = new File("../DesktopSearch2U/test/sourceFile");
		boolean exists = file.exists();
		System.out.print(exists);

		System.out.println(file.getCanonicalPath());
	}

	@Ignore
	@Test
	public void testChooserGroup() {

//		Chooser fileChooser = new ChooserGroup(new ChooserClause(
//				new FileOnly(), ChooserClause.Occur.MUST), new ChooserClause(
//				new PostfixFileChooser("txt", "html", "css"),
//				ChooserClause.Occur.NOT));

//		Chooser fileChooser2 = new ChooserGroup(new ChooserClause(
//				new FileOnly(), ChooserClause.Occur.MUST), new ChooserClause(
//				new PostfixFileChooser("txt", "html", "css"),
//				ChooserClause.Occur.NOT));
//
//		Fecther fetcher = new FileFetcher(new File[] { new File(
//				"../DesktopSearch2U/test/sourceFile") }, fileChooser);
//
//		Enumeration enumer = fetcher.iterator();

//		while (enumer.hasMoreElements()) {
//			enumer.nextElement();
//		}
	}

	@Ignore
	@Test
	public void testChooserGroupIndex() throws CorruptIndexException,
			LockObtainFailedException, IOException, IndexException {

		FileIndexer indexer = FileIndexer.createFileIndexer(
				"../DesktopSearch2U/test/index", true);

		Chooser fileChooser = new ChooserGroup(new ChooserClause(
				new FileOnly(), ChooserClause.Occur.MUST), new ChooserClause(
				new PostfixFileChooser("txt", "html", "css"),
				ChooserClause.Occur.NOT));

		Chooser directoryChooser = new ChooserGroup(new ChooserClause(
				new DirectoryOnly(), ChooserClause.Occur.MUST));

		Chooser group = new ChooserGroup(new ChooserClause(fileChooser,
				ChooserClause.Occur.SHOULD), new ChooserClause(
				directoryChooser, ChooserClause.Occur.SHOULD));
		Fecther<FileData> fetcher = new FileFetcher(new File[] { new File(
				"../DesktopSearch2U/test/sourceFile") }, group);

		Enumeration<FileData> enumer = fetcher.iterator();

		while (enumer.hasMoreElements()) {
			indexer.index(enumer.nextElement());
		}

		indexer.close();
	}

//	@Ignore
	@Test
	public void testSearchDirectory() throws SearchException {
		FileSearcher searcher = new FileSearcher(
				"../DesktopSearch2U/test/index");

		Query textDirectory = SearchTools.createRealDirectory();

		QuerySession session = searcher.createQuerySession(textDirectory);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			// if (userData instanceof FileData) {
			FileData file = (FileData) userData;
			System.out.printf("  Name:%-20s%n Path:%s%n Date:%s%n%n ", file
					.getFileName(), file.getAbsolutePath(), DateTools
					.timeToString(file.getLastModified(),
							DateTools.Resolution.MINUTE));
			// }
		}
	}

	@Ignore
	@Test
	public void testSearchFile() throws SearchException {
		FileSearcher searcher = new FileSearcher(
				"../DesktopSearch2U/test/index");

		Query textDirectory = SearchTools.createRealFileQuery();

		QuerySession session = searcher.createQuerySession(textDirectory);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			FileData file = (FileData) userData;
			System.out.printf("  Name:%-20s%n Path:%s%n Date:%s%n%n ", file
					.getFileName(), file.getAbsolutePath(), DateTools
					.timeToString(file.getLastModified(),
							DateTools.Resolution.MINUTE));

		}
	}
	
	
	@Ignore
	@Test
	public void testSearchFileName() throws SearchException {
		FileSearcher searcher = new FileSearcher(
				"../DesktopSearch2U/test/index");
		//TODO 现在的搜索在not analyzed 的field 是,不区分大小写
		Query textDirectory = SearchTools.createFullFileNameQuery("BEEA.GIF");

		QuerySession session = searcher.createQuerySession(textDirectory);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			FileData file = (FileData) userData;
			FileTools.print(file);
		}
	}
	
	@Ignore
	@Test
	public void testSearchFileName2() throws SearchException, IOException {
		FileSearcher searcher = new FileSearcher(
				"../DesktopSearch2U/test/index");
		Query textDirectory = SearchTools.crreateFileNameQueryAnalyzedByIK("jquery Ui");

		QuerySession session = searcher.createQuerySession(textDirectory);
		session.execute();

		while (session.hasMore()) {
			UserData userData = session.next();
			FileData file = (FileData) userData;
			FileTools.print(file);
		}
	}
}
