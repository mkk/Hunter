package test.search;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopDocsCollector;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.JaroWinklerDistance;
import org.apache.lucene.search.spell.LevensteinDistance;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.spell.NGramDistance;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.search.spell.StringDistance;
import org.apache.lucene.store.Directory;

import com.gh.deskTopSearch.file.FieldNameConstants;
import com.gh.deskTopSearch.file.FileTools;
import com.gh.deskTopSearch.search.SearchTools;
import com.gh.lucene.util.Lucenes;
import com.gh.lucene.util.search.query.AllWhioutScoreSortingCollector;
import com.mkk.deskTopSearch.util.PropertiesUtil;

public class SearchAllFile {
	public static void main(String[] args) throws IOException {
		testCelection();
	}

	public static void test() throws IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(PropertiesUtil.SAVE_INDEX_PATH);
		IndexReader reader = IndexReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(directory);

		Term term = new Term(FieldNameConstants.TYPE, FileTools.REAL_FILE_TYPE
				.getValue());

		Query query = new TermQuery(term);

		TopDocs docs = searcher.search(query, 3000);
		System.out.println("hits: " + docs.totalHits);
		for (ScoreDoc scoreDoc : docs.scoreDocs) {
			Document doc = reader.document(scoreDoc.doc);
			System.out.println(scoreDoc.doc + " score: " + scoreDoc.score);
			System.out.println(doc.getField(FieldNameConstants.ABSOLUTE_PATH)
					.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.NAME).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_DATE)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_LONG)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.IS_HIDDEN).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.LENGTH).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.POSTFIX).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.TYPE).stringValue());

			System.out.println();
		}
	}

	public static void test2() throws IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(PropertiesUtil.SAVE_INDEX_PATH);
		IndexReader reader = IndexReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(directory);

		Term term = new Term(FieldNameConstants.POSTFIX, "doc");

		Query query = new TermQuery(term);
		AllWhioutScoreSortingCollector collector = new AllWhioutScoreSortingCollector();
		searcher.search(query, collector);
		List<Integer> docsId = collector.docs();
		System.out.println("hits: " + docsId.size());
		for (Integer id : docsId) {
			Document doc = reader.document(id);
			System.out.println(id + " score: " + 0);
			System.out.println(doc.getField(FieldNameConstants.ABSOLUTE_PATH)
					.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.NAME).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_DATE)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_LONG)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.IS_HIDDEN).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.LENGTH).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.POSTFIX).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.TYPE).stringValue());
			System.out.println();
		}
	}

	public static void test3() throws IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory("E:\\desktopSearchIndex");
		IndexReader reader = IndexReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(directory);

		// Term term = new Term(FieldNameConstants.POSTFIX, "java");
		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 3, 13);
		Date lower = calendar.getTime();
		calendar.set(2010, 6, 1);
		Date upper = calendar.getTime();
		Query query = SearchTools.createDateRangeQuery(lower, upper);
		AllWhioutScoreSortingCollector collector = new AllWhioutScoreSortingCollector();
		searcher.search(query, collector);
		List<Integer> docsId = collector.docs();
		System.out.println("hits: " + docsId.size());
		for (Integer id : docsId) {
			Document doc = reader.document(id);
			System.out.println(id + " score: " + 0);
			System.out.println(doc.getField(FieldNameConstants.ABSOLUTE_PATH)
					.stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.NAME).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_DATE)
							.stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.LAST_MODIFY_LONG)
			// .stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.IS_HIDDEN).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.LENGTH).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.POSTFIX).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.TYPE).stringValue());
			System.out.println();
		}
	}

	public static void testTermEnum() throws CorruptIndexException, IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory("../DesktopSearch2U/test/index");
		IndexReader reader = IndexReader.open(directory);
		TermEnum terms = reader.terms();
		while (terms.next()) {
			Term term = terms.term();
			System.out.println(term.text());
		}
	}

	public static void testSpellChecker() throws CorruptIndexException,
			IOException {
		// JaroWinklerDistance, LevensteinDistance, NGramDistance
		StringDistance distance = new JaroWinklerDistance();

		float dist = distance.getDistance("李小辉", "小高");
		System.out.println(dist);

		distance = new LevensteinDistance();
		dist = distance.getDistance("李小辉", "小高");
		System.out.println(dist);

		distance = new NGramDistance();
		dist = distance.getDistance("李小辉", "小高");
		System.out.println(dist);
		IndexReader reader = IndexReader.open(Lucenes
				.getSimpleFileSystemDirectory("../DesktopSearch2U/test/index"));
		SpellChecker cheker = new SpellChecker(Lucenes
				.getSimpleFileSystemDirectory("../DesktopSearch2U/test/index"),
				new NGramDistance());

		Dictionary dic = new LuceneDictionary(reader,
				FieldNameConstants.NAME_ANALYZED);

		cheker.indexDictionary(dic);

		String[] suggestion = cheker.suggestSimilar("me", 100);
		for (String s : suggestion) {
			System.out.println(s);
		}
	}

	public static void testCelection() throws IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(PropertiesUtil.SAVE_INDEX_PATH);
		IndexReader reader = IndexReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(directory);

		Term term = new Term(FieldNameConstants.POSTFIX, "mp3");

		Query query = new TermQuery(term);
		TopDocsCollector<ScoreDoc> collector = TopScoreDocCollector.create(1000, false);
		searcher.search(query, collector);
		TopDocs docs = collector.topDocs();
		System.out.println("hits: " + docs.totalHits);
		for (ScoreDoc docS : docs.scoreDocs) {
			Document doc = reader.document(docS.doc);
			System.out.println(docS.doc + " score: " + 0);
			System.out.println(doc.getField(FieldNameConstants.ABSOLUTE_PATH)
					.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.NAME).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_DATE)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.LAST_MODIFY_LONG)
							.stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.IS_HIDDEN).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.LENGTH).stringValue());
			// System.out.println(" "
			// + doc.getField(FieldNameConstants.POSTFIX).stringValue());
			System.out.println(" "
					+ doc.getField(FieldNameConstants.TYPE).stringValue());
			System.out.println();
		}
	}
}
