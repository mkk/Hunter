package com.mkk.deskTopSearch.util;

import com.gh.deskTopSearch.search.FileSearcher;

/**
 * 2010-8-16 下午07:06:33<br>
 * 搜索对象缓存工具类
 * 
 * @author mkk 
 */
public class SearcherManager {

	private static SearcherManager man = null;

	private SearcherManager() {
	}

	/**
	 * 获取唯一实例
	 * 
	 * @return
	 */
	public static SearcherManager getSearcherManager() {
		if (man == null) {
			man = new SearcherManager();
		}
		return man;
	}

	// 缓存对象
	private FileSearcher searcher = null;

	/**
	 * 初始化FileSearcher<br>
	 * 在系统启动时调用
	 */
	public void initFileSearcher() {
		new Thread() {
			@Override
			public void run() {
				if (SystemUtil.isIndexFileExist()) {
					updateFileSearcher();
				}
			}
		}.start();
	}

	/**
	 * 更新Searcher <br>
	 * 注意: 应该在每次创建完索引后调用此方法
	 */
	public void updateFileSearcher() {
		searcher = new FileSearcher(PropertiesUtil.SAVE_INDEX_PATH);
	}

	/**
	 * 获取Searcher对象
	 * 
	 * @return
	 */
	public FileSearcher getFileSearcher() {
		if (searcher == null) {
			updateFileSearcher();
		}
		return searcher;
	}

	/**
	 * 关闭Searcher <br>
	 * 注意: 在退出系统时请调用 此方法
	 */
	public void closeSearch() {
		if (searcher != null) {
			searcher.close();
		}
	}

}
