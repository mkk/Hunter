package com.mkk.deskTopSearch.util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * 2010-8-10 下午09:16:48
 * 
 * @author mkk 系统工具类
 */
public class SystemUtil {

	// Toolkit
	private static Toolkit toolkit = Toolkit.getDefaultToolkit();
	// dateformat
	private static SimpleDateFormat sdFormat = new SimpleDateFormat();
	// number format
	private static DecimalFormat dFormat = new DecimalFormat();

	/**
	 * 判断索引文件是否有
	 * 
	 * @return
	 */
	public static boolean isIndexFileExist() {
		File f = new File(PropertiesUtil.SAVE_INDEX_PATH);
		if (f.exists()) {
			if (f.list().length > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 打开指定的文件
	 * 
	 * @param filePath
	 *            文件绝对路径
	 * @throws IOException
	 */
	public static void openFile(String filePath) throws IOException {
		if (filePath == null) {
			return;
		}
		String command = "cmd /c start explorer " + filePath;
		Runtime.getRuntime().exec(command);
	}

	/**
	 * 打开目录并选中指定的文件
	 * 
	 * @param filePath
	 *            文件
	 * @throws IOException
	 */
	public static void operDir4File(String filePath) throws IOException {
		if (filePath == null) {
			return;
		}
		String command = "cmd /c explorer /select," + filePath;
		Runtime.getRuntime().exec(command);
	}

	/**
	 * 将文件长度转化
	 * 
	 * @param fileLength
	 * @return
	 */
	public static String formatFileLength(long fileLength) {
		if (fileLength / (1024 * 1024) > 0) {
			return formatNumber(fileLength / (1024 * 1024d), "0.00") + " MB";
		} else {
			return formatNumber(fileLength / 1024d, "0.00") + " K";
		}
	}

	/**
	 * 系统logo图标
	 * 
	 * @return
	 */
	public static ImageIcon getSystemLogoIcon() {
		ImageIcon icon = new ImageIcon("icon" + File.separator + "logo.gif");
		return icon;
	}

	/**
	 * 格式化数字
	 * 
	 * @param number
	 *            数字
	 * @param format
	 *            格式,如 0.00
	 * @return
	 */
	public static String formatNumber(Object number, String format) {
		if (number == null || format == null) {
			return null;
		}
		dFormat.applyPattern(format);
		String s = dFormat.format(number);
		return s;
	}

	/**
	 * 格式化日期
	 * 
	 * @param date
	 *            日期值
	 * @param format
	 *            格式
	 * @return
	 */
	public static String formatDate(Date date, String format) {
		if (date == null || format == null) {
			return null;
		}
		sdFormat.applyPattern(format);
		String s = sdFormat.format(date);
		return s;
	}

	/**
	 * 根据数值获取时间字符串
	 * 
	 * @param times
	 * @return
	 */
	public static String getTimeByLong(long times) {
		Date d = new Date(times);
		String s = formatDate(d, "yyyy-MM-dd HH:mm:ss");
		return s;
	}

	/**
	 * 根据界面的长度与宽度确定界面的左上角坐标值
	 * 
	 * @param width
	 *            长度
	 * @param height
	 *            宽度
	 * @return
	 */
	public static Point getStartPoint(int width, int height) {
		Dimension dim = toolkit.getScreenSize();
		dim.width = dim.width / 2 - width / 2;
		dim.height = dim.height / 2 - height / 2;
		Point point = new Point(dim.width, dim.height);
		return point;
	}

	/**
	 * 设置UI WindowsLookAndFeel
	 * 
	 */
	public static void setUI() {
		try {
			String ui = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(ui);
			JFrame.setDefaultLookAndFeelDecorated(true);
		} catch (Exception e) {
			System.out.println("UI exception: " + e.getMessage());
		}
	}

}
