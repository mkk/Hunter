package com.mkk.deskTopSearch.util;

import java.io.File;
import java.util.prefs.Preferences;

/**
 * 2010-8-10 下午10:20:59 系统配置工具类
 * 
 * @author mkk
 */
public class PropertiesUtil {

	// 日志存放目录
	public static final String LOG_PATH = "log";
	static {
		// 创建需要的目录
		File f = new File(LOG_PATH);
		if (!f.exists()) {
			f.mkdir();
		}
	}
	// 存放索引文件目录
	public static final String SAVE_INDEX_PATH = "indexs";
	// 默认搜索每次获取的结果条数,默认100条,暂时没用
	public static final int SEARCH_RETURN_NUMBER = 100;
	// 每页显示的数目
	public static final int DEFAULT_PAGE_SIZE = 7;

	// preferences
	private static Preferences preferences = Preferences
			.systemNodeForPackage(PropertiesUtil.class);

	/**
	 * 获取是否显示关闭时的窗口
	 * 
	 * @return
	 */
	public static boolean isShowClosePanel() {
		return preferences.getBoolean("search.isShowClosePanel", true);
	}

	/**
	 * 设置是否显示关闭时的窗口
	 * 
	 * @param flag
	 */
	public static void setShowClosePanel(boolean flag) {
		preferences.putBoolean("search.isShowClosePanel", flag);
	}

	/**
	 * 获取关闭主界面时的操作类型
	 * 
	 * @return true为关闭,false为最小化到系统托盘
	 */
	public static boolean getCloseChooser() {
		return preferences.getBoolean("search.closeChooser", true);
	}

	/**
	 * 设置关闭主界面时的操作类型
	 * 
	 * @param flag
	 *            true为关闭,false为最小化到系统托盘
	 */
	public static void setCloseChooser(boolean flag) {
		preferences.putBoolean("search.closeChooser", flag);
	}

}
