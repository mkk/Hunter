package com.mkk.deskTopSearch.util;

import java.util.Collections;
import java.util.List;

/**
 * 2010-8-11
 * 
 * @author mkk 分页工具类,基于数据在内存中的分页
 */
public class PageTool<T> {

	// 数据
	private List<T> datas;
	// 当前页
	private int currPage = 1;
	// 总页数
	private int totalPages = 0;
	// 每页显示数目,默认20
	private int pageSize = 20;
	// 数据总条数
	private int totalCount = 0;

	/**
	 * 构造函数
	 * 
	 * @param datas
	 *            需要分页的数据集合
	 */
	public PageTool(List<T> datas) {
		if (datas == null) {
			throw new NullPointerException("datas is null");
		}
		this.datas = datas;
		this.calculationInfos();
	}

	/**
	 * 设置搜索数据,将对象重置
	 * 
	 * @param datas
	 */
	public void setNewDatas(List<T> datas) {
		if (datas == null) {
			throw new NullPointerException("datas is null");
		}
		this.datas = datas;
		currPage = 1;
		totalPages = 0;
		totalCount = 0;
		this.calculationInfos();
	}

	/**
	 * 构造函数
	 * 
	 * @param datas
	 *            需要分页的数据集合
	 * @param pageSize
	 *            每页要显示的数目,若小于1则使用默认值20
	 */
	public PageTool(List<T> datas, int pageSize) {
		if (datas == null) {
			throw new NullPointerException("datas is null");
		}
		this.datas = datas;
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}
		this.calculationInfos();
	}

	/**
	 * 计算相关数目信息
	 */
	private void calculationInfos() {
		this.totalCount = this.datas.size();
		if (this.totalCount % this.pageSize == 0) {
			this.totalPages = (this.totalCount / this.pageSize);
		} else {
			this.totalPages = (this.totalCount / this.pageSize) + 1;
		}
	}

	/**
	 * 获取当前页数目
	 * 
	 * @return
	 */
	public int getCurrpage() {
		return this.currPage;
	}

	/**
	 * 获取总页数
	 * 
	 * @return
	 */
	public int getTotalpages() {
		return this.totalPages;
	}

	/**
	 * 获取数据总条数
	 * 
	 * @return
	 */
	public int getDatasSize() {
		return this.totalCount;
	}

	/**
	 * 判断是否还有下一页
	 * 
	 * @return
	 */
	public boolean hasNextpage() {
		int i = this.currPage + 1;
		if (i <= this.totalPages) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否有上一页
	 * 
	 * @return
	 */
	public boolean hasPreviouspage() {
		int i = this.currPage - 1;
		if (i >= 1) {
			return true;
		}
		return false;
	}

	/**
	 * 获取每页的数据
	 * 
	 * @param start
	 *            开始数目
	 * @param end
	 *            结束数目
	 * @return
	 */
	private List<T> getPageDatas(int start, int end) {
		List<T> subList = this.datas.subList(start, end);
		return subList;
	}

	/**
	 * 获取第一页数据
	 * 
	 * @return
	 */
	public List<T> getFirstpageDatas() {
		this.currPage = 1;
		if (this.totalCount <= this.pageSize) {
			return this.getPageDatas(0, this.totalCount);
		}
		return this.getPageDatas(0, this.pageSize);
	}

	/**
	 * 获取最后一页数据
	 * 
	 * @return
	 */
	public List<T> getLastpageDatas() {
		this.currPage = this.totalPages;
		int start = (this.totalPages - 1) * this.pageSize;
		return this.getPageDatas(start, this.totalCount);
	}

	/**
	 * 获取下一页数据<br>
	 * 若在最后一页继续获取下一页数据,永远返回最后一页数据 可调用hasNextpage()方法判断是否有下一页
	 * 
	 * @return
	 */
	public List<T> getNextpageDatas() {
		if (this.currPage < this.totalPages - 1) {
			this.currPage++;
			int start = (this.currPage - 1) * this.pageSize;
			int end = start + this.pageSize;
			return this.getPageDatas(start, end);
		} else {
			return this.getLastpageDatas();
		}
	}

	/**
	 * 获取上一页数据<br>
	 * 若在第一页继续获取上一页数据,永远返回第一页数据 可调用hasPreviouspage()方法判断是否有上一页
	 * 
	 * @return
	 */
	public List<T> getPreviouspageDatas() {
		if (this.currPage > 1) {
			this.currPage--;
			int start = (this.currPage - 1) * this.pageSize;
			int end = start + this.pageSize;
			return this.getPageDatas(start, end);
		} else {
			return this.getFirstpageDatas();
		}
	}

	/**
	 * 获取指定页的数据
	 * 
	 * @param page
	 *            页数,必须位于1 与 getTotalpages()之间的数
	 * @return 若指定的页数不存在则返回空数据集
	 */
	public List<T> getPageDatas(int page) {
		List<T> list = null;
		if (page < 1 || page > this.totalPages) {
			list = Collections.emptyList();
			return list;
		}
		this.currPage = page;
		if (page == this.totalPages) {
			list = this.getLastpageDatas();
		} else if (page == 1) {
			list = this.getFirstpageDatas();
		} else {
			list = this.getPageDatas((page - 1) * this.pageSize, page
					* this.pageSize);
		}
		return list;
	}

}
