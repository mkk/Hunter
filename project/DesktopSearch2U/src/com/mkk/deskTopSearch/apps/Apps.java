package com.mkk.deskTopSearch.apps;

import java.awt.EventQueue;

import com.mkk.deskTopSearch.util.SearcherManager;
import com.mkk.deskTopSearch.util.SystemUtil;
import com.mkk.deskTopSearch.view.MainFrame;

/**
 * 2010-8-10  下午09:18:36
 * @author mkk
 * 程序主启动类
 */
public class Apps {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// set UI
				SystemUtil.setUI();
				//init search
				SearcherManager.getSearcherManager().initFileSearcher();
				//show
				new MainFrame();
				
			}
		});

	}

}
