package com.mkk.deskTopSearch.worker;

import java.io.File;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import com.gh.deskTopSearch.file.FileData;
import com.gh.deskTopSearch.file.fetch.FileFetcher;
import com.gh.deskTopSearch.file.fetch.support.ChooserClause;
import com.gh.deskTopSearch.file.fetch.support.ChooserGroup;
import com.gh.deskTopSearch.file.fetch.support.DirectoryOnly;
import com.gh.deskTopSearch.file.fetch.support.FileOnly;
import com.gh.deskTopSearch.file.fetch.support.ChooserClause.Occur;
import com.gh.deskTopSearch.index.FileIndexer;
import com.mkk.deskTopSearch.util.LogUtil;
import com.mkk.deskTopSearch.util.PropertiesUtil;
import com.mkk.deskTopSearch.util.SearcherManager;
import com.mkk.deskTopSearch.util.SystemUtil;

/**
 * 2010-8-10 下午10:03:12
 * 
 * @author mkk 创建索引的SwingWorker类
 */
public class CreateIndexWorker extends SwingWorker<String, String> {

	// 创建索引的目录
	private File file;
	// 是否覆盖已经存在的索引
	private boolean isOvered;
	// 输出信息组件
	private JTextArea textarea;

	/**
	 * @param textarea
	 *            输出信息的组件
	 * @param file
	 *            创建索引的目录
	 * @param isOvered
	 *            是否覆盖已经存在的索引
	 */
	public CreateIndexWorker(JTextArea textarea, File file, boolean isOvered) {
		this.file = file;
		this.isOvered = isOvered;
		this.textarea = textarea;

	}

	/**
	 * 后台任务
	 */
	protected String doInBackground() throws Exception {
		publish("开始创建索引...\n");
		long start = System.currentTimeMillis();
		// 创建类型
		// 1. new FileOnly()只对文件建立索引
		// 2.new DirectoryOnly() 对目录建立索引
		// 默认为文件与目录建索引
		ChooserClause filec = new ChooserClause(new FileOnly(), Occur.SHOULD);
		ChooserClause directory = new ChooserClause(new DirectoryOnly(),
				Occur.SHOULD);
		ChooserGroup group = new ChooserGroup(filec, directory);
		FileFetcher ff = new FileFetcher(new File[] { file }, group);
		Enumeration<FileData> fd = ff.iterator();
		FileIndexer fileIndexer = null;
		try {
			// 处理日志
			PrintStream ps = LogUtil.getCreatIndexStream();
			if (ps != null) {
				fileIndexer = FileIndexer.createFileIndexer(
						PropertiesUtil.SAVE_INDEX_PATH, this.isOvered, ps);
			} else {
				fileIndexer = FileIndexer.createFileIndexer(
						PropertiesUtil.SAVE_INDEX_PATH, this.isOvered);
			}
			while (fd.hasMoreElements()) {
				FileData fda = fd.nextElement();
				publish("正在创建索引文件: " + fda.getAbsolutePath() + "\n");
				fileIndexer.index(fda);
			}
			publish("\n创建索引共耗时 "
					+ SystemUtil.formatNumber(
							(System.currentTimeMillis() - start) / 1000d,
							"0.00") + " 秒");

		} catch (Exception e) {
			e.printStackTrace();
			// 保存异常日志
			LogUtil.saveExceptionLog(e);
			return "创建索引异常,信息\n" + e.getMessage();
		} finally {
			// 关闭
			if (fileIndexer != null) {
				fileIndexer.close();
			}
		}
		return "索引创建成功!";
	}

	/**
	 * 输出
	 */
	protected void process(List<String> chunks) {
		for (String s : chunks) {
			this.textarea.append(s);
		}
	}

	/**
	 * 完成后关闭
	 */
	protected void done() {
		try {
			this.textarea.append("\n" + get());
		} catch (InterruptedException e) {
			LogUtil.saveExceptionLog(e);
		} catch (ExecutionException e) {
			LogUtil.saveExceptionLog(e);
		}
		// 更新searcher
		SearcherManager.getSearcherManager().updateFileSearcher();
	}

}
