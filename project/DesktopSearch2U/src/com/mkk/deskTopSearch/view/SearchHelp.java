package com.mkk.deskTopSearch.view;

import java.util.ArrayList;
import java.util.List;

/**
 * 2010-7-15 下午10:12:55 界面搜索测试帮助类
 */
public class SearchHelp {

	private static String[] datas = { "avage", "apple", "bad", "backup", "conime",
			"disktop" };

	/**
	 * 模拟搜索
	 * 
	 * @param s
	 * @return
	 */
	public static Object[] search(String s) {
		List<String> result = new ArrayList<String>();
		if (s.length() > 0) {
			for (String s1 : datas) {
				if (s1.startsWith(s)) {
					result.add(s1);
				}
			}
		}
		return result.toArray();
	}

}
