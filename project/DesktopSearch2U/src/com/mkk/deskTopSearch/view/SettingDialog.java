package com.mkk.deskTopSearch.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

import com.mkk.deskTopSearch.util.SystemUtil;

/**
 * 2010-8-16 下午08:06:06
 * 
 * @author mkk 系统设置界面
 */
public class SettingDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	// 默认大小
	private static int DEFAULT_WIDTH = 600;
	private static int DEFAULT_HEIGHT = 370;
	// 标题
	private String title = "设置";
	// 面板
	private JPanel panel1, panel2, panel3, panel4, panel5;
	// 标签
	private JLabel label1;
	// 按钮
	private JButton button1,button2,button3,button4;
	// 标签栏
	private JTabbedPane tabbedPane;
	//进度条
	private JProgressBar progressBar;

	/**
	 * 构造器
	 * 
	 * @param parent
	 *            父组件
	 * @param model
	 *            是否模式
	 */
	public SettingDialog(JFrame parent, boolean model) {
		super(parent, model);
		this.initCompents();
		this.processEvents();
	}

	/**
	 * 初始化组件
	 */
	private void initCompents() {
		this.setTitle(title);
		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		this.initCenterPanel();
		this.add(panel1, BorderLayout.CENTER);
		this.initSouthPanel();
		this.add(panel2, BorderLayout.SOUTH);
	}

	/**
	 * 初始化中部面板
	 */
	private void initCenterPanel() {
		this.panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		this.tabbedPane = new JTabbedPane();
		// 创建索引面板
		this.initCreateIndexPanel();
		this.tabbedPane.add(this.panel3, "创建索引");
		// 搜索设置
		this.initSearchSetPanel();
		this.tabbedPane.add(this.panel4, "搜索设置");
		// 首选项设置
		this.initFirstSettingPanel();
		this.tabbedPane.add(this.panel5, "首选项");
		panel1.add(this.tabbedPane, BorderLayout.CENTER);
	}

	/**
	 * 初始化首选项面板
	 */
	private void initFirstSettingPanel() {
		this.panel5 = new JPanel();
	}

	/**
	 * 初始化搜索设置面板
	 */
	private void initSearchSetPanel() {
		this.panel4 = new JPanel();
	}

	/**
	 * 初始化创建索引面板
	 */
	private void initCreateIndexPanel() {
		this.panel3 = new JPanel();
		this.panel3.setLayout(new BorderLayout());
		JLabel la=new JLabel("<html>第一步: <b>选择创建索引的目录</b>  (共三步)</html>");
		la.setForeground(Color.GRAY);
		panel3.add(la,BorderLayout.NORTH);
		JPanel jp1=new JPanel();
		CardLayout clayout=new CardLayout();
		jp1.setLayout(clayout);
		jp1.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		panel3.add(jp1,BorderLayout.CENTER);
		JPanel jp2=new JPanel();
		jp2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		this.button2=new JButton("<<上一步");
		this.button3=new JButton("下一步>>");
		this.button4=new JButton("创建索引");
		jp2.add(button2);
		jp2.add(button3);
		jp2.add(button4);
		panel3.add(jp2,BorderLayout.SOUTH);
		
	}

	/**
	 * 南部面板
	 */
	private void initSouthPanel() {
		this.panel2 = new JPanel();
		this.panel2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		this.progressBar=new JProgressBar();
		this.progressBar.setVisible(false);
		this.panel2.add(this.progressBar);
		this.label1 = new JLabel();
		this.setInfo("状态栏", Color.LIGHT_GRAY);
		this.panel2.add(this.label1);
		this.panel2.add(Box.createHorizontalStrut(10));
		this.button1 = new JButton("关闭");
		this.button1.setToolTipText("关闭");
		this.button1.setForeground(Color.BLUE);
		this.panel2.add(this.button1);
		this.panel2.add(Box.createHorizontalStrut(10));
	}

	/**
	 * 事件处理
	 */
	private void processEvents() {
		// 关闭事件
		this.button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingDialog.this.dispose();
			}
		});

	}

	/**
	 * 设置状态栏信息
	 * 
	 * @param info
	 * @param c
	 */
	private void setInfo(String info, Color c) {
		this.label1.setText(info);
		this.label1.setForeground(c);
	}

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				SystemUtil.setUI();
				SettingDialog dialog = new SettingDialog(
						new javax.swing.JFrame(), true);
				dialog.setLocation(300, 200);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

}
