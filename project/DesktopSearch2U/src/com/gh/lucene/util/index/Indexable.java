/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.index;

import com.gh.lucene.util.UserData;

/**
 * 
 * @author Administrator
 */
public interface Indexable {

	public void index(UserData document) throws IndexException;

	public void commit();

	public void close() throws IndexException;
}
