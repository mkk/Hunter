package com.gh.lucene.util.index;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;

import com.gh.lucene.util.UserData;
import com.gh.lucene.util.mapping.Mapper;
import com.gh.lucene.util.store.UserDocument;

/**
 * 这个类表示只能一种类需要索引和搜索时。
 * 
 * @author bastengao
 * 
 */
public class OnlyOneIndexer extends Indexer {
	public OnlyOneIndexer(IndexWriter indexWriter) {
		super(indexWriter);
	}

	private Mapper<UserData> mapper;

	@Override
	protected UserDocument map(UserData userData) {
		Document document = mapper.indexMap(userData);
		return assemble(userData, document);
	}

	/**
	 * 设置这个类的索引搜索映射类
	 * 
	 * @param mapper
	 */
	public void setMapper(Mapper<UserData> mapper) {
		this.mapper = mapper;
	}

}
