package com.gh.lucene.util.index;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;

import com.gh.lucene.util.UserData;
import com.gh.lucene.util.mapping.Mapper;
import com.gh.lucene.util.mapping.Resolver;
import com.gh.lucene.util.store.UserDocument;

/**
 * 实现有多个索引搜索数据， 可以过此类的setResolver(Resolver) 来设置关于这些类的映射解析器
 * 
 * @author bastengao
 * 
 */
public class MultiIndexer extends Indexer {
	public MultiIndexer(IndexWriter indexWriter) {
		super(indexWriter);
	}

	private Resolver resolver = null;

	@Override
	protected UserDocument map(UserData userData) {
		// TODO
		Mapper<UserData> mapper = resolver.getMapper(userData);
		if (mapper == null) {
			throw new IllegalStateException("could not find mapper for"
					+ userData.getClass().getName());
		}
		Document document = mapper.indexMap(userData);
		return assemble(userData, document);
	}

	public void setResolver(Resolver resolver) {
		this.resolver = resolver;
	}

}
