package com.gh.lucene.util.store;

import org.apache.lucene.document.Document;

import com.gh.lucene.util.UserData;

public interface UserDocumentFactory {

	/**
	 * 组织一下 UserDocument 根据 UserData 和 Document
	 * 
	 * @param userData
	 * @param document
	 * @return
	 */
	public UserDocument assemble(UserData userData, Document document);
}
