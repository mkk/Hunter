package com.gh.lucene.util.store;

import org.apache.lucene.document.Field;

public class OnlyStoreFieldWriteStrategy implements FieldWriteStrategy {

	@Override
	public Field createField(String fieldName, Object value) {
		if (value instanceof String) {
			return new Field(fieldName, (String) value, Field.Store.YES,
					Field.Index.NO);
		} else {
			throw new IllegalArgumentException("unknow type of value.value must be String.  value: "+value);
		}
	}
}
