package com.gh.lucene.util.store;

import org.apache.lucene.document.Document;

import com.gh.lucene.util.UserData;

public class SimpleUserDocumentFactory implements UserDocumentFactory {

	@Override
	public UserDocument assemble(UserData userData, Document document) {

		return new SimpleDocument(document);
	}

}
