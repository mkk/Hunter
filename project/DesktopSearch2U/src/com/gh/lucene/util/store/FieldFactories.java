/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.store;

import java.util.Map;

/**
 * this class use FlyWeight pattern ,common fieldFactory are cached is this class.
 * So it is should to get fieldFactory from this class rather than other approaches.
 * note: this class is Singleton
 * @author Basten.Gao
 */
public class FieldFactories {

    public static final String CONTENT_FIELD_FACTORY = "com.gh.lucene.util.ContentFieldFactory";
    public static final String METAL_FIELD_FACTORY = "com.gh.lucene.util.store.MetalFieldFactory";
    public static final String ONLY_STORE_FIELD_FACTORY = "com.gh.lucene.util.store.MetalFieldFactory";
    private Map<String, FieldWriteStrategy> factoryHolder = null;
    private static FieldFactories instance;

    public static synchronized FieldFactories getInstance() {
        if (instance == null) {
            instance = new FieldFactories();
        }
        return instance;
    }


    /**
     * get cached fieldFactory by its unique name
     * @param name
     * @return
     * @throws FieldFactoryNotFoundException when specified fieldFactory is not found
     */
    public FieldWriteStrategy getFieldFactory(String name) throws FieldFactoryNotFoundException {
        FieldWriteStrategy factory = null;
        factory = factoryHolder.get(name);
        if (factory == null) {
            try {
                Class<?> clazz = Class.forName(name);
                factory = (FieldWriteStrategy) clazz.newInstance();
            } catch (Exception ex) {
                FieldFactoryNotFoundException exx = new FieldFactoryNotFoundException();
                exx.initCause(ex);
                throw exx;
            }
        }
        return factory;
    }
    
    /**
     * @param name
     * @param strategy
     * @return
     */
    public boolean bind(String name,FieldWriteStrategy strategy) {
    	return false;
    }
}
