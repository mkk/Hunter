/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.store;

import com.gh.lucene.util.store.FieldWriteStrategy;
import java.io.Reader;
import org.apache.lucene.document.Field;

/**
 * 
 * @author Administrator
 *
 */
public class ContentFieldWriteStrategy implements FieldWriteStrategy {

	
    @Override
    public Field createField(String fieldName, Object value) {
        if (value instanceof String) {
            String value2 = (String) value;
            return new Field(fieldName, value2, Field.Store.NO, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS);
        } else if (value instanceof Reader) {
            Reader reader = (Reader) value;
            return new Field(fieldName, reader, Field.TermVector.WITH_POSITIONS_OFFSETS);
        } else {
            throw new IllegalArgumentException("unknow type of value.  value: "+value);
        }
    }
}
