/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.store;

import org.apache.lucene.document.Document;

/**
 * 为什么要这个类，多绕了一圈，是因为我想直接返回一个 Document ，可能有些直接，
 * 伴随着 Document ，可能还有一些其他的信息，所以我就写了这么一个类，日前只有一方法，
 * 我想以后要是还想加一些特性的化，就可以直接在这里加方法了。比如针对某种 Document ，
 * 使用某种特定的 Analyzer 。
 * @author Administrator
 */
public interface UserDocument {

	public Document getDocument() throws DocumentException;
}
