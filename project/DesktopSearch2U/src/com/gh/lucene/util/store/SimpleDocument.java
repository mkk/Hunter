package com.gh.lucene.util.store;

import org.apache.lucene.document.Document;

public class SimpleDocument implements UserDocument {
	private Document document = null;

	public SimpleDocument(Document document) {
		super();
		this.document = document;
	}

	@Override
	public Document getDocument() throws DocumentException {
		return document;
	}

}
