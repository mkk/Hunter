/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.store;

/**
 *
 * @author Administrator
 */
public class FieldFactoryNotFoundException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new instance of <code>FieldFactoryNotFoundException</code> without detail message.
     */
    public FieldFactoryNotFoundException() {
    }

    /**
     * Constructs an instance of <code>FieldFactoryNotFoundException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public FieldFactoryNotFoundException(String msg) {
        super(msg);
    }
}
