/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.lucene.util.store;

import org.apache.lucene.document.Field;

/**
 *
 * @author Administrator
 */
public class MetalFieldWriteStrategy implements FieldWriteStrategy {

    public Field createField(String fieldName, Object value) {
        if (value instanceof String) {
            return new Field(fieldName, (String) value, Field.Store.YES, Field.Index.NOT_ANALYZED);
        } else {
            throw new IllegalArgumentException("the value must be String");
        }
    }
}
