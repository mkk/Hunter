package com.gh.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.gh.lucene.util.UserData;

public class ResolverSearchMapper implements SearchMapper {
	private Resolver resolver = null;

	public ResolverSearchMapper(Resolver resolver) {
		this.resolver = resolver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserData searchMap(Document document) {
		Mapper mapper = resolver.getMapper(document);
		if (mapper == null) {
			return null;
		}
		return mapper.searchMap(document);
	}

}
