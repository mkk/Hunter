package com.gh.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.gh.lucene.util.UserData;

/**
 * 负责统一的映射
 * 
 * @author bastengao
 * 
 */
public interface SearchMapper {
	public UserData searchMap(Document document);
}
