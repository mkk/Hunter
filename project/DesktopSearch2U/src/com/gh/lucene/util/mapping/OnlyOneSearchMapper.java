package com.gh.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.gh.lucene.util.UserData;

public class OnlyOneSearchMapper implements SearchMapper {

	@SuppressWarnings("unchecked")
	private Mapper mapper = null;
	private Suiteable suiteable = null;

	@SuppressWarnings("unchecked")
	public OnlyOneSearchMapper(Mapper mapper, Suiteable suiteable) {
		this.mapper = mapper;
		this.suiteable = suiteable;
	}

	@Override
	public UserData searchMap(Document document) {
		if (suiteable.isSuite(document)) {
			return mapper.searchMap(document);
		} else {
			return null;
		}
	}

	public void setSuiteable(Suiteable suiteable) {

	}

}
