package com.gh.lucene.util.search.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;

public class AllWhioutScoreSortingCollector extends Collector {
	List<Integer> docs = new ArrayList<Integer>();
	int docBase = 0;

	@Override
	public boolean acceptsDocsOutOfOrder() {
		return false;
	}

	@Override
	public void collect(int doc) throws IOException {
		docs.add(docBase + doc);
	}

	@Override
	public void setNextReader(IndexReader reader, int docBase)
			throws IOException {
		this.docBase = docBase;
	}

	@Override
	public void setScorer(Scorer scorer) throws IOException {
	}

	public List<Integer> docs() {
		return docs;
	}

}
