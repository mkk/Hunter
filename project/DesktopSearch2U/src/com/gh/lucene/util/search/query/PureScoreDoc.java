package com.gh.lucene.util.search.query;

import org.apache.lucene.search.ScoreDoc;

public class PureScoreDoc extends ScoreDoc {

	private static final long serialVersionUID = 1L;

	public PureScoreDoc(int doc, float score) {
		super(doc, score);
	}
}
