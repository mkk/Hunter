package com.gh.lucene.util.search.query;

import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.util.PriorityQueue;

public class ScorePriorityQueue extends PriorityQueue<ScoreDoc> {

	@Override
	protected boolean lessThan(ScoreDoc docA, ScoreDoc docB) {

		return docA.score > docB.score ? false : docA.score < docB.score ? true
				: false;
	}

}
