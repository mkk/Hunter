package com.gh.lucene.util.search;

import java.util.List;

import com.gh.lucene.util.UserData;

/**
 * 此类代表一次搜索会话。 因为考虑到分页，而分页可以理解为一次搜索会话，所以就把它抽象为一个类.
 * 
 * @author bastengao
 * 
 */
public interface QuerySession {

	/**
	 * 执行查询
	 * @throws SearchException 
	 */
	public void execute() throws SearchException;

	/**
	 * 有没有更多结果
	 * 
	 * @return
	 */
	public boolean hasMore();

	/**
	 * 得到当前结果，并把游标位置移到下一个
	 * 
	 * @return
	 * @throws SearchException 
	 */
	public UserData next() throws SearchException;

	/**
	 * 得到 length 长度的结果数。但实际的返回的结果数可能要小于需求的结果数。
	 * 
	 * @param length
	 * @return
	 * @throws SearchException 
	 */
	public List<UserData> next(int length) throws SearchException;

	/**
	 * 关闭此次搜索会话。搜索会话一但关闭，就不可用了。
	 */
	public void close();
}
