package com.gh.lucene.util.search;

import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;

import com.gh.lucene.util.mapping.SearchMapper;

public class Searcher implements Seachable {
	private SearchWorker worker = null;
	private SearchMapper searchMapper = null;

	public Searcher(SearchWorker worker, SearchMapper searchMapper) {
		super();
		this.worker = worker;
		this.searchMapper = searchMapper;
	}

	@Override
	public void setUserDataSearchMapper(SearchMapper searchMapper) {
		this.searchMapper = searchMapper;
	}

	@Override
	public QuerySession createQuerySession(Query query) {
		if (query == null) {
			throw new IllegalArgumentException("query is null");
		}
		return new CollectAllWithoutScoreSoringQuerySession(query, worker,
				searchMapper);
	}

	@Override
	public QuerySession createQuerySession(Filter filter, Query query) {
		if (query == null) {
			throw new IllegalArgumentException("query is null");
		}
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		return new CollectAllWithoutScoreSoringQuerySession(query, filter,
				worker, searchMapper);
	}

	@Override
	public void close() {
		worker.close();
	}

}
