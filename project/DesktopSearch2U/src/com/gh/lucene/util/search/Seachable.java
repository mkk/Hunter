package com.gh.lucene.util.search;

import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;

import com.gh.lucene.util.mapping.SearchMapper;

public interface Seachable {
	public QuerySession createQuerySession(Query query);

	public QuerySession createQuerySession(Filter filter, Query query);

	public void setUserDataSearchMapper(SearchMapper searchMapper);

	
	public void close();
}
