package com.gh.lucene.util.search.query;

import java.io.IOException;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.TopDocsCollector;

public class RangeCollector extends TopDocsCollector<ScoreDoc> {
	private float lastScore = 0;

	public static RangeCollector createCollector() {
		ScorePriorityQueue queue = new ScorePriorityQueue();
		return new RangeCollector(queue);
	}

	public RangeCollector(ScorePriorityQueue queue) {
		super(queue);
	}

	@Override
	public boolean acceptsDocsOutOfOrder() {
		return false;
	}

	@Override
	public void collect(int id) throws IOException {
		ScoreDoc scoreDoc = new ScoreDoc(id, lastScore);
		pq.add(scoreDoc);
	}

	@Override
	public void setNextReader(IndexReader arg0, int arg1) throws IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public void setScorer(Scorer score) throws IOException {
		lastScore = score.score();
	}

}
