package com.gh.deskTopSearch.file;

import com.gh.lucene.util.UserData;
import com.gh.lucene.util.mapping.TypeFieldResolver.Type;

/**
 * 文件索引搜索中表示文件类父类
 * 
 * @author bastengao
 * 
 */
public abstract class FileData implements UserData {
	private String fileName;
	private String absolutePath;
	private long lastModified;
	private boolean isHidden;

	public FileData() {
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public abstract Type getTypeField();
}
