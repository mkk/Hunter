package com.gh.deskTopSearch.file;

import java.io.File;

/**
 * 由 File 到 FileData 及其他一些类的工厂
 * 
 * @author bastengao
 * 
 */
public class FileDataFactory {
	/**
	 * 
	 * @param file
	 * @return
	 * @throws IllegalArgumentException
	 *             file is not file and also is not directory
	 */
	public FileData createFileData(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				return createRealDirectory(file);
			} else if (file.isFile()) {
				return createRealFile(file);
			} else {
				throw new IllegalArgumentException(
						"file is not file and also is not directory");
			}
		} else {
			//如果电脑有一些盘符，但那些盘符，没有东西就会出现，或者文件不存在。
			throw new IllegalArgumentException("file does not exists. "
					+ file.getAbsolutePath());
		}
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws IllegalArgumentException
	 *             if file is not file
	 */
	public RealFile createRealFile(File file) {
		if (!file.isFile()) {
			throw new IllegalArgumentException("file is not file");
		}
		RealFile realFile = new RealFile();
		realFile.setLength(file.length());
		realFile.setPostfix(FileTools.getPostfix(file.getName()));
		assembleCommonProperty(realFile, file);
		return realFile;
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws IllegalArgumentException
	 *             file is not directory
	 */
	public RealDirectory createRealDirectory(File file) {
		if (!file.isDirectory()) {
			throw new IllegalArgumentException("file is not directory");
		}
		RealDirectory realDirectory = new RealDirectory();
		assembleCommonProperty(realDirectory, file);
		return realDirectory;
	}

	private void assembleCommonProperty(FileData fileData, File file) {
		fileData.setFileName(file.getName());
		fileData.setAbsolutePath(file.getAbsolutePath());
		fileData.setHidden(file.isHidden());
		fileData.setLastModified(file.lastModified());
	}
}
