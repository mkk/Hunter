package com.gh.deskTopSearch.file.content;

import java.io.File;

/**
 * 内容选择器
 * 
 * @author bastengao
 * 
 */
public interface ContentChooser {
	/**
	 * 返回这个文件的内容是否应该被索引
	 * 
	 * @param file
	 * @return
	 */
	public boolean index(File file);

}
