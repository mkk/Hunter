package com.gh.deskTopSearch.file.content;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.gh.deskTopSearch.file.FileTools;

/**
 * 可以根据文件的后缀决定哪些文件的内容应该被索引
 * 
 * @author bastengao
 * 
 */
public class PostfixContentChooser implements ContentChooser {
	Set<String> postfixSet = null;

	/**
	 * 
	 * @param postfix
	 *            被索引文件名的后缀
	 */
	public PostfixContentChooser(Set<String> postfix) {
		this.postfixSet = postfix;
	}

	/**
	 * 指定苦于关键字
	 * 
	 * @param postfixes
	 */
	public PostfixContentChooser(String... postfixes) {
		this.postfixSet = new HashSet<String>(Arrays.asList(postfixes));
	}

	/**
	 * 如果文件名的后缀在后缀集中，则返回 true
	 * 
	 * @see com.gh.deskTopSearch.file.content.ContentChooser#index(java.io.File)
	 */
	@Override
	public boolean index(File file) {
		String fileName = file.getName();
		String postfix = FileTools.getPostfix(fileName);
		return postfixSet.contains(postfix);
	}

}
