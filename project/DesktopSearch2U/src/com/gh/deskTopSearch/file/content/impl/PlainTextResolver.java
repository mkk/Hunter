package com.gh.deskTopSearch.file.content.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import com.gh.deskTopSearch.file.content.ReaderResolveException;
import com.gh.deskTopSearch.file.content.ReaderResolver;

/**
 * txt,纯文本的读取实现
 * 
 * @author bastengao
 * 
 */
public class PlainTextResolver implements ReaderResolver {

	@Override
	public Reader read(File file) throws ReaderResolveException {
		try {
			return new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ReaderResolveException(e);
		}
	}

}
