/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.file.fetch;

import java.io.File;

/**
 * 利用装饰器模式，且可以表示为 Chooser 的一个链表,且他们的关系是且的关系，只有都被接受，才返回真
 * 
 * @author bastengao
 */
public class FileChooser implements Chooser {

	private Chooser chooser;// 当前选择策略
	private Chooser nextChooser;// 下一个选择

	public FileChooser(Chooser chooser) {
		this(chooser, null);
	}

	public FileChooser(Chooser chooser, Chooser nextChooser) {
		this.chooser = chooser;
		this.nextChooser = nextChooser;
	}

	@Override
	public boolean accept(File file) {
		if (chooser.accept(file)) {
			if (nextChooser != null) {
				return nextChooser.accept(file);
			}
		} else {
			return false;
		}
		return true;
	}
}
