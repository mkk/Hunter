package com.gh.deskTopSearch.file.fetch.support;

import java.io.File;

import com.gh.deskTopSearch.file.fetch.Chooser;

public class NotHidden implements Chooser {

	@Override
	public boolean accept(File file) {
		return !file.isHidden();
	}

}
