/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.file.fetch;

import java.util.Enumeration;

import com.gh.lucene.util.UserData;

/**
 * 抓取，与遍历的类
 * @author Administrator
 */
public interface Fecther<E extends UserData> {

	/**
	 * 返回 iterator
	 * @return
	 */
    public Enumeration<E> iterator();
}
