/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.file.fetch.support;

import java.io.File;

import com.gh.deskTopSearch.file.fetch.Chooser;

/**
 * 
 * @author Administrator
 */
public class FileOnly implements Chooser {

	@Override
	public boolean accept(File file) {
		return valueOf(file);
	}

	public boolean valueOf(File file) {
		return file.isFile();
	}
}
