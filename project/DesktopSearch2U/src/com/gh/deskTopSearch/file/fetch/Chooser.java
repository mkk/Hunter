/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.file.fetch;

import java.io.File;

/**
 * 文件选择
 * 
 * @author Administrator
 */
public interface Chooser {

	/**
	 * 决定些文件是否应该被索引
	 * 
	 * @param file
	 * @return
	 */
	public boolean accept(File file);
}
