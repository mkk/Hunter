package com.gh.deskTopSearch.file.fetch.support;

import java.io.File;

import com.gh.deskTopSearch.file.fetch.Chooser;

/**
 * 接受所有的文件
 * @author rjgc
 *
 */
public class AllFile implements Chooser {

	@Override
	public boolean accept(File file) {
		return true;
	}

}
