package com.gh.deskTopSearch.file.fetch.support;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.gh.deskTopSearch.file.FileTools;
import com.gh.deskTopSearch.file.fetch.Chooser;

public class PostfixFileChooser implements Chooser {

	private Set<String> postfixes = null;

	public PostfixFileChooser(String... postfixes) {
		this.postfixes = new HashSet<String>(Arrays.asList(postfixes));
	}

	@Override
	public boolean accept(File file) {
		if (file.isFile()) {
			String fileName = file.getName();
			String postfix = FileTools.getPostfix(fileName);
			return postfixes.contains(postfix);
		} else {
			return false;
		}
	}

}
