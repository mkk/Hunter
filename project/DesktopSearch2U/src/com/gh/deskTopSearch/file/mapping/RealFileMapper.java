package com.gh.deskTopSearch.file.mapping;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import com.gh.deskTopSearch.file.RealFile;
import com.gh.lucene.util.mapping.Mapper;
import static com.gh.deskTopSearch.file.FieldNameConstants.*;

public class RealFileMapper implements Mapper<RealFile> {

	private FileMapper fileMapper = new FileMapper();

	@Override
	public Document indexMap(RealFile e) {
		Document document = fileMapper.indexMap(e);

		Field length = new Field(LENGTH, String.valueOf(e.getLength()),
				Field.Store.YES, Field.Index.NO);

		Field postfix = new Field(POSTFIX, e.getPostfix(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);

		Field type = new Field(e.getTypeField().getFieldName(), e
				.getTypeField().getValue(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);

		document.add(length);
		document.add(postfix);
		document.add(type);
		return document;
	}

	@Override
	public RealFile searchMap(Document document) {
		RealFile realFile = new RealFile();
		fileMapper.searchMap(realFile, document);
		realFile.setLength(Long.parseLong(document.getField(LENGTH)
				.stringValue()));
		realFile.setPostfix(document.getField(POSTFIX).stringValue());
		return realFile;
	}

}
