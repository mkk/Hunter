package com.gh.deskTopSearch.file;

import com.gh.lucene.util.mapping.TypeFieldResolver.Type;

/**
 * 表示一个实际的文件夹
 * 
 * @author bastengao
 * 
 */
public class RealDirectory extends FileData {
	public static final Type TYPE = FileTools.REAL_DIRECTORY_TYPE;

	@Override
	public Type getTypeField() {

		return FileTools.REAL_DIRECTORY_TYPE;
	}

}
