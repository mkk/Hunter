/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.file;

/**
 * this class is used for declaring some constant string of fieldName in order
 * to index
 * 
 * @author Basten.Gao
 */
public enum FileType {

	FILE("file"), DIRECTORY("directory");

	FileType(String type) {
		this.typeValue = type;
	}

	public final String typeValue;

}
