package com.gh.deskTopSearch.search;

import java.io.IOException;
import java.util.Date;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.wltea.analyzer.lucene.IKQueryParser;

import com.gh.deskTopSearch.file.FieldNameConstants;
import com.gh.deskTopSearch.file.RealDirectory;
import com.gh.deskTopSearch.file.RealFile;
import com.gh.lucene.util.search.Queries;

/**
 * 返回各种查询，和过滤的类。 可以通过些类，得到一些有用的查询和过滤。但如果不能满足使用，可以自己创建合适的查询和过滤。
 * 查询是确定哪些东西应该被命中的条件。而过滤可以缩小范围，配合查询，使用查询更高效。但不能当查询使用。
 * 
 * @author rjgc
 * 
 */
public class SearchTools {

	/**
	 * 文件名的精确查询,即结果的文件名与指定的文件名相同。
	 * 
	 * @param fileName
	 * @return
	 */
	public static Query createFullFileNameQuery(String fileName) {
		return Queries.termQuery(FieldNameConstants.NAME, fileName);
	}

	public static Query createPartFileNameQuery(String partFileName) {
		return Queries
				.termQuery(FieldNameConstants.NAME_ANALYZED, partFileName);
	}

	public static Query crreateFileNameQueryAnalyzedByIK(String fileName)
			throws IOException {
		return IKQueryParser.parse(FieldNameConstants.NAME_ANALYZED, fileName);
	}

	/**
	 * 隐藏或者非隐藏的文件的查询
	 * 
	 * @param hidden
	 * @return
	 */
	public static Query crreateQueryFileIfHidden(boolean hidden) {
		return Queries.termQuery(FieldNameConstants.IS_HIDDEN, Boolean
				.toString(hidden));
	}

	/**
	 * 后缀的查询
	 * 
	 * @param postfix
	 * @return
	 */
	public static Query createPostfixQuery(String postfix) {
		return Queries.termQuery(FieldNameConstants.POSTFIX, postfix);
	}

	/**
	 * 针对纯文件的查询
	 * 
	 * @return
	 */
	public static Query createRealFileQuery() {
		return Queries.termQuery(FieldNameConstants.TYPE, RealFile.TYPE
				.getValue());
	}

	/**
	 * 针对文件夹的查询
	 * 
	 * @return
	 */
	public static Query createRealDirectory() {
		return Queries.termQuery(FieldNameConstants.TYPE, RealDirectory.TYPE
				.getValue());
	}

	/**
	 * 针对文件最后修改时间的区间查询
	 * 
	 * @param lower
	 * @param upper
	 * @return
	 */
	public static Query createDateRangeQuery(Date lower, Date upper) {
		return Queries.termRangeQuery(FieldNameConstants.LAST_MODIFY_DATE,
				DateTools.dateToString(lower, DateTools.Resolution.MINUTE),
				DateTools.dateToString(upper, DateTools.Resolution.MINUTE),
				true, true);
	}

	/**
	 * 匹配某个时间点的查询。如果 date 为 19881013,且 resolution 为 year。那么就会匹配 1988 那一年的结果。
	 * 注意：此系统中，resolution 只能由 year 到 minute。其它的 resolution,可能会出现不可预期的结果。
	 * 
	 * @param point
	 * @param resolution
	 * @return
	 */
	public static Query createDatePointQuery(Date point,
			DateTools.Resolution resolution) {
		return Queries.prefixQuery(FieldNameConstants.LAST_MODIFY_DATE,
				DateTools.dateToString(point, resolution));
	}

	/**
	 * 针对纯文件的过滤器
	 * 
	 * @return
	 */
	public static Filter createOnlyFileFilter() {
		Query query = Queries.termQuery(FieldNameConstants.TYPE, RealFile.TYPE
				.getValue());
		return new QueryWrapperFilter(query);
	}

	/**
	 * 针对文件夹的过滤器
	 * 
	 * @return
	 */
	public static Filter createOnlyDirectoryFilter() {
		Query query = Queries.termQuery(FieldNameConstants.TYPE,
				RealDirectory.TYPE.getValue());
		return new QueryWrapperFilter(query);
	}
}
