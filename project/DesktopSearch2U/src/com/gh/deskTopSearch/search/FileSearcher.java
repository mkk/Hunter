package com.gh.deskTopSearch.search;

import java.io.IOException;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;

import com.gh.deskTopSearch.file.RealDirectory;
import com.gh.deskTopSearch.file.RealFile;
import com.gh.deskTopSearch.file.mapping.RealDirectoryMapper;
import com.gh.deskTopSearch.file.mapping.RealFileMapper;
import com.gh.lucene.util.Lucenes;
import com.gh.lucene.util.UserData;
import com.gh.lucene.util.mapping.ResolverSearchMapper;
import com.gh.lucene.util.mapping.SearchMapper;
import com.gh.lucene.util.mapping.TypeFieldResolver;
import com.gh.lucene.util.search.QuerySession;
import com.gh.lucene.util.search.SearchException;
import com.gh.lucene.util.search.SearchWorker;
import com.gh.lucene.util.search.Searcher;

/**
 * 搜索的核心类。
 * 
 * @author rjgc
 * 
 */
public class FileSearcher {
	private Searcher searcher = null;

	/**
	 * 
	 * @param indexPath
	 *            索引所在路径
	 * 
	 */
	public FileSearcher(String indexPath) {
		Directory directory = null;
		try {
			directory = Lucenes.getSimpleFileSystemDirectory(indexPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		IndexSearcher indexSearcher = null;
		try {
			indexSearcher = new IndexSearcher(directory, true);
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		SearchWorker searchWorker = null;
		searchWorker = new SearchWorker(indexSearcher);
		TypeFieldResolver resolver = new TypeFieldResolver();
		resolver
				.bindMapper(new RealFileMapper(), RealFile.class, RealFile.TYPE);
		resolver.bindMapper(new RealDirectoryMapper(), RealDirectory.class,
				RealDirectory.TYPE);
		SearchMapper searchMapper = new ResolverSearchMapper(resolver);

		searcher = new Searcher(searchWorker, searchMapper);
	}

	/**
	 * 返回一个搜索会话。
	 * 
	 * @param query
	 * @return
	 */
	public QuerySession createQuerySession(Query query) {
		return searcher.createQuerySession(query);
	}

	/**
	 * 返回一个搜索会话。
	 * 
	 * @param query
	 * @param filter
	 * @return
	 */
	public QuerySession createQuerySession(Query query, Filter filter) {
		return searcher.createQuerySession(filter, query);
	}

	public static void main(String[] args) throws SearchException {
		FileSearcher searcher = new FileSearcher("E:\\desktopSearchIndex");

		Query query = SearchTools.createPostfixQuery("txt");
		// QuerySession querySession = searcher.createQuerySession(query,
		// createOnlyFileFilter());
		QuerySession querySession = searcher.createQuerySession(query);
		querySession.execute();

		int count = 0;
		while (querySession.hasMore()) {
			// List<UserData> files = (List<UserData>) querySession.next(10);
			// for (UserData userData : files) {
			UserData userData = querySession.next();
			if (userData instanceof RealFile) {
				RealFile file = (RealFile) userData;
				System.out.printf(" Postfix[%-3s] Name[%-20s] Path[%s]%n", file
						.getPostfix(), file.getFileName(), file
						.getAbsolutePath());
			}
			count++;
			// }
		}
		System.out.println(count);
		querySession.close();
	}

	public void close() {
		searcher.close();
	}
}
