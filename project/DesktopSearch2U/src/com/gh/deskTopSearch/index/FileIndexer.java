/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gh.deskTopSearch.index;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;

import com.gh.deskTopSearch.file.FileData;
import com.gh.deskTopSearch.file.FileTools;
import com.gh.deskTopSearch.file.RealDirectory;
import com.gh.deskTopSearch.file.RealFile;
import com.gh.deskTopSearch.file.fetch.FileChooser;
import com.gh.deskTopSearch.file.fetch.FileFetcher;
import com.gh.deskTopSearch.file.fetch.support.FileOnly;
import com.gh.deskTopSearch.file.fetch.support.PostfixFileChooser;
import com.gh.deskTopSearch.file.mapping.RealDirectoryMapper;
import com.gh.deskTopSearch.file.mapping.RealFileMapper;
import com.gh.lucene.util.Lucenes;
import com.gh.lucene.util.index.IndexException;
import com.gh.lucene.util.index.Indexable;
import com.gh.lucene.util.index.MultiIndexer;
import com.gh.lucene.util.mapping.TypeFieldResolver;

/**
 * 负责文件索引的重要的类。索引类的核心。
 * @author Administrator
 */
public class FileIndexer {
	private Indexable indexer = null;

	public static void main(String[] args) throws IndexException {
		// example
		FileFetcher ff = new FileFetcher(new File[] {new File("D:\\")},new FileChooser(new FileOnly(),new PostfixFileChooser("txt")));
		Enumeration<FileData> fd = ff.iterator();
		FileIndexer fileIndexer = null;
		try {
			fileIndexer = FileIndexer
					.createFileIndexer("E:\\desktopSearchIndex",true);
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (fd.hasMoreElements()) {
			fileIndexer.index(fd.nextElement());
		}

		fileIndexer.close();
	}

	public FileIndexer(IndexWriter indexWriter) {
		super();
		MultiIndexer indexer = new MultiIndexer(indexWriter);
		TypeFieldResolver resolver = new TypeFieldResolver();
		resolver.bindMapper(new RealDirectoryMapper(), RealDirectory.class,
				FileTools.REAL_DIRECTORY_TYPE);
		resolver.bindMapper(new RealFileMapper(), RealFile.class,
				FileTools.REAL_FILE_TYPE);
		indexer.setResolver(resolver);
		this.indexer = indexer;
	}

	public void index(FileData fileData) throws IndexException {
		indexer.index(fileData);
	}

	/**
	 * 关闭写索引,在创建完索引后必须调用此方法关闭
	 * @throws IndexException
	 */
	public void close() throws IndexException {
		indexer.close();
	}

	/**
	 * 
	 * @param directory 创建索引的目录,可以通过Lucenes类工厂方法创建
	 *                             此对象可以在创建索引与搜索时使用
	 * @param create 是否新建
	 * @return
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public static FileIndexer createFileIndexer(Directory directory,
			boolean create) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		Analyzer analyzer = Lucenes.getChineseAnaylzer();
		IndexWriter indexWriter = Lucenes.createIndexWriter(directory,
				analyzer, create);
		return new FileIndexer(indexWriter);
	}

	public static FileIndexer createFileIndexer(Directory directory)
			throws CorruptIndexException, LockObtainFailedException,
			IOException {
		Analyzer analyzer = Lucenes.getChineseAnaylzer();
		IndexWriter indexWriter = Lucenes
				.createIndexWriter(directory, analyzer);
		return new FileIndexer(indexWriter);
	}

	/**
	 * 创建FileIndexer
	 * @param directoryPath  索引文件存放目录
	 * @return
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public static FileIndexer createFileIndexer(String directoryPath)
			throws CorruptIndexException, LockObtainFailedException,
			IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(directoryPath);
		Analyzer analyzer = Lucenes.getChineseAnaylzer();
		IndexWriter indexWriter = Lucenes
				.createIndexWriter(directory, analyzer);
		indexWriter.setInfoStream(System.out);
		return new FileIndexer(indexWriter);
	}

	/**
	 * 创建FileIndexer
	 * @param directoryPath  索引文件存放目录
	 * @param create 是否重新建立索引
	 * @return
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public static FileIndexer createFileIndexer(String directoryPath,
			boolean create) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(directoryPath);
		Analyzer analyzer = Lucenes.getChineseAnaylzer();
		IndexWriter indexWriter = Lucenes.createIndexWriter(directory,
				analyzer, create);
		indexWriter.setInfoStream(System.out);
		return new FileIndexer(indexWriter);
	}
	
	/**
	 * 创建FileIndexer
	 * @param directoryPath  索引文件存放目录
	 * @param create 是否重新建立索引
	 * @param ps 后台信息输出流
	 * @return
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public static FileIndexer createFileIndexer(String directoryPath,
			boolean create,PrintStream ps) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		Directory directory = Lucenes
				.getSimpleFileSystemDirectory(directoryPath);
		Analyzer analyzer = Lucenes.getChineseAnaylzer();
		IndexWriter indexWriter = Lucenes.createIndexWriter(directory,
				analyzer, create);
		indexWriter.setInfoStream(ps);
		return new FileIndexer(indexWriter);
	}

}
