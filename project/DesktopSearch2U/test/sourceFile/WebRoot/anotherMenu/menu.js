
$(document).ready(function () {
	$("#menu > li > ul").hide().click(function (e) {
		e.stopPropagation();
	});
	$("#menu > li").toggle(function () {
		$(this).find("ul").stop(false,true).slideDown();
	}, function () {
		$(this).find("ul").stop(false,true).slideUp();
	});
});

