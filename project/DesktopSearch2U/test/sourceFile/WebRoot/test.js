
$(document).ready(function() {
	
	var narrowFun=function(event) {
		$('body').removeClass().addClass('narrow');		
	};
	
	$('#switcher-default').bind('click', function(event) {
		$('body').removeClass();		
	});
	$('#switcher-narrow').bind('click.narrow',narrowFun );
	$('#switcher-large').bind('click', function(event) {
		$('body').removeClass().addClass('large');
		
	});
	$('#switcher').children().not("h3").bind('click', function(event) {
		$('#switcher').children().not("h3").removeClass('selected');
		$(this).addClass('selected');		
	});
	
	$('#switcher-default').trigger("click");
	
	$('#switcher').children().not("h3").hover(function(){
		$(this).addClass("hover");
	},function (){
		$(this).removeClass("hover");
	});
	
	$("#switcher h3").click(function(){		
		$("#switcher > div").toggleClass("hidden");		
	});
	
	$("a").click(function(event){
		event.preventDefault();
	});
	
	 $(document).keyup(function(event) {
    switch (String.fromCharCode(event.keyCode)) {
      case 'D':
        $('#switcher-default').click();
        break;
      case 'N':
        $('#switcher-narrow').click();
        break;
      case 'L':
        $('#switcher-large').click();
        break;
    }
  });
});