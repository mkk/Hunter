//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.09.04 at 10:58:36 ���� CST 
//


package com.hunter.schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TermVectorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TermVectorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NO"/>
 *     &lt;enumeration value="YES"/>
 *     &lt;enumeration value="WITH_OFFSETS"/>
 *     &lt;enumeration value="WITH_POSITIONS"/>
 *     &lt;enumeration value="WITH_POSITIONS_OFFSETS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TermVectorType")
@XmlEnum
public enum TermVectorType {

    NO,
    YES,
    WITH_OFFSETS,
    WITH_POSITIONS,
    WITH_POSITIONS_OFFSETS;

    public String value() {
        return name();
    }

    public static TermVectorType fromValue(String v) {
        return valueOf(v);
    }

}
