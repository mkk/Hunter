//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.09.04 at 10:58:36 ���� CST 
//


package com.hunter.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AnalyzerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnalyzerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="analyzerClass" type="{http://www.hunter.com/schema}ClassType"/>
 *         &lt;element name="analyzerFactory" type="{http://www.hunter.com/schema}AnalyzerFactory"/>
 *       &lt;/choice>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnalyzerType", propOrder = {
    "analyzerClass",
    "analyzerFactory"
})
public class AnalyzerType {

    protected ClassType analyzerClass;
    protected AnalyzerFactory analyzerFactory;
    @XmlAttribute(required = true)
    protected String name;

    /**
     * Gets the value of the analyzerClass property.
     * 
     * @return
     *     possible object is
     *     {@link ClassType }
     *     
     */
    public ClassType getAnalyzerClass() {
        return analyzerClass;
    }

    /**
     * Sets the value of the analyzerClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassType }
     *     
     */
    public void setAnalyzerClass(ClassType value) {
        this.analyzerClass = value;
    }

    /**
     * Gets the value of the analyzerFactory property.
     * 
     * @return
     *     possible object is
     *     {@link AnalyzerFactory }
     *     
     */
    public AnalyzerFactory getAnalyzerFactory() {
        return analyzerFactory;
    }

    /**
     * Sets the value of the analyzerFactory property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnalyzerFactory }
     *     
     */
    public void setAnalyzerFactory(AnalyzerFactory value) {
        this.analyzerFactory = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
