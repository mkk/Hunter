//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.09.04 at 10:58:36 ���� CST 
//


package com.hunter.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchBeanType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchBeanType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="discriminator" type="{http://www.hunter.com/schema}DiscriminatorType"/>
 *         &lt;element name="field" type="{http://www.hunter.com/schema}SearchBeanFieldType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="class" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchBeanType", propOrder = {
    "discriminator",
    "field"
})
public class SearchBeanType {

    @XmlElement(required = true)
    protected DiscriminatorType discriminator;
    @XmlElement(required = true)
    protected List<SearchBeanFieldType> field;
    @XmlAttribute(name = "class", required = true)
    protected String clazz;

    /**
     * Gets the value of the discriminator property.
     * 
     * @return
     *     possible object is
     *     {@link DiscriminatorType }
     *     
     */
    public DiscriminatorType getDiscriminator() {
        return discriminator;
    }

    /**
     * Sets the value of the discriminator property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscriminatorType }
     *     
     */
    public void setDiscriminator(DiscriminatorType value) {
        this.discriminator = value;
    }

    /**
     * Gets the value of the field property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the field property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchBeanFieldType }
     * 
     * 
     */
    public List<SearchBeanFieldType> getField() {
        if (field == null) {
            field = new ArrayList<SearchBeanFieldType>();
        }
        return this.field;
    }

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

}
