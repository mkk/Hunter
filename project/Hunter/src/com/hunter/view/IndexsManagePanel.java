package com.hunter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.hunter.util.LogUtil;
import com.hunter.util.SystemUtil;

/**
 * 2010-9-25 下午05:09:19<br>
 * 索引管理面板
 */
public class IndexsManagePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private SettingDialog main;
	// panel
	private JPanel panel1;
	// jeditpane
	private JEditorPane editPane;

	/**
	 * constructor
	 * 
	 * @param main
	 */
	public IndexsManagePanel(SettingDialog main) {
		this.main = main;
		this.initComponents();
		this.processEvents();
	}

	/**
	 * 初始化组件
	 */
	private void initComponents() {
		this.setLayout(new BorderLayout());
		// north
		this.panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2, 1, 5, 5));
		JLabel label = new JLabel(" 索引管理");
		label.setFont(new Font("宋体", Font.PLAIN, 16));
		label.setForeground(new Color(0, 0, 204));
		panel1.add(label);
		JSeparator sep = new JSeparator(SwingConstants.HORIZONTAL);
		panel1.add(sep);
		this.add(panel1, BorderLayout.NORTH);
		// center
		this.editPane = new JEditorPane();
		this.editPane.setContentType("text/html");
		this.editPane.setEditable(false);
		this.editPane.setText(getHtmlInfo());
		this.add(new JScrollPane(this.editPane), BorderLayout.CENTER);

	}

	/**
	 * 获取显示的HTML信息
	 * 
	 * @return
	 */
	private String getHtmlInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("<body bgcolor='#eeeeee' style='font-family:宋体,font-size:13'>");
		sb.append("在索引管理项,可选择以下操作:");
		sb.append("<br><ul>");
		sb
				.append("  <li>创建索引.创建索引是搜索的必要步骤,<a href='create'>点此</a>创建索引<p></li>");
		sb
				.append("  <li>对于已创建的索引文件,可以在管理创建的索引中进行管理,包括索引更新,索引删除等.<a href='manage'>点此</a>管理创建的索引<p></li>");
		File f = SystemUtil.getIndexFile();
		sb.append("   <li>索引文件保存在 <i>" + f.getAbsolutePath()
				+ "</i> 目录中,<a href='open'>点此</a>打开并查看索引文件<p></li>");
		sb.append("</ul>");
		sb.append("</body>");
		return sb.toString();
	}

	/**
	 * 事件处理
	 */
	private void processEvents() {
		// 超链接点击事件
		this.editPane.addHyperlinkListener(new HyperlinkListener() {

			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					String s = e.getDescription();
					if ("create".equalsIgnoreCase(s)) {
						// 创建索引面板
						main.showPanel(SettingDialog.CARD_CREATE_INDEX);
						main.showTreeNode("创建索引");
					} else if ("manage".equalsIgnoreCase(s)) {
						// 管理
						main.showPanel(SettingDialog.CARD_MANAGE_INDEX);
						main.showTreeNode("管理创建的索引");
					} else if ("open".equalsIgnoreCase(s)) {
						// 打开
						try {
							File f = SystemUtil.getIndexFile();
							SystemUtil.openFile(f.getAbsolutePath());
						} catch (IOException e1) {
							e1.printStackTrace();
							main.setInfo("打开异常!", Color.RED);
							LogUtil.saveExceptionLog(e1);
						}
					}
				}

			}
		});

	}

}
