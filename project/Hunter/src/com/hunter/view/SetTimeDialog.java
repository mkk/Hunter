package com.hunter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.hunter.util.SystemUtil;
import com.mkk.swing.JTimeChooser;
import com.mkk.swing.SwingUtil;
import com.mkk.swing.SwingUtil.UIType;

/**
 * 2010-9-18 下午12:59:17<br>
 * 设置时间段界面
 * 
 */
public class SetTimeDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JLabel label1;
	private JFormattedTextField formatText1, formatText2;
	private JButton button1, button2, button3;
	private JPanel panel1, panel2, panel3;
	private Calendar cal, startCal, endCal;

	/**
	 * constructor
	 * 
	 * @param parent
	 * @param modal
	 */
	public SetTimeDialog(Dialog parent, boolean modal) {
		super(parent, modal);
		cal = Calendar.getInstance();
		endCal = cal;
		// init
		this.initComponents();
		this.setTime();
		// events
		this.processEvents();
	}

	/**
	 * 设置初始时间值
	 */
	private void setTime() {
		this.formatText2.setText(SystemUtil.formatCalendar(cal));
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		startCal = cal;
		this.formatText1.setText(SystemUtil.formatCalendar(cal));
	}

	/**
	 * 初始化组件
	 */
	private void initComponents() {
		this.setTitle("设置时间段");
		this.setSize(260, 130);
		this.setLayout(new BorderLayout(1, 1));
		// center
		panel1 = new JPanel(new GridLayout(2, 1, 1, 1));
		panel2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		label1 = new JLabel("开始时间:");
		DateFormat format = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
				DateFormat.MEDIUM);
		this.formatText1 = new JFormattedTextField(format);
		this.formatText1.setColumns(20);
		this.formatText1.setText(format.format(new Date()));
		this.button1 = new JButton("...");
		button1.setToolTipText("重新设置");
		panel2.add(this.label1);
		panel2.add(this.formatText1);
		panel2.add(this.button1);
		panel2.add(Box.createHorizontalStrut(3));
		panel1.add(panel2);
		panel2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		label1 = new JLabel("结束时间:");
		this.formatText2 = new JFormattedTextField(format);
		this.formatText2.setColumns(20);
		this.button2 = new JButton("...");
		button2.setToolTipText("重新设置");
		panel2.add(this.label1);
		panel2.add(this.formatText2);
		panel2.add(this.button2);
		panel2.add(Box.createHorizontalStrut(3));
		panel1.add(panel2);
		this.add(panel1, BorderLayout.CENTER);
		// south
		panel3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		label1 = new JLabel("默认为本月1号零时到现在");
		label1.setForeground(Color.LIGHT_GRAY);
		this.button3 = new JButton("确定");
		panel3.add(label1);
		panel3.add(this.button3);
		this.add(panel3, BorderLayout.SOUTH);

	}

	/**
	 * 获取开始时间
	 * 
	 * @return
	 */
	public Calendar getStart() {
		return startCal;
	}

	/**
	 * 获取结束时间
	 * 
	 * @return
	 */
	public Calendar getEnd() {
		return endCal;
	}

	/**
	 * 事件处理
	 */
	private void processEvents() {
		// start
		this.button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Point p = button1.getLocationOnScreen();
				JTimeChooser chooser = new JTimeChooser(SetTimeDialog.this,
						"开始时间", p);
				startCal = chooser.showTimeDialog();
				formatText1.setText(SystemUtil.formatCalendar(startCal));
			}
		});
		// end
		this.button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Point p = button2.getLocationOnScreen();
				JTimeChooser chooser = new JTimeChooser(SetTimeDialog.this,
						"结束时间", p);
				endCal = chooser.showTimeDialog();
				formatText2.setText(SystemUtil.formatCalendar(endCal));
			}
		});
		// ok
		this.button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetTimeDialog.this.dispose();

			}
		});

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				SwingUtil.setUI(UIType.WINDOWS);
				SetTimeDialog p = new SetTimeDialog(null, true);
				p.setLocation(300, 200);
				p.setVisible(true);

			}
		});
	}

}
