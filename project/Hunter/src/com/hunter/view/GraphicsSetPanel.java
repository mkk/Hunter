package com.hunter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * 2010-9-29  下午05:54:58<br>
 * 界面设置面板
 * 
 * @author mkk
 */
public class GraphicsSetPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private SettingDialog main;
	// panel
	private JPanel panel1;

	/**
	 * constructor
	 */
	public GraphicsSetPanel(SettingDialog main) {
		this.main = main;
		this.initComponents();
		this.processEvents();
	}

	/**
	 * 事件处理
	 */
	private void processEvents() {

	}

	/**
	 * 初始化组件
	 */
	private void initComponents() {
		this.setLayout(new BorderLayout());
		// north
		this.panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2, 1, 5, 5));
		JLabel label = new JLabel(" 界面设置");
		label.setFont(new Font("宋体", Font.PLAIN, 16));
		label.setForeground(new Color(0, 0, 204));
		panel1.add(label);
		JSeparator sep = new JSeparator(SwingConstants.HORIZONTAL);
		panel1.add(sep);
		this.add(panel1, BorderLayout.NORTH);
		// center

	}

}
