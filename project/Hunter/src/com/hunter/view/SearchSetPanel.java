package com.hunter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * 2010-9-26 下午04:08:30<br>
 * 搜索设置面板
 * 
 * @author mkk
 */
public class SearchSetPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private SettingDialog main;
	// panel
	private JPanel panel1;
	// editpanel
	private JEditorPane editPane;

	/**
	 * constructor
	 * 
	 * @param main
	 */
	public SearchSetPanel(SettingDialog main) {
		this.main = main;
		this.initComponents();
		this.processEvents();
	}

	/**
	 * 初始化组件
	 */
	private void initComponents() {
		this.setLayout(new BorderLayout());
		// north
		this.panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2, 1, 5, 5));
		JLabel label = new JLabel(" 搜索设置");
		label.setFont(new Font("宋体", Font.PLAIN, 16));
		label.setForeground(new Color(0, 0, 204));
		panel1.add(label);
		JSeparator sep = new JSeparator(SwingConstants.HORIZONTAL);
		panel1.add(sep);
		this.add(panel1, BorderLayout.NORTH);
		// center
		this.editPane = new JEditorPane();
		this.editPane.setContentType("text/html");
		this.editPane.setEditable(false);
		this.editPane.setText(getHtmlInfo());
		this.add(new JScrollPane(this.editPane), BorderLayout.CENTER);

	}

	/**
	 * 获取显示的HTML信息
	 * 
	 * @return
	 */
	private String getHtmlInfo() {
		StringBuilder sb = new StringBuilder();
		sb
				.append("<body bgcolor='#eeeeee' style='font-family:宋体,font-size:13'>");
		sb.append("在搜索设置项,可选择以下操作:");
		sb.append("<br><ul>");
		sb.append("  <li>设置默认搜索,<a href='default'>点此</a>进入<p></li>");
		sb
				.append("  <li>个人爱好,设置你自己的搜索习惯.<a href='personal'>点此</a>进入<p></li>");
		sb.append("</ul>");
		sb.append("</body>");
		return sb.toString();
	}

	/**
	 * 事件处理
	 */
	private void processEvents() {
		// 超链接点击事件
		this.editPane.addHyperlinkListener(new HyperlinkListener() {

			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					String s = e.getDescription();
					if ("default".equalsIgnoreCase(s)) {
						// 默认搜索
						main.showPanel(SettingDialog.CARD_DEFAULT_SEARCH_SET);
						main.showTreeNode("默认搜索");
					} else if ("personal".equalsIgnoreCase(s)) {
						// 个人爱好
						main.showPanel(SettingDialog.CARD_PERSONAL_SET);
						main.showTreeNode("个人爱好");
					} 
					//more else if
				}

			}
		});

	}

}
