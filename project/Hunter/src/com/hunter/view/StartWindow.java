package com.hunter.view;

import java.awt.BorderLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import com.hunter.util.SystemUtil;
import com.mkk.swing.SwingUtil;

/**
 * 2010-9-4 下午01:25:36<br>
 * 程序启动界面
 * 
 * @author mkk
 */
public class StartWindow {

	// default width and height
	private static final int DEFAULT_WIDTH = 405;
	private static final int DEFAULT_HEIGHT = 250;
	// window
	private JWindow window;
	private JLabel label1;
	private JProgressBar progressBar1;

	public StartWindow() {
		this.initComponents();
	}

	/**
	 * 初始化组件
	 */
	private void initComponents() {
		this.window = new JWindow();
		this.window.setLayout(new BorderLayout());
		this.window.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		Point p = SwingUtil.getStartPoint(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		this.window.setLocation(p);
		// logo
		this.label1 = new JLabel(SystemUtil.getStartupImage());
		this.window.add(this.label1, BorderLayout.CENTER);
		// progress
		this.progressBar1 = new JProgressBar(0,100);
		this.progressBar1.setVisible(false);
		this.window.add(this.progressBar1, BorderLayout.SOUTH);

	}

	/**
	 * 设置是否显示启动界面
	 * 
	 * @param isVisible
	 */
	public void setVisiable(boolean isVisible) {
		this.window.setVisible(isVisible);
	}

	/**
	 * @return the progressBar1
	 */
	public JProgressBar getProgressBar1() {
		return progressBar1;
	}

	public static void main(String[] args) {
		new Thread(new Runnable() {

			public void run() {
				StartWindow win = new StartWindow();
				win.setVisiable(true);
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
				win.getProgressBar1().setVisible(true);
				try {
					Thread.sleep(3000);
				} catch (Exception e) {
				}
				System.exit(0);

			}
		}).start();

	}

}
