package com.hunter.util;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.index.Writable;
import com.hunter.lucene.util.search.Searchable;
import com.hunter.lucene.util.search.SuggestionConsult;
import com.mkk.swing.SwingUtil;

/**
 * 2010-8-16 下午07:06:33<br>
 * 2010-09-18 修改 搜索对象管理类<br>
 * 缓存Configuration
 * 
 * @author mkk
 */
public class ConfigManager {

	private static Logger log = Logger.getLogger(ConfigManager.class);
	// Cache object
	private static ConfigManager man = null;

	private ConfigManager() {
	}

	/**
	 * 获取唯一实例
	 * 
	 * @return
	 */
	public static ConfigManager getManager() {
		if (man == null) {
			man = new ConfigManager();
		}
		return man;
	}

	// 缓存对象
	private Configuration config = null;
	private Searchable search = null;
	// Cache SuggestionConsult object
	private SuggestionConsult consult = null;

	/**
	 * 设置Configuration对象
	 * 
	 * @param config
	 */
	public void setConfig(Configuration config) {
		this.config = config;
	}

	/**
	 * 获取Searchable对象<br>
	 * 若Configuration为null,返回null
	 * 
	 * @return
	 * @throws LuceneException
	 */
	public Searchable getSearcher() throws LuceneException {
		if (config != null) {
			if (search == null) {
				search = config.getOrBuildSearcher();
			}
			return search;
		}
		return null;
	}

	/**
	 * 更新Searchable<br>
	 * 在每次创建索引后调用此方法,否则会导致新创建的索引文件无法搜索到<br>
	 * 删除索引后也需要调用
	 * 
	 * @throws LuceneException
	 */
	public void updateSearchable() throws LuceneException {
		if (config != null) {
			search = config.rebuildSearhcer();
		}
	}

	/**
	 * 获取Writable<br>
	 * 若Configuration为null,返回null
	 * 
	 * @return
	 * @throws LuceneException
	 */
	public Writable getWriter() throws LuceneException {
		if (config != null) {
			return config.getOrBuildWriter();
		}
		return null;
	}

	/**
	 * 获取Writable<br>
	 * 若Configuration为null,返回null
	 * 
	 * @param isCreate
	 *            true 覆盖所有索引 false 追加
	 * @return
	 * @throws LuceneException
	 */
	public Writable getWriter(boolean isCreate) throws LuceneException {
		if (config != null) {
			return config.getOrBuildWriter(isCreate);
		}
		return null;
	}

	/**
	 * 获取Configuration对象
	 * 
	 * @return
	 * @throws ConfigInfoInvalideException
	 * @throws ConfigLoadException
	 */
	public Configuration getConfig() throws ConfigLoadException,
			ConfigInfoInvalideException {
		if (config == null) {
			config = Configuration.build(PropertiesUtil.CONFIG_FILE);
		}
		return config;
	}

	/**
	 * Auto complete
	 * 
	 * @param keyWord
	 *            keyWord
	 * @return Search result as Arrays,return null if parameters error or search
	 *         exception
	 */
	public String[] suggestion(String keyWord) {
		if (!SystemUtil.hasText(keyWord)) {
			return null;
		}
		// Check indexs directory whether exist
		if (!SystemUtil.isIndexFileExist()) {
			return null;
		}
		try {
			if (consult == null) {
				consult = getConfig().getSuggestionConsult(
						PropertiesUtil.SUGGESTION_FIELD_NAME);
			}
			return consult.suggest(keyWord,
					PropertiesUtil.AUTO_COMPLATE_MAX_LENGTH);
		} catch (Exception e) {
			LogUtil.saveExceptionLog(e);
			if (log.isDebugEnabled()) {
				log.debug("自动补全搜索时发生异常", e);
			}
		}
		return null;
	}

	/**
	 * Update SuggestionConsult cache object, After create index,need call this
	 * method.
	 * 
	 * @throws Exception
	 *             Exception
	 */
	public void updateSuggestionConsult() throws Exception {
		consult = getConfig().refreshSuggestionConsult();
	}

	/**
	 * 关闭Configuration <br>
	 * 注意: 在退出系统时请调用 此方法
	 */
	public void close() {
		if (config != null) {
			try {
				config.close();
			} catch (IOException e) {
				LogUtil.saveExceptionLog(e);
				SwingUtil.showErrorInfo(null, "退出异常", "IOException,信息:\n"
						+ e.getMessage());
			} catch (LuceneException e) {
				LogUtil.saveExceptionLog(e);
				SwingUtil.showErrorInfo(null, "退出异常", "LuceneException,信息:\n"
						+ e.getMessage());
			}
		}
	}

}
