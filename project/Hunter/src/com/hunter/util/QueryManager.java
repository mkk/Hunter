package com.hunter.util;

import java.io.IOException;

import org.apache.lucene.search.Query;

import com.hunter.desktop.search.SearchTools;

/**
 * 2010-9-23 下午02:06:13<br>
 * 查询对象Query管理
 * 
 * @author mkk
 */
public class QueryManager {

	private static QueryManager man = new QueryManager();

	/**
	 * get object
	 * 
	 * @return
	 */
	public static QueryManager getQueryManager() {
		return man;
	}

	/**
	 * private constructor
	 */
	private QueryManager() {
	}

	/**
	 * 获取设置的query对象,可以有搜索设置面板中修改相关属性
	 * 
	 * @param keyWord
	 *            搜索关键字,若为null则返回null
	 * @return
	 * @throws IOException
	 */
	public Query getQuery(String keyWord) throws IOException {
		if (keyWord == null) {
			return null;
		}
		// 暂时使用默认的.
		Query q = getDefaultQuery(keyWord);
		return q;
	}

	/**
	 * 获取默认的Query对象<br>
	 * 默认使用文件的IK查询,若创建IK的Query为Null,则使用文件名(FullFileNameQuery)的Query
	 * 
	 * @param keyWord
	 *            搜索关键字,若为null则返回null
	 * @return
	 * @throws IOException
	 */
	public Query getDefaultQuery(String keyWord) throws IOException {
		if (keyWord == null) {
			return null;
		}
		Query q = SearchTools.createFileNameQueryAnalyzedByIK(keyWord);
		if (q == null) {
			q = SearchTools.createFullFileNameQuery(keyWord);
		}
		return q;
	}

}
