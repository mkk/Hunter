package com.hunter.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;

import com.hunter.desktop.file.FileData;
import com.hunter.desktop.file.RealDirectory;
import com.hunter.desktop.file.RealFile;
import com.hunter.lucene.util.UserData;

/**
 * 2010-8-10 下午09:16:48<br>
 * 系统工具类
 * 
 * @author mkk
 */
public class SystemUtil {

	// Toolkit
	// private static Toolkit toolkit = Toolkit.getDefaultToolkit();
	// dateformat
	private static SimpleDateFormat sdFormat = new SimpleDateFormat();
	// number format
	private static DecimalFormat dFormat = new DecimalFormat();
	// runtime
	private static Runtime runtime = Runtime.getRuntime();
	// index file
	private static File indexFile = new File(PropertiesUtil.SAVE_INDEX_PATH);

	/**
	 * 判断是否有索引文件
	 * 
	 * @return True if have index files,otherwise return false.
	 */
	public static boolean isIndexFileExist() {
		if (indexFile.exists()) {
			if (indexFile.list().length > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取索引存放目录
	 * 
	 * @return
	 */
	public static File getIndexFile() {
		if (!indexFile.exists()) {
			indexFile.mkdir();
		}
		return indexFile;
	}

	/**
	 * 打开指定的文件
	 * 
	 * @param filePath
	 *            文件绝对路径
	 * @throws IOException
	 */
	public static void openFile(String filePath) throws IOException {
		if (filePath == null) {
			return;
		}
		String command = null;
		String ss[] = filePath.split("\\" + File.separator);
		String fileName = ss[ss.length - 1];
		if (!fileName.contains(" ")) {
			command = "cmd /c start " + filePath;
		} else {
			command = "cmd /c start explorer " + filePath;
		}

		runtime.exec(command);
	}

	/**
	 * 打开目录并选中指定的文件
	 * 
	 * @param filePath
	 *            文件
	 * @throws IOException
	 */
	public static void openDir4File(String filePath) throws IOException {
		if (filePath == null) {
			return;
		}
		String command = "cmd /c explorer /select," + filePath;
		runtime.exec(command);
	}

	/**
	 * 将文件长度转化
	 * 
	 * @param fileLength
	 * @return
	 */
	public static String formatFileLength(long fileLength) {
		if (fileLength / (1024 * 1024) > 0) {
			return formatNumber(fileLength / (1024 * 1024d), "0.00") + " MB";
		} else {
			return formatNumber(fileLength / 1024d, "0.00") + " K";
		}
	}

	/**
	 * 格式化日期显示<br>
	 * 返回字符串格式 年-月-日 时:分:秒
	 * 
	 * @param cal
	 *            Calendar对象
	 * @return
	 */
	public static String formatCalendar(Calendar cal) {
		if (cal == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(cal.get(Calendar.YEAR) + "-");
		sb.append(((cal.get(Calendar.MONTH) + 1)) + "-");
		sb.append((cal.get(Calendar.DAY_OF_MONTH)) + " ");
		sb.append((cal.get(Calendar.HOUR_OF_DAY)) + ":");
		sb.append((cal.get(Calendar.MINUTE)) + ":");
		sb.append((cal.get(Calendar.SECOND)));
		return sb.toString();
	}

	/**
	 * 获取搜索无结果的显示信息
	 * 
	 * @param keyWord
	 *            搜索关键字
	 * @return
	 */
	public static String getNullResultInfo(String keyWord) {
		StringBuilder sb = new StringBuilder();
		sb.append("搜索不到任何与 <b>");
		sb.append(keyWord + "</b> 相关的文件或目录信息<br><br>");
		sb.append("建议:<ul>");
		sb.append("<li>请检查输入关键字是否有误</li>");
		sb.append("<li>请使用另外关键字搜索</li>");
		sb.append("<li>请创建相关目录的索引</li>");
		sb.append("</ul>");
		return sb.toString();
	}

	/**
	 * 系统logo图标
	 * 
	 * @return
	 */
	public static ImageIcon getSystemLogoIcon() {
		ImageIcon icon = getIcon("logo.gif");
		return icon;
	}

	/**
	 * 搜索按钮图标,small
	 * 
	 * @return
	 */
	public static ImageIcon getSearchIcon() {
		ImageIcon icon = getIcon("search2.gif");
		return icon;
	}

	/**
	 * 根据图标名称获取图标
	 * 
	 * @param iconName
	 *            图标名称,图标必须位于resource包里
	 * @return
	 */
	public static ImageIcon getIcon(String iconName) {
		if (iconName == null) {
			return null;
		}
		URL url = SystemUtil.class.getResource("/com/hunter/resource/"
				+ iconName);
		ImageIcon icon = new ImageIcon(url);
		return icon;
	}

	/**
	 * 启动图标
	 * 
	 * @return
	 */
	public static ImageIcon getStartupImage() {
		ImageIcon icon = getIcon("start.jpg");
		return icon;
	}

	/**
	 * 格式化数字
	 * 
	 * @param number
	 *            数字
	 * @param format
	 *            格式,如 0.00
	 * @return
	 */
	public static String formatNumber(Object number, String format) {
		if (number == null || format == null) {
			return null;
		}
		dFormat.applyPattern(format);
		String s = dFormat.format(number);
		return s;
	}

	/**
	 * 格式化日期
	 * 
	 * @param date
	 *            日期值
	 * @param format
	 *            格式
	 * @return
	 */
	public static String formatDate(Date date, String format) {
		if (date == null || format == null) {
			return null;
		}
		sdFormat.applyPattern(format);
		String s = sdFormat.format(date);
		return s;
	}

	/**
	 * 根据数值获取时间字符串
	 * 
	 * @param times
	 * @return
	 */
	public static String getTimeByLong(long times) {
		Date d = new Date(times);
		String s = formatDate(d, "yyyy-MM-dd HH:mm:ss");
		return s;
	}

	/**
	 * 根据需求获取显示的搜索结果分页信息
	 * 
	 * @param datas
	 *            将要显示的数据对象
	 * @return
	 */
	public static String getPageInfo(List<FileData> datas) {
		StringBuilder sb = new StringBuilder();
		if (datas == null) {
			return sb.toString();
		}
		for (UserData ud : datas) {
			sb.append("<table width='" + PropertiesUtil.RESULT_SHOW_WIDTH
					+ "'border='0' style='font-family:宋体' >");
			if (ud instanceof RealFile) {
				RealFile f = (RealFile) ud;
				sb.append("<tr>");
				sb.append("  <td><a href='" + f.getFileName() + "' >"
						+ f.getFileName() + "</a></td>");
				sb.append(" <td>&nbsp</td>");
				sb.append("</tr>");
				sb.append(" <tr>");
				sb.append(" <td colspan='2'>");
				sb
						.append("<span style='color:gray'>路径: <span style='color:#548C00'>"
								+ f.getAbsolutePath()
								+ "</span></span>&nbsp;&nbsp;");
				sb
						.append("<span style='color:gray'>大小: <span style='color:#548C00'>"
								+ formatFileLength(f.getLength())
								+ "</span></span>&nbsp;&nbsp;");
				sb
						.append("<span style='color:gray'>最后修改时间: <span style='color:#548C00'>"
								+ getTimeByLong(f.getLastModified())
								+ "</span></span>");
				sb.append("</td>");
				sb.append(" </tr>");
				String content = f.getStringContent();
				if (content != null && content.length() > 0) {
					sb.append(" <tr>");
					sb.append("<td colspan='2'>");
					sb.append("  <div>");
					sb.append(content + "</div>");
					sb.append("</td>");
					sb.append("</tr>");
				} else {
					sb.append("<tr><td colspan='2'>&nbsp;</td></tr>");
				}

			} else if (ud instanceof RealDirectory) {
				RealDirectory dir = (RealDirectory) ud;
				sb.append("<tr>");
				sb.append("  <td><a href='" + dir.getFileName()
						+ "' style='color:#6C3365'>" + dir.getFileName()
						+ "</a></td>");
				sb.append(" <td>&nbsp</td>");
				sb.append("</tr>");
				sb.append(" <tr>");
				sb.append(" <td colspan='2'>");
				sb
						.append("<span style='color:gray'>路径: <span style='color:#548C00'>"
								+ dir.getAbsolutePath()
								+ "</span></span>&nbsp;&nbsp;");
				sb
						.append("<span style='color:gray'>最后修改时间: <span style='color:#548C00'>"
								+ getTimeByLong(dir.getLastModified())
								+ "</span></span>");
				sb.append("</td></tr>");
				sb.append(" <tr><td colspan='2'>&nbsp;</td></tr>");
			}
			sb.append("</table>");
		}
		return sb.toString();
	}

	/**
	 * Check text whether have character
	 * @param text Text
	 * @return True if have,otherwise return false
	 */
	public static boolean hasText(String text) {
		if (text == null) {
			return false;
		}
		if (text.trim().length() == 0) {
			return false;
		}
		return true;
	}

}
