package com.hunter.util;

import java.io.File;
import java.util.prefs.Preferences;

/**
 * 2010-8-10 下午10:20:59 <br>
 * 系统配置工具类
 * 
 * @author mkk
 */
public class PropertiesUtil {

	// 初始主界面默认大小值
	public static final int DEFAULT_WIDTH = 830;
	public static final int DEFAULT_HEIGHT = 566;
	// 系统标题
	public static final String TITLE = "Hunter 1.0";
	// 日志存放目录
	public static final String LOG_PATH = "log";
	// 存放索引文件目录
	public static final String SAVE_INDEX_PATH = "indexs";
	// 默认搜索每次获取的结果条数,默认100条,暂时没用
	public static final int SEARCH_RETURN_NUMBER = 100;
	// 每页显示的数目
	public static final int DEFAULT_PAGE_SIZE = 6;
	// 搜索结果显示页面的字符显示宽度
	public static final int RESULT_SHOW_WIDTH = 790;
	// 初始配置文件名
	public static final String CONFIG_FILE = "file.xml";
	// 字符编码
	public static final String DEFAULT_ENCODING = "GBK";
	// 自动补全开关,默认TRUE
	public static final boolean AUTO_COMPLATE_OPEN = true;
	// 自动补全显示结果最大条数,默认5
	public static final int AUTO_COMPLATE_MAX_LENGTH = 5;
	//自动补全搜索时使用的属性名(Luncec使用)
	public static final String SUGGESTION_FIELD_NAME="analyzedName";
	// preferences
	private static Preferences preferences = Preferences
			.systemNodeForPackage(PropertiesUtil.class);

	static {
		// 创建需要的目录
		File f = new File(LOG_PATH);
		if (!f.exists()) {
			f.mkdir();
		}
	}

	/**
	 * 获取是否显示关闭时的窗口
	 * 
	 * @return
	 */
	public static boolean isShowClosePanel() {
		return preferences.getBoolean("search.isShowClosePanel", true);
	}

	/**
	 * 设置是否显示关闭时的窗口
	 * 
	 * @param flag
	 */
	public static void setShowClosePanel(boolean flag) {
		preferences.putBoolean("search.isShowClosePanel", flag);
	}

	// public static void main(String[] args) {
	// //setShowClosePanel(true);
	// String s="a@@bc@@#123";
	// System.out.println(s.split("@@#")[0]);
	// }

	/**
	 * 获取关闭主界面时的操作类型
	 * 
	 * @return true为关闭,false为最小化到系统托盘
	 */
	public static boolean getCloseChooser() {
		return preferences.getBoolean("search.closeChooser", true);
	}

	/**
	 * 设置关闭主界面时的操作类型
	 * 
	 * @param flag
	 *            true为关闭,false为最小化到系统托盘
	 */
	public static void setCloseChooser(boolean flag) {
		preferences.putBoolean("search.closeChooser", flag);
	}

	/**
	 * 保存已创建索引的目录信息<br>
	 * 多个目录用@@#号分隔
	 * 
	 * @param paths
	 */
	public static void saveIndexPaths(String paths) {
		if (paths == null) {
			return;
		}
		preferences.put("index.paths", paths);
	}

	/**
	 * 获取已创建索引的目录信息<br>
	 * 没有则返回空字符串
	 * 
	 * @return
	 */
	public static String getIndexPaths() {
		return preferences.get("index.paths", "");
	}

}
