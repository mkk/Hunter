package com.hunter.lucene.util.convert;

/**
 * 
 * @author bastengao
 * 
 */
public class ToStringConvert extends AbstractFieldConvertor {

	/**
	 * 此方法将直接调用对象的 toString() 方法，并将返回值直接返回。
	 * 
	 * @param data
	 */
	@Override
	public String toStringValue(Object data) {
		return data.toString();
	}

	@Override
	public ConvertToType convertTo() {
		return ConvertToType.STRING;
	}

}
