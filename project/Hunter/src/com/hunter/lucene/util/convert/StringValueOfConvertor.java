package com.hunter.lucene.util.convert;

/**
 * 
 * @author bastengao
 * 
 */
public class StringValueOfConvertor extends AbstractFieldConvertor {

	/**
	 * 此方法将直接调用 String.valueOf() 方法，并做为返回值直接返回。
	 * 
	 * @param data
	 */
	@Override
	public String toStringValue(Object data) {
		return String.valueOf(data);
	}

	@Override
	public ConvertToType convertTo() {
		return ConvertToType.STRING;
	}

}
