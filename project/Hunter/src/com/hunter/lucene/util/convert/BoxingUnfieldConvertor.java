package com.hunter.lucene.util.convert;

/**
 * 基本类型的转换器。包括 byte ,short ,int ,long ,float ,double ,boolean.
 * 
 * @author bastengao
 * 
 */
public class BoxingUnfieldConvertor implements UnfieldConvertor {

	/**
	 * @throws IllegalArgumentException
	 *             如果传递的参数的类型不在上面所说的之中
	 */
	@Override
	public Object fromStringValue(String value, Class<?> returnType) {
		Object result = null;
		if (returnType == Byte.TYPE) {
			result = Byte.parseByte(value);
		} else if (returnType == Short.TYPE) {
			result = Short.parseShort(value);
		} else if (returnType == Integer.TYPE) {
			result = Integer.parseInt(value);
		} else if (returnType == Long.TYPE) {
			result = Long.parseLong(value);
		} else if (returnType == Float.TYPE) {
			result = Float.parseFloat(value);
		} else if (returnType == Double.TYPE) {
			result = Double.parseDouble(value);
		} else if (returnType == Boolean.TYPE) {
			result = Boolean.parseBoolean(value);
		} else {
			throw new IllegalArgumentException("the type: " + result
					+ " could not be converted.");
		}
		return result;
	}

}
