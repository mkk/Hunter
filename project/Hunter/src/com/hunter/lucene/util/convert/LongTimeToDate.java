package com.hunter.lucene.util.convert;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.DateTools.Resolution;

/**
 * 
 * @author bastengao
 *
 */
public class LongTimeToDate extends AbstractFieldConvertor {

	@Override
	public ConvertToType convertTo() {
		return ConvertToType.STRING;
	}

	@Override
	public String toStringValue(Object data) {
		if(data instanceof Long) {
			long time=(Long)data;
			return DateTools.timeToString(time, Resolution.MINUTE);
		}else {
			throw new IllegalArgumentException("the data type is not long");
		}
	}

}
