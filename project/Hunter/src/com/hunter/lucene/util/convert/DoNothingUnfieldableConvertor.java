package com.hunter.lucene.util.convert;

/**
 * 顾名思义，如果返回值要求的是 String 类型，则直接返回。
 * 
 * @author bastengao
 * 
 */
public class DoNothingUnfieldableConvertor implements UnfieldConvertor {

	/**
	 * 如果返回值要求的是 String 类型，则直接返回 value 。
	 * 
	 * @throws IllegalStateException
	 *             如果 returnType 的类型不是 String
	 */
	@Override
	public Object fromStringValue(String value, Class<?> returnType) {
		if (!returnType.equals(String.class)) {
			throw new IllegalStateException(
					"this method is only used on String returnType,not "
							+ returnType.getName());
		}
		return value;
	}

}
