package com.hunter.lucene.util.convert;

import java.io.Reader;

/**
 * 实现者可以通过继承此类，来进行简单的实现。通过覆盖自己关心的方法，调用者则通过 convertTo() 的方法的返回值决定调用哪个方法。
 * 
 * @author bastengao
 * 
 */
public abstract class AbstractFieldConvertor implements FieldConvertor {

	@Override
	public Reader toReaderValue(Object data) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toStringValue(Object data) {
		throw new UnsupportedOperationException();
	}

	@Override
	abstract public ConvertToType convertTo();

}
