package com.hunter.lucene.util.convert;

/**
 * 将 field 的 String 转换为 object,用户自定义类型
 * 
 * @author bastengao
 * 
 */
public interface UnfieldConvertor {
	public Object fromStringValue(String value, Class<?> returnType);
}
