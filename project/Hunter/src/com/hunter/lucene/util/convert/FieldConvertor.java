package com.hunter.lucene.util.convert;

import java.io.Reader;

/**
 * 将data 转换为 String 或者 Reader ,提供给创建 field 数据转换器。至于进行怎么的转换由 convertTo() 的返回值决定。
 * 
 * @author bastengao
 * 
 */
public interface FieldConvertor {

	/**
	 * 将data 转换 为 String
	 * 
	 * @param data
	 * @return
	 */
	public String toStringValue(Object data);

	/**
	 * 将data 转换为 Reader
	 * @param data
	 * @return
	 */
	public Reader toReaderValue(Object data);

	/**
	 * 指定将要为的数据类型
	 * 
	 * @return
	 */
	public ConvertToType convertTo();

	public static enum ConvertToType {
		STRING, READER;
	}
}
