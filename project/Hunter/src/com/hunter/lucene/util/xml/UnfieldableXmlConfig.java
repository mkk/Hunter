package com.hunter.lucene.util.xml;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.LoadStrategy;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 
 * @author bastengao
 *
 */
public class UnfieldableXmlConfig {
	private String name;
	private LoadStrategy loadStrategy;
	private Class<? extends UnfieldConvertor> convertor;
	private Class<?> attributeClass;
	private Method writeMethod;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LoadStrategy getLoadStrategy() {
		return loadStrategy;
	}

	public void setLoadStrategy(LoadStrategy loadStrategy) {
		this.loadStrategy = loadStrategy;
	}

	public Class<? extends UnfieldConvertor> getConvertor() {
		return convertor;
	}

	public void setConvertor(Class<? extends UnfieldConvertor> convertor) {
		this.convertor = convertor;
	}

	public Class<?> getAttributeClass() {
		return attributeClass;
	}

	public void setAttributeClass(Class<?> attributeClass) {
		this.attributeClass = attributeClass;
	}

	public Method getWriteMethod() {
		return writeMethod;
	}

	public void setWriteMethod(Method writeMethod) {
		this.writeMethod = writeMethod;
	}

	@Override
	public String toString() {
		return "UnfieldableXmlConfig [attributeClass=" + attributeClass
				+ ", convertor=" + convertor + ", loadStrategy=" + loadStrategy
				+ ", name=" + name + ", writeMethod=" + writeMethod + "]";
	}

}
