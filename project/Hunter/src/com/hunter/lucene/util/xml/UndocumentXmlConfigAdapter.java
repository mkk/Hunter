package com.hunter.lucene.util.xml;

import java.util.List;

import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.UndocumentConfigInfo;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;
/**
 * 
 * @author bastengao
 *
 */
public class UndocumentXmlConfigAdapter implements UndocumentConfigInfo {
	private UndocumentXmlConfig config;

	public UndocumentXmlConfigAdapter(UndocumentXmlConfig config) {
		super();
		this.config = config;
	}

	@Override
	public DiscriminatorValue discrimination() {
		return config.getDiscrimination();
	}

	@Override
	public Class<?> undocumentedClass() {
		return config.getUndocumentedClass();
	}

	@Override
	public List<UnfieldableConfigInfo> unfieldableConfigs() {
		return config.getUnfieldableConfigs();
	}

	@Override
	public String toString() {
		return "UndocumentXmlConfigWrapper [config=" + config + "]";
	}

}
