package com.hunter.lucene.util.xml;

import java.lang.reflect.Method;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.config.FieldableConfigInfo;
import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * 
 * @author bastengao
 * 
 */
public class FieldXmlConfigAdapter implements FieldableConfigInfo {

	private FieldableXmlConfig config;

	public FieldXmlConfigAdapter(FieldableXmlConfig config) {
		this.config = config;
	}

	@Override
	public float boost() {
		return config.getBoost();
	}

	@Override
	public Class<? extends FieldConvertor> convertor() {
		return config.getConvertor();
	}

	@Override
	public Index index() {
		return config.getIndex();
	}

	@Override
	public String name() {
		return config.getName();
	}

	@Override
	public Method readMethod() {
		return config.getReadMethod();
	}

	@Override
	public Store store() {
		return config.getStore();
	}

	@Override
	public TermVector termVector() {
		return config.getTermVector();
	}

	@Override
	public Class<?> type() {
		return config.getAttributeType();
	}

	@Override
	public String toString() {
		return "FieldXmlConfigWrapper [config=" + config + "]";
	}

}
