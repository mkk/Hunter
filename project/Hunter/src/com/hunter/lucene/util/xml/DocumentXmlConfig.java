package com.hunter.lucene.util.xml;

import java.util.List;

import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.FieldableConfigInfo;

/**
 * 
 * 
 * @author bastengao
 * 
 */
public class DocumentXmlConfig {

	private String analyzer;

	private DiscriminatorValue discriminator;

	private float boost;

	private Class<?> documentedClass;

	private FieldableConfigInfo uniqueField;

	List<FieldableConfigInfo> fieldConfigs;

	public String getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(String analyzer) {
		this.analyzer = analyzer;
	}

	public float getBoost() {
		return boost;
	}

	public void setBoost(float boost) {
		this.boost = boost;
	}

	public Class<?> getDocumentedClass() {
		return documentedClass;
	}

	public void setDocumentedClass(Class<?> documentedClass) {
		this.documentedClass = documentedClass;
	}

	public List<FieldableConfigInfo> getFieldConfigs() {
		return fieldConfigs;
	}

	public void setFieldConfigs(List<FieldableConfigInfo> fieldConfigs) {
		this.fieldConfigs = fieldConfigs;
	}

	public FieldableConfigInfo getUniqueField() {
		return uniqueField;
	}

	public void setUniqueField(FieldableConfigInfo uniqueField) {
		this.uniqueField = uniqueField;
	}

	public DiscriminatorValue getDiscriminator() {
		return discriminator;
	}

	public void setDiscriminator(DiscriminatorValue discriminator) {
		this.discriminator = discriminator;
	}

	@Override
	public String toString() {
		return "DocumentXmlConfig [analyzer=" + analyzer + ", boost=" + boost
				+ ", discrimination=" + discriminator + ", documentedClass="
				+ documentedClass + ", fieldConfigs=" + fieldConfigs
				+ ", uniqueField=" + uniqueField + "]";
	}

}
