package com.hunter.lucene.util.xml;

import java.util.List;

import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;

/**
 * "@Undocument" 的包装类型
 * 
 * @author bastengao
 * 
 */
public class UndocumentXmlConfig {
	private Class<?> undocumentedClass;
	private DiscriminatorValue discrimination;

	private List<UnfieldableConfigInfo> unfieldableConfigs;

	public Class<?> getUndocumentedClass() {
		return undocumentedClass;
	}

	public void setUndocumentedClass(Class<?> undocumentedClass) {
		this.undocumentedClass = undocumentedClass;
	}

	public DiscriminatorValue getDiscrimination() {
		return discrimination;
	}

	public void setDiscrimination(DiscriminatorValue discrimination) {
		this.discrimination = discrimination;
	}

	public List<UnfieldableConfigInfo> getUnfieldableConfigs() {
		return unfieldableConfigs;
	}

	public void setUnfieldableConfigs(
			List<UnfieldableConfigInfo> unfieldableConfigs) {
		this.unfieldableConfigs = unfieldableConfigs;
	}

	@Override
	public String toString() {
		return "UndocumentXmlConfig [discrimination=" + discrimination
				+ ", undocumentedClass=" + undocumentedClass
				+ ", unfieldableConfigs=" + unfieldableConfigs + "]";
	}

}
