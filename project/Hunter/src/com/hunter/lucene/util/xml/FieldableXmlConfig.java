package com.hunter.lucene.util.xml;

import java.lang.reflect.Method;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * "@Fieldable" 的包装类型
 * 
 * @author bastengao
 * 
 */
public class FieldableXmlConfig {
	private String name;
	private Class<? extends FieldConvertor> convertor;
	private float boost;
	private Class<?> attributeType;
	private Store store;
	private Index index;
	private TermVector termVector;
	private Method readMethod;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<? extends FieldConvertor> getConvertor() {
		return convertor;
	}

	public void setConvertor(Class<? extends FieldConvertor> convertor) {
		this.convertor = convertor;
	}

	public float getBoost() {
		return boost;
	}

	public void setBoost(float boost) {
		this.boost = boost;
	}

	public Class<?> getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(Class<?> attributeType) {
		this.attributeType = attributeType;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Index getIndex() {
		return index;
	}

	public void setIndex(Index index) {
		this.index = index;
	}

	public TermVector getTermVector() {
		return termVector;
	}

	public void setTermVector(TermVector termVector) {
		this.termVector = termVector;
	}

	public Method getReadMethod() {
		return readMethod;
	}

	public void setReadMethod(Method readMethod) {
		this.readMethod = readMethod;
	}

	@Override
	public String toString() {
		return "FieldableXmlConfig [attributeType=" + attributeType
				+ ", boost=" + boost + ", convertor=" + convertor + ", index="
				+ index + ", name=" + name + ", readMethod=" + readMethod
				+ ", store=" + store + ", termVector=" + termVector + "]";
	}

}
