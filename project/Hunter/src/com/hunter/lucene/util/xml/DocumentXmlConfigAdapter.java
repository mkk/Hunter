package com.hunter.lucene.util.xml;

import java.util.List;

import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.DocumentConfigInfo;
import com.hunter.lucene.util.config.FieldableConfigInfo;

/**
 * 
 * @author bastengao
 * 
 */
public class DocumentXmlConfigAdapter implements DocumentConfigInfo {
	private DocumentXmlConfig config;

	public DocumentXmlConfigAdapter(DocumentXmlConfig config) {
		this.config = config;
	}

	@Override
	public String analyzerName() {
		return config.getAnalyzer();
	}

	@Override
	public DiscriminatorValue discriminator() {
		return config.getDiscriminator();
	}

	@Override
	public Class<?> documentedClass() {
		return config.getDocumentedClass();
	}

	@Override
	public List<FieldableConfigInfo> fieldConfigs() {
		return config.getFieldConfigs();
	}

	@Override
	public float boost() {
		return config.getBoost();
	}

	@Override
	public String toString() {
		return "DocumentXmlConfigWrapper [config=" + config + "]";
	}

	@Override
	public FieldableConfigInfo uniqueField() {
		if (config.getUniqueField() != null) {
			return config.getUniqueField();
		}
		return null;
	}

}
