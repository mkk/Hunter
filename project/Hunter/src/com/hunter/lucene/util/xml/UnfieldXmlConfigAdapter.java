package com.hunter.lucene.util.xml;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.LoadStrategy;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 
 * @author bastengao
 * 
 */
public class UnfieldXmlConfigAdapter implements UnfieldableConfigInfo {
	private UnfieldableXmlConfig config;

	public UnfieldXmlConfigAdapter(UnfieldableXmlConfig config) {
		super();
		this.config = config;
	}

	@Override
	public Class<? extends UnfieldConvertor> convertor() {
		return config.getConvertor();
	}

	@Override
	public LoadStrategy loadStrategy() {
		return config.getLoadStrategy();
	}

	@Override
	public String name() {
		return config.getName();
	}

	@Override
	public Method writeMethed() {
		return config.getWriteMethod();
	}

	@Override
	public Class<?> type() {
		return config.getAttributeClass();
	}

	@Override
	public String toString() {
		return "UnfieldXmlConfigWrapper [config=" + config + "]";
	}

}
