package com.hunter.lucene.util.annotation;

import java.lang.reflect.Method;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.annotation.parse.UniqueFieldAnnotationWrapper;
import com.hunter.lucene.util.config.FieldableConfigInfo;
import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * 通过包装 UniqueFieldAnnotatioinWrapper,实现 FieldableConfigInfo
 * @author bastengao
 *
 */
public class UniqueFieldConfigAdapter implements FieldableConfigInfo {
	private UniqueFieldAnnotationWrapper config;

	protected UniqueFieldConfigAdapter(UniqueFieldAnnotationWrapper config) {
		this.config = config;
	}

	@Override
	public float boost() {
		return config.getUniqueField().boost();
	}

	@Override
	public Class<? extends FieldConvertor> convertor() {
		return config.getUniqueField().convertor();
	}

	@Override
	public Index index() {
		return config.getUniqueField().index();
	}

	@Override
	public String name() {
		return config.getUniqueField().name();
	}

	@Override
	public Method readMethod() {
		return config.getReadMethod();
	}

	@Override
	public Store store() {
		return config.getUniqueField().store();
	}

	@Override
	public TermVector termVector() {
		return config.getUniqueField().termVector();
	}

	@Override
	public Class<?> type() {
		return config.getAnnotationedClass();
	}

}
