package com.hunter.lucene.util.annotation;

import java.lang.reflect.Method;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.annotation.parse.FieldableAnnotationWrapper;
import com.hunter.lucene.util.config.FieldableConfigInfo;
import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * 
 * @author bastengao
 * 
 */
public class FieldableAnnotationConfigAdapter implements FieldableConfigInfo {
	private FieldableAnnotationWrapper config;

	public FieldableAnnotationConfigAdapter(FieldableAnnotationWrapper config) {
		super();
		this.config = config;
	}

	@Override
	public float boost() {
		return config.getAnnotation().boost();
	}

	@Override
	public Class<? extends FieldConvertor> convertor() {
		return config.getAnnotation().convertor();
	}

	@Override
	public Index index() {
		return config.getAnnotation().index();
	}

	@Override
	public String name() {
		return config.getAnnotation().name();
	}

	@Override
	public Store store() {
		return config.getAnnotation().store();
	}

	@Override
	public TermVector termVector() {
		return config.getAnnotation().termVector();
	}

	@Override
	public Class<?> type() {
		return config.getAnnotationedClass();
	}

	@Override
	public Method readMethod() {
		return config.getReadMethod();
	}

	@Override
	public String toString() {
		return "FieldableAnnotationConfigWrapper [config=" + config + "]";
	}

}
