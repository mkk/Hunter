package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表示一个类可以被索引。此类与 lucene 中的 document 等价 与 @Fieldable 配合使用
 * 
 * 使用:此注解只能对类使用，这样被他注解的类的方法，和属性才可以使用Fieldabel 或者 Data 注解；否则，单独使用这些注解，则没有效果。
 * 
 * @author bastengao
 * 
 */
@Target(value = { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Document {
	/**
	 * 指定用于此document 的 Analyzer
	 * 
	 * @return
	 */
	String analyzer() default "default";

	/**
	 * 指定与其他 document 区别的 discrimination
	 * 
	 * @return
	 */
	Discriminator discrimination();

	float boost() default 1.0f;
}
