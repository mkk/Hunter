package com.hunter.lucene.util.annotation;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.parse.UnfieldableAnnotationWrapper;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 通过包装 UnfieldableAnnoatationWrapper ,实现 UnfieldableConfigInfo
 * @author bastengao
 * 
 */
public class UnfieldableAnnotationConfigAdapter implements
		UnfieldableConfigInfo {
	private UnfieldableAnnotationWrapper config;

	public UnfieldableAnnotationConfigAdapter(
			UnfieldableAnnotationWrapper config) {
		super();
		this.config = config;
	}

	@Override
	public Class<? extends UnfieldConvertor> convertor() {
		return config.getAnnotation().convertor();
	}

	@Override
	public LoadStrategy loadStrategy() {
		return config.getAnnotation().load();
	}

	@Override
	public String name() {
		return config.getAnnotation().name();
	}

	@Override
	public Method writeMethed() {
		return config.getWriteMethod();
	}

	@Override
	public Class<?> type() {
		return config.getAnnotationedClass();
	}

	@Override
	public String toString() {
		return "UnfieldableAnnotationConfigWrapper [config=" + config + "]";
	}

}
