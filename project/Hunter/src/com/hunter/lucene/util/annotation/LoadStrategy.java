package com.hunter.lucene.util.annotation;

import org.apache.lucene.document.FieldSelectorResult;

/**
 * 
 * @author bastengao
 *
 */
public enum LoadStrategy {
	/**
	 * 立即加载
	 */
	LOAD(FieldSelectorResult.LOAD),
	/**
	 * 延迟加载
	 */
	LAZY_LOAD(FieldSelectorResult.LAZY_LOAD);
	private LoadStrategy(FieldSelectorResult mode) {
		this.mode = mode;
	}

	private FieldSelectorResult mode;

	public FieldSelectorResult mode() {
		return mode;
	}
}
