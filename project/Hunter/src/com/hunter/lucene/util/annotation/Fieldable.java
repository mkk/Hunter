package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.convert.FieldConvertor;
import com.hunter.lucene.util.convert.StringValueOfConvertor;

/**
 * 表示lucene 中的 Document 的 Field。可参见 lucene 参考
 * 
 * 使用：此注解只能有在被 Document 注解的类的方法或者属性上。
 * 
 * @author bastengao
 * 
 */
@Target( { ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Fieldable {
	/**
	 * field name
	 * 
	 * @return
	 */
	String name();

	Class<? extends FieldConvertor> convertor() default StringValueOfConvertor.class;

	/**
	 * store 策略,默认为 YES
	 * 
	 * @return
	 */
	Store store() default Store.YES;

	/**
	 * index 策略，默认为 ANALYZED
	 * 
	 * @return
	 */
	Index index() default Index.ANALYZED;

	/**
	 * termVector 策略，默认为 NO
	 * 
	 * @return
	 */
	TermVector termVector() default TermVector.NO;

	/**
	 * 权重
	 * 
	 * @return
	 */
	float boost() default 1.0f;

}
