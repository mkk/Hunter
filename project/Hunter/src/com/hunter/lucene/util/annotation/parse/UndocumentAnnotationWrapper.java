package com.hunter.lucene.util.annotation.parse;

import java.util.List;

import com.hunter.lucene.util.annotation.Undocument;

/**
 * 
 * @author bastengao
 *
 */
public class UndocumentAnnotationWrapper {
	private Undocument annotation;
	private Class<?> annotationedClass;

	private List<UnfieldableAnnotationWrapper> unfieldabelAnnotationWrappers;

	public Undocument getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Undocument annotation) {
		this.annotation = annotation;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

	public List<UnfieldableAnnotationWrapper> getUnfieldabelAnnotationWrappers() {
		return unfieldabelAnnotationWrappers;
	}

	public void setUnfieldabelAnnotationWrappers(
			List<UnfieldableAnnotationWrapper> unfieldabelAnnotationWrappers) {
		this.unfieldabelAnnotationWrappers = unfieldabelAnnotationWrappers;
	}

	@Override
	public String toString() {
		return "UndocumentAnnotationWrapper [annotation=" + annotation
				+ ", annotationedClass=" + annotationedClass
				+ ", unfieldabelAnnotationWrappers="
				+ unfieldabelAnnotationWrappers + "]";
	}

}
