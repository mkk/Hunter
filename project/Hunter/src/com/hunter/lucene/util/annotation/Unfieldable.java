package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.hunter.lucene.util.convert.DoNothingUnfieldableConvertor;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 加载策略
 * 
 * 使用：单独使用此注解，是没有任何效果的。与 Undocument 配合使用，才能发挥作用。
 * 
 * @author bastengao
 */
@Target( { ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Unfieldable {
	String name();

	LoadStrategy load() default LoadStrategy.LOAD;

	/**
	 * 指定转换器
	 * 
	 * @return
	 */
	Class<? extends UnfieldConvertor> convertor() default DoNothingUnfieldableConvertor.class;
}
