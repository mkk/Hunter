package com.hunter.lucene.util.annotation.parse;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.Unfieldable;

/**
 * 
 * @author bastengao
 * 
 */
public class UnfieldableAnnotationWrapper {
	private Unfieldable annotation;
	private Class<?> annotationedClass;
	private Method writeMethod;

	public Unfieldable getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Unfieldable annotation) {
		this.annotation = annotation;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

	public Method getWriteMethod() {
		return writeMethod;
	}

	public void setWriteMethod(Method writeMethod) {
		this.writeMethod = writeMethod;
	}

	@Override
	public String toString() {
		return "UnfieldableAnnotationWrapper [annotation=" + annotation
				+ ", annotationedClass=" + annotationedClass + ", writeMethod="
				+ writeMethod + "]";
	}

}
