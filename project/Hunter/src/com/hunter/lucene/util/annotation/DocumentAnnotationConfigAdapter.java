package com.hunter.lucene.util.annotation;

import java.util.ArrayList;
import java.util.List;

import com.hunter.lucene.util.annotation.parse.DocumentAnnotationWrapper;
import com.hunter.lucene.util.annotation.parse.FieldableAnnotationWrapper;
import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.DocumentConfigInfo;
import com.hunter.lucene.util.config.FieldableConfigInfo;

/**
 * 通过包装 DocumentAnnotationWrapper ，实现 DocumentConfigInfo。
 * 
 * @author bastengao
 * 
 */
public class DocumentAnnotationConfigAdapter implements DocumentConfigInfo {
	private DocumentAnnotationWrapper config;

	public DocumentAnnotationConfigAdapter(DocumentAnnotationWrapper config) {
		this.config = config;
	}

	@Override
	public String analyzerName() {
		return config.getDocumentAnnotation().analyzer();
	}

	@Override
	public DiscriminatorValue discriminator() {
		return new DiscriminatorValue(config.getDocumentAnnotation()
				.discrimination());
	}

	@Override
	public Class<?> documentedClass() {
		return config.getAnnotationedClass();
	}

	@Override
	public float boost() {
		return config.getDocumentAnnotation().boost();
	}

	@Override
	public List<FieldableConfigInfo> fieldConfigs() {
		// TODO these code may be has low performance.
		// these method rebuild the result when it is invoked.
		List<FieldableConfigInfo> fieldConfigs = new ArrayList<FieldableConfigInfo>(
				config.getFieldAnnotationWrappers().size() + 1);

		for (FieldableAnnotationWrapper wrapper : config
				.getFieldAnnotationWrappers()) {
			fieldConfigs.add(new FieldableAnnotationConfigAdapter(wrapper));
		}

		if (config.getUniqueField() != null) {
			fieldConfigs.add(new UniqueFieldConfigAdapter(config
					.getUniqueField()));
		}
		return fieldConfigs;
	}

	@Override
	public String toString() {
		return "DocumentAnnotationConfigWrapper [config=" + config + "]";
	}

	@Override
	public FieldableConfigInfo uniqueField() {
		if (config.getUniqueField() != null) {
			return new UniqueFieldConfigAdapter(config.getUniqueField());
		}
		return null;
	}

}
