package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.convert.FieldConvertor;
import com.hunter.lucene.util.convert.StringValueOfConvertor;

@Target( { ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueField {
	/**
	 * field name
	 * 
	 * @return
	 */
	String name();

	/**
	 * 应用的转换器
	 * 
	 * @return
	 */
	Class<? extends FieldConvertor> convertor() default StringValueOfConvertor.class;

	/**
	 * store 策略,默认为 YES
	 * 
	 * @return
	 */
	Store store() default Store.YES;

	/**
	 * index 策略，默认为 ANALYZED
	 * 
	 * @return
	 */
	Index index() default Index.NOT_ANALYZED;

	/**
	 * termVector 策略，默认为 NO
	 * 
	 * @return
	 */
	TermVector termVector() default TermVector.NO;

	/**
	 * 权重
	 * 
	 * @return
	 */
	float boost() default 1.0f;
}
