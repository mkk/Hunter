package com.hunter.lucene.util.annotation;

/**
 * 区别document。此类 将转换为 field(lucene) ，所以尽量不要与 @Fieldable 的 name 相同
 * 
 * @author bastengao
 * 
 */
public @interface Discriminator {
	String name();

	String value();
}
