package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表示被索引的数据。通常应用在字段，和有返回数据的方法上
 * 
 * @author bastengao
 * 
 */
@Target( { ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldGroup {
	/**
	 * 根据此数据，所要应用的field策略
	 * 
	 * @return
	 */
	Fieldable[] value();

}
