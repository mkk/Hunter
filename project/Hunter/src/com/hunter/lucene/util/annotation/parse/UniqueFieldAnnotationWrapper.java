package com.hunter.lucene.util.annotation.parse;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.UniqueField;

/**
 * 
 * @author bastengao
 *
 */
public class UniqueFieldAnnotationWrapper {
	private UniqueField uniqueField;

	private Class<?> annotationedClass;

	private Method readMethod;

	public UniqueField getUniqueField() {
		return uniqueField;
	}

	public void setUniqueField(UniqueField uniqueField) {
		this.uniqueField = uniqueField;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

	public Method getReadMethod() {
		return readMethod;
	}

	public void setReadMethod(Method readMethod) {
		this.readMethod = readMethod;
	}

	@Override
	public String toString() {
		return "UniqueFieldAnnotationWrapper [annotationedClass="
				+ annotationedClass + ", readMethod=" + readMethod
				+ ", uniqueField=" + uniqueField + "]";
	}

}
