package com.hunter.lucene.util.annotation.parse;

import com.hunter.lucene.util.annotation.Document;
import com.hunter.lucene.util.annotation.Undocument;

/**
 * 
 * @author bastengao
 *
 */
public class AnnotationParser {

	public static boolean isDocumentAnnotated(Class<?> annotatedClass) {
		return annotatedClass.isAnnotationPresent(Document.class);
	}

	public static boolean isUndocumentAnnotated(Class<?> annotatedClass) {
		return annotatedClass.isAnnotationPresent(Undocument.class);
	}
}
