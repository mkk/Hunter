package com.hunter.lucene.util.annotation.parse;

import java.lang.annotation.Annotation;

/**
 * 
 * @author bastengao
 *
 */
public class AnnotationWrapper {
	private Annotation annotation;
	private Class<?> annotationedClass;

	public Annotation getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

}
