package com.hunter.lucene.util.annotation.parse;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.Fieldable;

/**
 * 
 * @author bastengao
 *
 */
public class FieldableAnnotationWrapper {
	private Fieldable annotation;

	private Class<?> annotationedClass;

	private Method readMethod;

	public Fieldable getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Fieldable annotation) {
		this.annotation = annotation;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

	public Method getReadMethod() {
		return readMethod;
	}

	public void setReadMethod(Method readMethod) {
		this.readMethod = readMethod;
	}

	@Override
	public String toString() {
		return "FieldableAnnotationWrapper [annotation=" + annotation
				+ ", annotationedClass=" + annotationedClass + ", readMethod="
				+ readMethod + "]";
	}

}
