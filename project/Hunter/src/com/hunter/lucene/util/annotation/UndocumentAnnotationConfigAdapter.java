package com.hunter.lucene.util.annotation;

import java.util.ArrayList;
import java.util.List;

import com.hunter.lucene.util.annotation.parse.UndocumentAnnotationWrapper;
import com.hunter.lucene.util.annotation.parse.UnfieldableAnnotationWrapper;
import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.UndocumentConfigInfo;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;

/**
 * 通过包装 UndocumentAnnotationWrapper ，实现 UndocumentConfigInfo。
 * 
 * @author bastengao
 * 
 */
public class UndocumentAnnotationConfigAdapter implements UndocumentConfigInfo {
	private UndocumentAnnotationWrapper config;

	private List<UnfieldableConfigInfo> unfieldConfigInfos = null;

	public UndocumentAnnotationConfigAdapter(UndocumentAnnotationWrapper config) {
		this.config = config;
	}

	@Override
	public DiscriminatorValue discrimination() {
		return new DiscriminatorValue(config.getAnnotation().discrimination());
	}

	@Override
	public Class<?> undocumentedClass() {
		return config.getAnnotationedClass();
	}

	@Override
	public List<UnfieldableConfigInfo> unfieldableConfigs() {
		// cache the unfieldConfigInfos
		if (unfieldConfigInfos == null) {
			List<UnfieldableConfigInfo> unfieldableConfigs = new ArrayList<UnfieldableConfigInfo>(
					config.getUnfieldabelAnnotationWrappers().size());

			for (UnfieldableAnnotationWrapper wrapper : config
					.getUnfieldabelAnnotationWrappers()) {
				unfieldableConfigs.add(new UnfieldableAnnotationConfigAdapter(
						wrapper));
			}
			unfieldConfigInfos = unfieldableConfigs;
		}
		return unfieldConfigInfos;
	}

	@Override
	public String toString() {
		return "UndocumentAnnotationConfigWrapper [config=" + config + "]";
	}

}
