package com.hunter.lucene.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表示将一个 document(lucene) 转换为 被此注解 标注的类
 * 
 * 使用：此注解只能对类使用，这样被他注解的类的方法，和属性才可以使用Unfieldabe 注解。
 * @author bastengao
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Undocument {
	Discriminator discrimination();
}
