package com.hunter.lucene.util.annotation.parse;

import java.util.List;

import com.hunter.lucene.util.annotation.Document;

/**
 * 通过包装 annotation ，得到注解的信息。
 * 
 * @author bastengao
 * 
 */
public class DocumentAnnotationWrapper {

	private Document documentAnnotation;

	private Class<?> annotationedClass;

	private UniqueFieldAnnotationWrapper uniqueField;

	private List<FieldableAnnotationWrapper> fieldAnnotationWrappers;

	public Document getDocumentAnnotation() {
		return documentAnnotation;
	}

	public void setDocumentAnnotation(Document documentAnnotation) {
		this.documentAnnotation = documentAnnotation;
	}

	public Class<?> getAnnotationedClass() {
		return annotationedClass;
	}

	public void setAnnotationedClass(Class<?> annotationedClass) {
		this.annotationedClass = annotationedClass;
	}

	public List<FieldableAnnotationWrapper> getFieldAnnotationWrappers() {
		return fieldAnnotationWrappers;
	}

	public void setFieldAnnotationWrappers(
			List<FieldableAnnotationWrapper> fieldAnnotationWrappers) {
		this.fieldAnnotationWrappers = fieldAnnotationWrappers;
	}

	public UniqueFieldAnnotationWrapper getUniqueField() {
		return uniqueField;
	}

	public void setUniqueField(UniqueFieldAnnotationWrapper uniqueField) {
		this.uniqueField = uniqueField;
	}

	@Override
	public String toString() {
		return "DocumentAnnotationWrapper [annotationedClass="
				+ annotationedClass + ", documentAnnotation="
				+ documentAnnotation + ", fieldAnnotationWrappers="
				+ fieldAnnotationWrappers + ", uniqueField=" + uniqueField
				+ "]";
	}

	

}
