package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.hunter.lucene.util.UserData;

/**
 * 负责统一的映射,用来将搜索到的具体Document 转换为 UserData
 * 
 * @author bastengao
 * 
 */
public interface SearchMapper {
	public UserData searchMap(Document document);
}
