package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

/**
 * 判断是否有能力进行转换
 * 
 * @author Administrator
 * 
 */
public interface Suiteable {
	public boolean isSuite(Document document);
}
