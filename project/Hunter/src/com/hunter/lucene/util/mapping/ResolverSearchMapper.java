package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.hunter.lucene.util.UserData;

/**
 * 将转换工作委托给 resolver
 * @author Administrator
 *
 */
public class ResolverSearchMapper implements SearchMapper {
	private Resolver resolver = null;

	public ResolverSearchMapper(Resolver resolver) {
		this.resolver = resolver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserData searchMap(Document document) {
		Mapper mapper = resolver.getMapper(document);
		if (mapper == null) {
			return null;
		}
		return mapper.searchMap(document);
	}

}
