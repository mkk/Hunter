package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.hunter.lucene.util.UserData;

/**
 * 映射器
 * 
 * @author bastengao
 * 
 * @param <E>
 */
public interface Mapper<E extends UserData> {
	/**
	 * 通过指定类，映射为一个Document
	 * @param e
	 * @return
	 */
	public Document indexMap(E e);

	/**
	 * 通过Document，映射为一个 自定义类
	 * @param document
	 * @return
	 */
	public E searchMap(Document document);

}
