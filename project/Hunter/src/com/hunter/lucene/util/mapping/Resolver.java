package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.hunter.lucene.util.UserData;

/**
 * 映射器的解析器,帮助找到合适的映射器
 * 
 * @author bastengao
 * 
 */
public interface Resolver {
	/**
	 * �õ��Լ����ʵ�ӳ��
	 * 
	 * @param <E>
	 * @param o
	 * @return
	 */
	<E extends UserData> Mapper<E> getMapper(E o);

	/**
	 * �õ��Լ����ʵ�ӳ��
	 * 
	 * @param document
	 * @return
	 */
	@SuppressWarnings("unchecked")
	Mapper getMapper(Document document);
}
