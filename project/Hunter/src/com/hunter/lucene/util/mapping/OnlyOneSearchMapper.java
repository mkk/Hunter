package com.hunter.lucene.util.mapping;

import org.apache.lucene.document.Document;

import com.hunter.lucene.util.UserData;

/**
 * 此类指负责一种 Document 的转换工作。必须为要转换的 Document 提供一个 suiteable。由suiteable 决定是否有能力转换。
 * 
 * @author Administrator
 * 
 */
public class OnlyOneSearchMapper implements SearchMapper {

	@SuppressWarnings("unchecked")
	private Mapper mapper = null;
	private Suiteable suiteable = null;

	@SuppressWarnings("unchecked")
	public OnlyOneSearchMapper(Mapper mapper, Suiteable suiteable) {
		this.mapper = mapper;
		this.suiteable = suiteable;
	}

	@Override
	public UserData searchMap(Document document) {
		if (suiteable.isSuite(document)) {
			return mapper.searchMap(document);
		} else {
			return null;
		}
	}

	public void setSuiteable(Suiteable suiteable) {

	}

}
