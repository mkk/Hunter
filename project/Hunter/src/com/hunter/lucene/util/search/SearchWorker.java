package com.hunter.lucene.util.search;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;

/**
 * IndexSearcher 的包装类。
 * 
 * @author bastengao
 * 
 */
public class SearchWorker {

	private IndexSearcher indexSearcher = null;

	public SearchWorker(IndexSearcher indexSearcher) {
		super();
		this.indexSearcher = indexSearcher;
	}

	/**
	 * 
	 * @param query
	 * @param top
	 * @return
	 */
	public TopDocs excuteQuery(Query query, int top) {
		return null;
	}

	/**
	 * 
	 * @param query
	 * @param collector
	 * @throws IOException
	 */
	public void excuteQuery(Query query, Collector collector)
			throws IOException {
		indexSearcher.search(query, collector);
	}

	/**
	 * 可以添加自己的过滤
	 * 
	 * @param query
	 * @param filter
	 *            过滤可以缩小搜索的范围,相比Query ，更能提高性能。
	 * @param collector
	 * @throws IOException
	 */
	public void excuteQuery(Query query, Filter filter, Collector collector)
			throws IOException {
		indexSearcher.search(query, filter, collector);
	}

	/**
	 * 高亮用的方法。
	 * 
	 * @param basicQuery
	 * @param fieldName
	 * @param docId
	 * @return
	 */
	public String highlighter(Query basicQuery, String fieldName, int docId) {
		return null;
	}

	public Document get(int docId) throws CorruptIndexException, IOException {
		return indexSearcher.doc(docId);
	}

	public void close() {
		try {
			indexSearcher.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
