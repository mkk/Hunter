package com.hunter.lucene.util.search.build;

/**
 * 负责多个对象的创建。
 * 
 * @author bastengao
 * 
 */
public interface ObjectsBuilder {

	/**
	 * 根据docId ，查找，创建对象。如果没有合适的，则反回null。
	 * 
	 * @param docId
	 * @return
	 * @throws ObjectBuildException
	 */
	public Object build(int docId) throws ObjectBuildException;
}
