package com.hunter.lucene.util.search.build;

/**
 * 
 * @author bastengao
 * 
 */
public class ObjectBuildException extends Exception {

	private static final long serialVersionUID = 1L;

	public ObjectBuildException() {
		super();
	}

	public ObjectBuildException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectBuildException(String message) {
		super(message);
	}

	public ObjectBuildException(Throwable cause) {
		super(cause);
	}

}
