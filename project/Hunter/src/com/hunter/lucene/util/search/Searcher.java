package com.hunter.lucene.util.search;

import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;

import com.hunter.lucene.util.search.build.ObjectsBuilder;

/**
 * 
 * @author bastengao
 *
 */
public class Searcher implements Searchable {
	private SearchWorker worker = null;
	private ObjectsBuilder objectsBuilder = null;

	public Searcher(SearchWorker worker, ObjectsBuilder objectsBuilder) {
		super();
		this.worker = worker;
		this.objectsBuilder = objectsBuilder;
	}

	@Override
	public QuerySession createQuerySession(Query query) {
		if (query == null) {
			throw new IllegalArgumentException("query is null");
		}
		return new AllScoreSortedQuerySession(query, worker, objectsBuilder);
	}

	@Override
	public QuerySession createQuerySession(Filter filter, Query query) {
		if (query == null) {
			throw new IllegalArgumentException("query is null");
		}
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		return new AllScoreSortedQuerySession(query, filter, worker,
				objectsBuilder);
	}

	@Override
	public void close() {
		worker.close();
	}

}
