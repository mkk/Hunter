package com.hunter.lucene.util.search.build;

/**
 * 负责某个对象的创建。
 * 
 * @author bastengao
 * 
 */
public interface ObjectBuilder {

	/**
	 * 查询这个对象是否适合创建。
	 * 
	 * @param docId
	 * @return
	 * @throws ObjectBuildException
	 */
	public boolean isBuildable(int docId) throws ObjectBuildException;

	/**
	 * 创建。如果没有能力创建，返回null。
	 * 
	 * @param docId
	 * @return
	 * @throws ObjectBuildException
	 */
	Object build(int docId) throws ObjectBuildException;
}
