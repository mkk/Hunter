package com.hunter.lucene.util.search;

/**
 * 
 * @author bastengao
 *
 */
public class SearchException extends Exception {

	private static final long serialVersionUID = 1L;

	public SearchException(Throwable arg0) {
		super(arg0);
	}

}
