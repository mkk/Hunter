package com.hunter.lucene.util.search;

import java.io.Closeable;

import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;

/**
 * 
 * @author bastengao
 * 
 */
public interface Searchable extends Closeable {
	public QuerySession createQuerySession(Query query);

	public QuerySession createQuerySession(Filter filter, Query query);

	@Override
	public void close();
}
