package com.hunter.lucene.util.search.build;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.document.MapFieldSelector;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;

import com.hunter.lucene.util.config.UndocumentConfigInfo;
import com.hunter.lucene.util.config.UnfieldableConfigInfo;
import com.hunter.lucene.util.config.MappingConfigInfoHolder.MapHolder;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * ObjectBuilder  的简单实现类，他没能能实现 load 特性。
 * @author bastengao
 * 
 */
public class SimpleObjectBuilder implements ObjectBuilder {
	private UndocumentConfigInfo undocumentInfo = null;
	private IndexReader reader = null;
	private FieldSelector discriminatorFieldSelector = null;
	private MapHolder<Class<?>, UnfieldConvertor> convertors = null;

	public SimpleObjectBuilder(UndocumentConfigInfo undocumentInfo,
			MapHolder<Class<?>, UnfieldConvertor> convertors, IndexReader reader) {
		super();
		this.undocumentInfo = undocumentInfo;
		this.convertors = convertors;
		this.reader = reader;

		discriminatorFieldSelector = new MapFieldSelector(undocumentInfo
				.discrimination().getName());
	}

	@Override
	public boolean isBuildable(int docId) throws ObjectBuildException {
		try {
			Fieldable field = reader
					.document(docId, discriminatorFieldSelector).getFieldable(
							undocumentInfo.discrimination().getName());
			if (field == null) {
				return false;
			} else if (undocumentInfo.discrimination().getValue().equals(
					field.stringValue())) {
				return true;
			}
		} catch (CorruptIndexException e) {
			throw new ObjectBuildException(e);
		} catch (IOException e) {
			throw new ObjectBuildException(e);
		}
		return false;
	}

	@Override
	public Object build(int docId) throws ObjectBuildException {

		Class<?> clazz = undocumentInfo.undocumentedClass();
		Object result = null;
		try {
			result = clazz.newInstance();
		} catch (InstantiationException e) {
			throw new ObjectBuildException(e);
		} catch (IllegalAccessException e) {
			throw new ObjectBuildException(e);
		}

		Document doc = null;
		try {
			doc = reader.document(docId);
		} catch (CorruptIndexException e) {
			throw new ObjectBuildException(e);
		} catch (IOException e) {
			throw new ObjectBuildException(e);
		}

		for (UnfieldableConfigInfo unfieldInfo : undocumentInfo
				.unfieldableConfigs()) {
			stuff(unfieldInfo, doc, result);
		}

		return result;
	}

	private void stuff(UnfieldableConfigInfo unfieldInfo, Document document,
			Object object) throws ObjectBuildException {
		String fieldName = unfieldInfo.name();
		Fieldable field = document.getFieldable(fieldName);

		if (field == null) {
			throw new ObjectBuildException("the field of name:" + fieldName
					+ " is not found");
		}

		String value = field.stringValue();

		UnfieldConvertor convertor = convertors.get(unfieldInfo.convertor());
		if (convertor == null) {
			try {
				convertor = unfieldInfo.convertor().newInstance();
				convertors.put(unfieldInfo.convertor(), convertor);
			} catch (InstantiationException e) {
				throw new ObjectBuildException(e);
			} catch (IllegalAccessException e) {
				throw new ObjectBuildException(e);
			}
		}

		Object stuff = convertor.fromStringValue(value, unfieldInfo.type());

		try {
			unfieldInfo.writeMethed().invoke(object, stuff);
		} catch (IllegalArgumentException e) {
			throw new ObjectBuildException(e);
		} catch (IllegalAccessException e) {
			throw new ObjectBuildException(e);
		} catch (InvocationTargetException e) {
			throw new ObjectBuildException(e);
		}
	}

}
