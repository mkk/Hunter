package com.hunter.lucene.util.search.build;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.lucene.index.IndexReader;

import com.hunter.lucene.util.config.UndocumentConfigInfo;
import com.hunter.lucene.util.config.MappingConfigInfoHolder.ClassKeyHolder;
import com.hunter.lucene.util.config.MappingConfigInfoHolder.MapHolder;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 没有支持延迟加载的 ObjectsBuilder
 * @author bastengao
 *
 */
public class NotLazyLoadObjectsBuilder implements ObjectsBuilder {

	private List<ObjectBuilder> objectBuilders = new ArrayList<ObjectBuilder>();
	private MapHolder<Class<?>, UnfieldConvertor> convertors = new ClassKeyHolder<UnfieldConvertor>();
	private Collection<UndocumentConfigInfo> undcoumentConfigInfos = null;

	public NotLazyLoadObjectsBuilder(Collection<UndocumentConfigInfo> undcoumentConfigInfos,
			IndexReader indexReader) {
		this.undcoumentConfigInfos = undcoumentConfigInfos;

		initObjectBuilds(indexReader);
	}

	private void initObjectBuilds(IndexReader indexReader) {
		for (UndocumentConfigInfo undocumentInfo : undcoumentConfigInfos) {
			SimpleObjectBuilder objectBuilder = new SimpleObjectBuilder(
					undocumentInfo, convertors, indexReader);
			objectBuilders.add(objectBuilder);
		}

	}

	@Override
	public Object build(int docId) throws ObjectBuildException {
		for (ObjectBuilder objectBuilder : objectBuilders) {
			if (objectBuilder.isBuildable(docId)) {
				return objectBuilder.build(docId);
			}
		}

		return null;
	}

}
