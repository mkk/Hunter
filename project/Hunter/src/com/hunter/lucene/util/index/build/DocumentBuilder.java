package com.hunter.lucene.util.index.build;

import org.apache.lucene.document.Document;

/**
 * 负责将实际的实例转换为document(lucene).
 * 
 * @author bastengao
 * 
 */
public interface DocumentBuilder {

	/**
	 * 将指定对象转换为 Document(lucene).
	 * 
	 * @param document
	 * @return
	 */
	public Document build(Object document) throws BuildDocumentException;
}
