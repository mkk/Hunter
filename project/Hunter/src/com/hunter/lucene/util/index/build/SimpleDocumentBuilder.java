package com.hunter.lucene.util.index.build;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;

import com.hunter.lucene.util.config.DiscriminatorValue;
import com.hunter.lucene.util.config.DocumentConfigInfo;
import com.hunter.lucene.util.config.FieldableConfigInfo;
import com.hunter.lucene.util.config.MappingConfigInfoHolder.MapHolder;
import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * DocumentBuilder的简单实现.
 * 
 * @author bastengao
 * 
 */
public class SimpleDocumentBuilder implements DocumentBuilder {
	private DocumentConfigInfo configInfo;
	private List<FieldBuilder> fieldBuilders;

	public SimpleDocumentBuilder(DocumentConfigInfo configInfo,
			MapHolder<Class<?>, FieldConvertor> convertors) {
		super();
		this.configInfo = configInfo;

		fieldBuilders = new ArrayList<FieldBuilder>();
		for (FieldableConfigInfo config : configInfo.fieldConfigs()) {
			fieldBuilders.add(new SimpleFieldBuilder(config, convertors));
		}
	}

	@Override
	public Document build(Object document) throws BuildDocumentException {
		Document doc = new Document();
		doc.setBoost(configInfo.boost());

		for (FieldBuilder fieldBuilder : fieldBuilders) {
			doc.add(fieldBuilder.build(document));
		}
		doc.add(createDiscriminator(document));

		return doc;
	}

	private Field createDiscriminator(Object document) {
		DiscriminatorValue disc = configInfo.discriminator();

		return new Field(disc.getName(), disc.getValue(), Store.YES,
				Index.NOT_ANALYZED);
	}

}
