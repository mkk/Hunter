package com.hunter.lucene.util.index.build;

import org.apache.lucene.document.Field;

/**
 * 负责field的转换工作
 * 
 * @author bastengao
 * 
 */
public interface FieldBuilder {

	/**
	 * 将指定数据转换为Field(lucene)
	 * @param data
	 * @return
	 */
	public Field build(Object data)throws BuildDocumentException;
}
