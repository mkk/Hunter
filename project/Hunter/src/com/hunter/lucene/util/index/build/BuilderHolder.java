package com.hunter.lucene.util.index.build;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 
 * @author bastengao
 *
 */
public class BuilderHolder {
	private Map<Class<?>, DocumentBuilder> builders = new ConcurrentHashMap<Class<?>, DocumentBuilder>();

	public DocumentBuilder get(Class<?> data) {
		return builders.get(data);
	}

	public DocumentBuilder putIfAbsent(Class<?> data,
			DocumentBuilder documentBuilder) {
		return ((ConcurrentMap<Class<?>, DocumentBuilder>) builders)
				.putIfAbsent(data, documentBuilder);
	}

	public DocumentBuilder put(Class<?> data, DocumentBuilder documentBuilder) {
		return builders.put(data, documentBuilder);
	}

}
