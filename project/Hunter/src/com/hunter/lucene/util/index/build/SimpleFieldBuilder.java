package com.hunter.lucene.util.index.build;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;

import org.apache.lucene.document.Field;

import com.hunter.lucene.util.config.FieldableConfigInfo;
import com.hunter.lucene.util.config.MappingConfigInfoHolder.MapHolder;
import com.hunter.lucene.util.convert.FieldConvertor;
import com.hunter.lucene.util.convert.FieldConvertor.ConvertToType;

/**
 * 
 * @author bastengao
 *
 */
public class SimpleFieldBuilder implements FieldBuilder {
	private FieldableConfigInfo fieldInfo;
	private MapHolder<Class<?>, FieldConvertor> convertors;

	public SimpleFieldBuilder(FieldableConfigInfo configInfo,
			MapHolder<Class<?>, FieldConvertor> convertors) {
		super();
		this.fieldInfo = configInfo;
		this.convertors = convertors;
	}

	@Override
	public Field build(Object data) throws BuildDocumentException {
		Class<? extends FieldConvertor> convertorClass = fieldInfo.convertor();
		FieldConvertor convertor = convertors.get(convertorClass);
		if (convertor == null) {
			try {
				convertor = convertorClass.newInstance();
				convertors.put(convertorClass, convertor);
			} catch (InstantiationException e) {
				throw new BuildDocumentException(e);
			} catch (IllegalAccessException e) {
				throw new BuildDocumentException(e);
			}
		}
		Object property = null;
		try {
			property = fieldInfo.readMethod().invoke(data);
		} catch (IllegalArgumentException e) {
			throw new BuildDocumentException(e);
		} catch (IllegalAccessException e) {
			throw new BuildDocumentException(e);
		} catch (InvocationTargetException e) {
			throw new BuildDocumentException(e);
		}
		Field field = null;
		if (convertor.convertTo() == ConvertToType.STRING) {
			String value = convertor.toStringValue(property);
			field = new Field(fieldInfo.name(), value, fieldInfo.store(),
					fieldInfo.index(), fieldInfo.termVector());
		} else if (convertor.convertTo() == ConvertToType.READER) {
			Reader value = convertor.toReaderValue(data);
			field = new Field(fieldInfo.name(), value, fieldInfo.termVector());
		}
		field.setBoost(fieldInfo.boost());
		return field;
	}

}
