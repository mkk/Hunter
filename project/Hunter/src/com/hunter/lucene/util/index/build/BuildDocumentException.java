package com.hunter.lucene.util.index.build;

public class BuildDocumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuildDocumentException() {
		super();
	}

	public BuildDocumentException(String message, Throwable cause) {
		super(message, cause);
	}

	public BuildDocumentException(String message) {
		super(message);
	}

	public BuildDocumentException(Throwable cause) {
		super(cause);
	}

}
