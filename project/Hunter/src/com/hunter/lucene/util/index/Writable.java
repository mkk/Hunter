/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hunter.lucene.util.index;

import java.io.Closeable;
import java.io.IOException;

/**
 * 负责与索引修改相关的操作。
 * 
 * @author bastengao
 */
public interface Writable extends Closeable {

	/**
	 * 将此对象存储进索引中
	 * 
	 * @param document
	 * @throws IndexException
	 */
	public void add(Object document) throws IndexException;

	/**
	 * 从索引中，删除对应对象,可能会删除与此对象有相同唯一标识的多个对象。
	 * 
	 * @throws UnsupportedOperationException
	 *             如果此对象没有唯一的字段
	 * @param document
	 * @throws IndexException
	 */
	public void remove(Object document) throws IndexException;

	/**
	 * 将此对象存储进索引中，如果对象存在则执行更新。
	 * 
	 * @param document
	 * @throws IndexException
	 */
	public void addOrUpdate(Object document) throws IndexException;

	/**
	 * 更新此对象。删除之前的对象，添加新的对象。不管对象之前是否存在，删除操作都不会失败。所以在之前没有添加，执行此方法等价于add方法。
	 * 
	 * @param document
	 * @throws IndexException
	 */
	public void update(Object document) throws IndexException;

	/**
	 * 提交。执行此方法使之前的增、删、改操作生效(同步到索引中，而不是缓存在内存中。这样子,IndexReader 和 IndexSearcher
	 * 则可对修改变化可见)。
	 * 
	 * @throws IndexException
	 */
	public void commit() throws IndexException;

	/**
	 * 回滚之前的所有操作，到上一次提交的状态。
	 * 
	 * @throws IndexException
	 */
	public void rollback() throws IndexException;

	/**
	 * 关闭。不再使用此对象时，请务必执行此方法。
	 */
	public void close() throws IOException;
}
