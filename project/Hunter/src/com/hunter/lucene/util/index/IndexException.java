package com.hunter.lucene.util.index;

/**
 * 
 * @author bastengao
 *
 */
public class IndexException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IndexException() {
		super();
	}

	public IndexException(String message, Throwable cause) {
		super(message, cause);
	}

	public IndexException(String message) {
		super(message);
	}

	public IndexException(Throwable cause) {
		super(cause);
	}

}
