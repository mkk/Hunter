package com.hunter.lucene.util.config;

import com.hunter.lucene.util.annotation.Discriminator;

/**
 * "@Discrimination" 的包装类型
 * 
 * @author bastengao
 * 
 */
public class DiscriminatorValue {
	private String name;
	private String value;

	public DiscriminatorValue(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public DiscriminatorValue(Discriminator discrimination) {
		super();
		this.name = discrimination.name();
		this.value = discrimination.value();
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiscriminatorValue other = (DiscriminatorValue) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DiscriminationValue [name=" + name + ", value=" + value + "]";
	}

}
