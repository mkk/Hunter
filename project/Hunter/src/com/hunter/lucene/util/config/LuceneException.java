package com.hunter.lucene.util.config;

/**
 * 
 * @author bastengao
 *
 */
public class LuceneException extends Exception {

	private static final long serialVersionUID = 1L;

	public LuceneException() {
		super();
	}

	public LuceneException(String message, Throwable cause) {
		super(message, cause);
	}

	public LuceneException(String message) {
		super(message);
	}

	public LuceneException(Throwable cause) {
		super(cause);
	}

}
