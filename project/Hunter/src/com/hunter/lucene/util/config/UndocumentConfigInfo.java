package com.hunter.lucene.util.config;

import java.util.List;

/**
 * 此类代表 各种Undocument 配置信息,包括注解的配置，XML的配置等。
 * 
 * @author bastengao
 * 
 */
public interface UndocumentConfigInfo {

	public DiscriminatorValue discrimination();

	public Class<?> undocumentedClass();
	
	
	public List<UnfieldableConfigInfo>  unfieldableConfigs();
}
