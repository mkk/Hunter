package com.hunter.lucene.util.config;

import java.lang.reflect.Method;

import com.hunter.lucene.util.annotation.LoadStrategy;
import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 表示 unfield 的配置信息。
 * 
 * @author bastengao
 * 
 */
public interface UnfieldableConfigInfo {

	public String name();

	public LoadStrategy loadStrategy();

	public Class<? extends UnfieldConvertor> convertor();

	public Class<?> type();

	public Method writeMethed();
}
