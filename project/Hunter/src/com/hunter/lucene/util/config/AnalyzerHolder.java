package com.hunter.lucene.util.config;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.lucene.analysis.Analyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 此类负责管理 Analyzer。配置文件中默认的 analyzer 的 name 为 "default"
 * 
 * @author bastengao
 * 
 */
public class AnalyzerHolder implements Injectable<Pair<String, Analyzer>> {
	private static final Logger log = LoggerFactory
			.getLogger(AnalyzerHolder.class);
	private ConcurrentHashMap<String, Analyzer> analyzers = new ConcurrentHashMap<String, Analyzer>();
	private String defaultName = null;
	private Injectable<String> defaultNameInjectable = new Injectable<String>() {

		@Override
		public void inject(String t) {
			log.debug("defaultAnalyzerName[{}]",t);
			defaultName = t;
		}
	};

	public String getDefaultAnalyzerName() {
		return defaultName;
	}

	public Injectable<String> getDefaultAnalyzerNameInjectable() {
		return defaultNameInjectable;
	}

	public Analyzer get(String name) {
		return analyzers.get(name);
	}

	public Analyzer bind(String name, Analyzer analyzer) {
		return analyzers.put(name, analyzer);
	}

	public Analyzer unbind(String name) {
		return analyzers.remove(name);
	}

	public void unbindAll() {
		analyzers.clear();
	}

	@Override
	public void inject(Pair<String, Analyzer> t) {
		log.debug("name:[{}] analyzer:[{}]", t.getOne(), t.getAnother());
		analyzers.put(t.getOne(), t.getAnother());
	}
}
