package com.hunter.lucene.util.config;

/**
 * 注入者,可实施注入。简单的观察者模式。
 * 
 * <pre>
 * {@code
 * Injectable<String> injectable=new Injectable<String>(){
 * 	String foo;
 * 	public void inject(String foo){
 * 		this.foo=foo;
 * 	}
 * }
 * 
 * Injector injector = new Injector();
 * injector.setInjectable(injectable);
 * 
 * injector.doInject("bar");//so foo is "bar"
 * }
 * </pre>
 * 
 * @author bastengao
 * 
 * @param <T>
 */
public class Injector<T> {

	private Injectable<T> injectable;

	/**
	 * 设置将要被注入的对象。
	 * 
	 * @param injectable
	 */
	public void setInjectable(Injectable<T> injectable) {
		this.injectable = injectable;
	}

	/**
	 * 实施注入。将参数注入给被注入者。
	 * 
	 * @param t
	 */
	public void doInject(T t) {
		injectable.inject(t);
	}
}
