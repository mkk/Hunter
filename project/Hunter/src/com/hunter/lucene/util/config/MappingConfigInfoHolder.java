package com.hunter.lucene.util.config;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 映射信息的持有者，他同时拥有 DocumentConfigInfo 和 UndocumentConfigInfo 的信息。
 * 
 * @author bastengao
 * 
 */
public class MappingConfigInfoHolder {
	private InjectableHolder<DocumentConfigInfo> documentConfigs = new InjectableHolder<DocumentConfigInfo>();
	private InjectableHolder<UndocumentConfigInfo> undocumentConfigs = new InjectableHolder<UndocumentConfigInfo>();

	public MappingConfigInfoHolder() {
		super();
	}

	/**
	 * 返回 DocumentConfigInfo 信息的持有者。
	 * 
	 * @return
	 */
	public InjectableHolder<DocumentConfigInfo> getDocumentConfigs() {
		return documentConfigs;
	}

	/**
	 * 返回 UndocumentConfigInfo 信息的持有者。
	 * 
	 * @return
	 */
	public InjectableHolder<UndocumentConfigInfo> getUndocumentConfigs() {
		return undocumentConfigs;
	}

	public static class MapHolder<K, V> {
		private Map<K, V> map = new ConcurrentHashMap<K, V>();

		public V put(K k, V v) {
			return map.put(k, v);
		}

		public V get(K k) {
			return map.get(k);
		}

		public Set<Entry<K, V>> entrySet() {
			return map.entrySet();
		}

		public Collection<V> values() {
			return map.values();
		}
	}

	public static class ClassKeyHolder<T> extends MapHolder<Class<?>, T> {

		@Override
		public T put(Class<?> k, T v) {
			return super.put(k, v);
		}

		@Override
		public T get(Class<?> k) {
			return super.get(k);
		}

	}

	public static class InjectableHolder<T> extends ClassKeyHolder<T> implements
			Injectable<Pair<Class<?>, T>> {

		@Override
		public void inject(Pair<Class<?>, T> t) {
			super.put(t.getOne(), t.getAnother());
		}

	}

}
