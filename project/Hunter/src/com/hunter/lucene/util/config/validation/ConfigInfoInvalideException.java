package com.hunter.lucene.util.config.validation;

/**
 * 
 * @author bastengao
 *
 */
public class ConfigInfoInvalideException extends Exception {

	private static final long serialVersionUID = 1L;

	protected ConfigInfoInvalideException() {
		super();
	}

	protected ConfigInfoInvalideException(String message, Throwable cause) {
		super(message, cause);
	}

	protected ConfigInfoInvalideException(String message) {
		super(message);
	}

	protected ConfigInfoInvalideException(Throwable cause) {
		super(cause);
	}

}
