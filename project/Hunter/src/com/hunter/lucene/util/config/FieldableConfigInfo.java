package com.hunter.lucene.util.config;

import java.lang.reflect.Method;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.Field.TermVector;

import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * 表示 field 配置信息。
 * 
 * @author bastengao
 * 
 */
public interface FieldableConfigInfo {

	public String name();

	public Store store();

	public Index index();

	public TermVector termVector();

	public float boost();

	public Class<? extends FieldConvertor> convertor();

	public Class<?> type();

	public Method readMethod();
}
