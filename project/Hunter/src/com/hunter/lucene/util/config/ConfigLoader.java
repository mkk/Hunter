package com.hunter.lucene.util.config;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.Directory;

/**
 * 负责配置信息的加载和解析。
 * <p>
 * 此类可返回若干注入器。如果想被注入，可以据通过设置 Injectable,期望得到自己关注的东西。 然后调用 <code>load()</code>
 * 则可对配置信息进行加载和解析,在 load 期间将进行注入。 如果未对注入器设置Injectable，则很可能在调用
 * <code>load()</code> 方法时，出现NullpointerException。
 * </p>
 * 
 * @author bastengao
 * 
 */
public interface ConfigLoader {

	/**
	 * 返回分词器的注入者。可对分词器进行注入。
	 * 
	 * @return
	 */
	public Injector<Pair<String, Analyzer>> getAnalyzerInjector();

	/**
	 * 返回默认分词器名称注入者。
	 * 
	 * @return
	 */
	public Injector<String> getDefaultAnalyzerNameInjector();

	/**
	 * 返回Directory 的注入者。
	 * 
	 * @return
	 */
	public Injector<Directory> getDirectoryInjector();

	/**
	 * 返回 DocumentConfigInfo 的注入者。
	 * 
	 * @return
	 */
	public Injector<Pair<Class<?>, DocumentConfigInfo>> getDocumentConfigInfoInjector();

	/**
	 * 返回 UndocumentConfigInfo 的注入者。
	 * 
	 * @return
	 */
	public Injector<Pair<Class<?>, UndocumentConfigInfo>> getUndocumentConfigInfoInjector();

	/**
	 * 加载配置文件的信息。只对其数据进行物理上的验证。并不其逻辑性进行验证。
	 * 
	 * @param configPath
	 * @throws ConfigLoadException
	 */
	public void load(String configPath) throws ConfigLoadException;
}
