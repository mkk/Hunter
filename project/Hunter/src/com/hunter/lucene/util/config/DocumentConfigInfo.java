package com.hunter.lucene.util.config;

import java.util.List;

/**
 * 此类代表 各种Document 配置信息,包括注解的配置，XML的配置等。
 * 
 * @author bastengao
 * 
 */
public interface DocumentConfigInfo {

	public String analyzerName();

	public Class<?> documentedClass();

	public DiscriminatorValue discriminator();
	
	public float boost();
	
	public FieldableConfigInfo uniqueField();

	public List<FieldableConfigInfo> fieldConfigs();
}
