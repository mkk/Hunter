package com.hunter.lucene.util.config;

/**
 * 将一对对象关联在一起。
 * 
 * @author bastengao
 * 
 * @param <M>
 * @param <N>
 */
public class Pair<M, N> {

	private M one;

	private N another;

	protected Pair(M one, N another) {
		super();
		this.one = one;
		this.another = another;
	}

	public M getOne() {
		return one;
	}

	public N getAnother() {
		return another;
	}

}
