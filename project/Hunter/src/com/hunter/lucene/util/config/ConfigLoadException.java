package com.hunter.lucene.util.config;

/**
 * 加载配置信息时，可能会抛出的异常。
 * 
 * @author bastengao
 * 
 */
public class ConfigLoadException extends Exception {

	private static final long serialVersionUID = 1L;

	protected ConfigLoadException() {
		super();
	}

	protected ConfigLoadException(String message, Throwable cause) {
		super(message, cause);
	}

	protected ConfigLoadException(String message) {
		super(message);
	}

	protected ConfigLoadException(Throwable cause) {
		super(cause);
	}

}
