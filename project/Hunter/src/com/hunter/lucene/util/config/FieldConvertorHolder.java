package com.hunter.lucene.util.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.hunter.lucene.util.convert.FieldConvertor;

/**
 * 所有FieldConvertor的持有者。
 * 
 * @author bastengao
 * 
 */
public class FieldConvertorHolder {
	private Map<Class<? extends FieldConvertor>, FieldConvertor> map = new ConcurrentHashMap<Class<? extends FieldConvertor>, FieldConvertor>();

	/**
	 * 先找符合的 convertor, 如果没有找到，则创建。
	 * 
	 * @param convertorClass
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public FieldConvertor get(Class<? extends FieldConvertor> convertorClass)
			throws InstantiationException, IllegalAccessException {
		FieldConvertor convertor = map.get(convertorClass);

		if (convertor == null) {
			convertor = convertorClass.newInstance();
			map.put(convertorClass, convertor);
		}
		return convertor;
	}

}
