package com.hunter.lucene.util.config;

/**
 * 可被注入器注入自己关心的数据。
 * 
 * @author bastengao
 * 
 * @param <T>
 */
public interface Injectable<T> {
	public void inject(T t);
}
