package com.hunter.lucene.util.config;

/**
 * 
 * @author bastengao
 *
 */
class ConfigInfoPrinter {

	public static String print(DocumentConfigInfo configInfo) {
		StringBuilder buff = new StringBuilder();

		buff.append("\ndocument [class=\"" + configInfo.documentedClass()
				+ "\"");
		buff.append(" analyzer=\"" + configInfo.analyzerName() + "\"");
		buff.append(" dircriminator=(name:'"
				+ configInfo.discriminator().getName() + "',type:'"
				+ configInfo.discriminator().getValue() + "')");
		buff.append(" boost=\"" + configInfo.boost() + "\"");
		buff.append("]");
		if (configInfo.uniqueField() != null) {
			FieldableConfigInfo fieldInfo = configInfo.uniqueField();
			buff.append("\n\tuniqueField [name=\"" + fieldInfo.name() + "\"");
			buff.append(" store=\"" + fieldInfo.store() + "\"");
			buff.append(" index=\"" + fieldInfo.index() + "\"");
			buff.append(" termVector=\"" + fieldInfo.termVector() + "\"");
			buff.append(" boost=\"" + fieldInfo.boost() + "\"");
			buff.append(" type=\"" + fieldInfo.type() + "\"");
			buff.append(" readMethod=\"" + fieldInfo.readMethod().getName()
					+ "\"");
			buff.append(" convertor=\"" + fieldInfo.convertor() + "\"");
			buff.append("]");
		}
		for (FieldableConfigInfo fieldInfo : configInfo.fieldConfigs()) {
			buff.append("\n\tfield [name=\"" + fieldInfo.name() + "\"");
			buff.append(" store=\"" + fieldInfo.store() + "\"");
			buff.append(" index=\"" + fieldInfo.index() + "\"");
			buff.append(" termVector=\"" + fieldInfo.termVector() + "\"");
			buff.append(" boost=\"" + fieldInfo.boost() + "\"");
			buff.append(" type=\"" + fieldInfo.type() + "\"");
			buff.append(" readMethod=\"" + fieldInfo.readMethod().getName()
					+ "\"");
			buff.append(" convertor=\"" + fieldInfo.convertor() + "\"");
			buff.append("]");
		}
		return buff.toString();
	}

	public static String print(UndocumentConfigInfo configInfo) {
		StringBuilder buff = new StringBuilder();

		buff.append("\nundocument [");
		buff.append("class=\"" + configInfo.undocumentedClass() + "\"");
		buff.append(" discriminator=(name:'"
				+ configInfo.discrimination().getName() + "',type:'"
				+ configInfo.discrimination().getValue() + "')");
		buff.append("]");

		for (UnfieldableConfigInfo unfieldInfo : configInfo
				.unfieldableConfigs()) {
			buff.append("\n\tunfield [");
			buff.append("name=\"" + unfieldInfo.name() + "\"");
			buff.append(" loadStrategy=\"" + unfieldInfo.loadStrategy() + "\"");
			buff.append(" type=\"" + unfieldInfo.type() + "\"");
			buff.append(" writeMethed=\"" + unfieldInfo.writeMethed() + "\"");
			buff.append(" convertor=\"" + unfieldInfo.convertor() + "\"");
			buff.append("]");
		}
		return buff.toString();
	}
}
