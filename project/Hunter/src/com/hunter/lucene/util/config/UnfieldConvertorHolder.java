package com.hunter.lucene.util.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.hunter.lucene.util.convert.UnfieldConvertor;

/**
 * 
 * @author bastengao
 *
 */
public class UnfieldConvertorHolder {
	private Map<Class<? extends UnfieldConvertor>, UnfieldConvertor> map = new ConcurrentHashMap<Class<? extends UnfieldConvertor>, UnfieldConvertor>();

	/**
	 * 先找符合的 convertor, 如果没有找到，则创建。
	 * 
	 * @param convertorClass
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public UnfieldConvertor get(
			Class<? extends UnfieldConvertor> convertorClass)
			throws InstantiationException, IllegalAccessException {
		UnfieldConvertor convertor = map.get(convertorClass);

		if (convertor == null) {
			convertor = convertorClass.newInstance();
			map.put(convertorClass, convertor);
		}
		return convertor;
	}
}
