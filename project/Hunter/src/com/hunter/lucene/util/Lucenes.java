/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hunter.lucene.util;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.TermFreqVector;
import org.apache.lucene.index.TermPositionVector;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

/**
 * 提供 Lucene 的一些工具方法。
 * @author bastengao
 */
public class Lucenes {

	public static Directory getSimpleFileSystemDirectory(String path)
			throws IOException {
		return new SimpleFSDirectory(new File(path));
	}

	public static Directory getRAMDirectory() {
		return new RAMDirectory();
	}

	public static Analyzer getStandardAnalyzer() {
		return new StandardAnalyzer(Version.LUCENE_30);
	}

	/**
	 * 返回的分词器，最好被缓存起来。
	 * 
	 * @return
	 */
	public static Analyzer getChineseAnaylzer() {
		// temporarily do not know use which analyzer for Chinese and how to
		// use.
		return IKAnalyzerHolder.ANALYZER.getAnaylzer();
	}

	/**
	 * 这个构造的 IndexWriter first creating it if it does not already exist default
	 * IndexDeletionPolicy is KeepOnlyLastCommitDeletionPolicy
	 * 
	 * @param directory
	 * @param analyzer
	 * @param create
	 * @return
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public static IndexWriter createIndexWriter(Directory directory,
			Analyzer analyzer) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		return new IndexWriter(directory, analyzer,
				IndexWriter.MaxFieldLength.UNLIMITED);
	}

	public static IndexWriter createIndexWriter(Directory directory,
			Analyzer analyzer, boolean create) throws CorruptIndexException,
			LockObtainFailedException, IOException {
		return new IndexWriter(directory, analyzer, create,
				IndexWriter.MaxFieldLength.UNLIMITED);
	}

	public static String highlighte(int docId, String fieldName,
			String sourceText, IndexReader indexReader, Highlighter highlighter)
			throws IOException, InvalidTokenOffsetsException {
		TermFreqVector tfv = indexReader.getTermFreqVector(docId, fieldName);
		TokenStream tokenStream = TokenSources
				.getTokenStream((TermPositionVector) tfv);
		return highlighter.getBestFragment(tokenStream, sourceText);
	}

	/**
	 * 枚举单例
	 * 
	 * @author bastengao
	 * 
	 */
	public enum IKAnalyzerHolder {
		ANALYZER;
		private Analyzer analyzer = new IKAnalyzer();

		public Analyzer getAnaylzer() {
			return analyzer;
		}
	}
}
