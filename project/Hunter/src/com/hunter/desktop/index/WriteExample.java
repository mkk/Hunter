package com.hunter.desktop.index;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import com.hunter.desktop.file.FileData;
import com.hunter.desktop.file.fetch.Chooser;
import com.hunter.desktop.file.fetch.FileFetcher;
import com.hunter.desktop.file.fetch.support.AllFile;
import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.index.IndexException;
import com.hunter.lucene.util.index.Writable;

public class WriteExample {
	public static void main(String[] args) throws ConfigLoadException,
			ConfigInfoInvalideException, LuceneException, IndexException,
			IOException {

		Configuration configuration = Configuration.build("file.xml");

		Writable writer = configuration.getOrBuildWriter(true);

		File[] files = new File[] { new File("D:\\") };
		Chooser chooser = new AllFile();
		FileFetcher fetcher = new FileFetcher(files, chooser);

		Enumeration<FileData> enumeration = fetcher.iterator();

		while (enumeration.hasMoreElements()) {
			FileData file = enumeration.nextElement();
			System.out.println(file.getAbsolutePath());

			writer.update(file);

		}

		writer.commit();

		writer.close();

	}
}
