package com.hunter.desktop.index;

import java.io.File;
import java.util.Date;
import java.util.Enumeration;

import com.hunter.desktop.file.FileData;
import com.hunter.desktop.file.fetch.Chooser;
import com.hunter.desktop.file.fetch.FileFetcher;
import com.hunter.desktop.file.fetch.support.ChooserClause;
import com.hunter.desktop.file.fetch.support.ChooserGroup;
import com.hunter.desktop.file.fetch.support.DirectoryOnly;
import com.hunter.desktop.file.fetch.support.FileOnly;
import com.hunter.desktop.file.fetch.support.NotHidden;
import com.hunter.desktop.file.fetch.support.PostfixFileChooser;
import com.hunter.desktop.file.fetch.support.TimeSpanChooser;
import com.hunter.desktop.file.fetch.support.ChooserClause.Occur;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.index.Writable;

public class ChooserExample {
	public static void main(String[] args) throws Exception {
		Configuration configuration = Configuration.build("file.xml");

		Writable writer = configuration.getOrBuildWriter(true);

		File[] files = new File[] { new File("D:\\") };

		// 其他的都不用管，就看Chooser 就行了。
		Chooser chooser = null;
		// example 1 只选择 文件和文件夹
		ChooserGroup group1 = new ChooserGroup();

		ChooserClause file = new ChooserClause(new FileOnly(), Occur.SHOULD);
		group1.add(file);

		ChooserClause directory = new ChooserClause(new DirectoryOnly(),
				Occur.SHOULD);
		group1.add(directory);

		chooser = group1;

		// example 2 选择非隐藏的 文件和文件夹
		ChooserGroup group2 = new ChooserGroup();
		// must 表示 选择的条件里，这个必须满足。先添加的先执行，所以添加的顺序不同，表示的意思也不同
		ChooserClause noHidden = new ChooserClause(new NotHidden(), Occur.MUST);
		group2.add(noHidden);// noHidden 必须在 file directory 的前面,这样子才能在他们之前起作用
		group2.add(file);
		group2.add(directory);

		chooser = group2;

		// example 3 选择时间段
		ChooserGroup group3 = new ChooserGroup();

		Date start = new Date();// 起始时间
		Date end = new Date();// 结束时间
		// 同样，也必须是must,且必须在他们之前
		ChooserClause timeSpan = new ChooserClause(new TimeSpanChooser(start,
				end), Occur.MUST);
		group3.add(timeSpan);
		group3.add(file);
		group3.add(directory);

		chooser = group3;

		// example 4 选择特定后缀
		// 这个跟其他的不太一样，因为后缀只对文件有效，文件夹没有后缀
		ChooserGroup group4 = new ChooserGroup();

		ChooserGroup fileGroup = new ChooserGroup();
		ChooserClause onlyFile = new ChooserClause(new FileOnly(), Occur.MUST);
		ChooserClause postfixChooser = new ChooserClause(
				new PostfixFileChooser("txt", "gif"), Occur.MUST);
		// 这个顺序无所谓，因为都是 must ，前面无所谓
		fileGroup.add(onlyFile);
		fileGroup.add(postfixChooser);

		ChooserClause postfixFile = new ChooserClause(fileGroup, Occur.SHOULD);

		group4.add(postfixFile);
		group4.add(directory);

		chooser = group4;
		//其他都只是这样子组合

		FileFetcher fetcher = new FileFetcher(files, chooser);

		Enumeration<FileData> enumeration = fetcher.iterator();

		while (enumeration.hasMoreElements()) {
			FileData f = enumeration.nextElement();
			System.out.println(f.getAbsolutePath());

			writer.update(f);

		}

		writer.commit();

		configuration.close();
	}
}
