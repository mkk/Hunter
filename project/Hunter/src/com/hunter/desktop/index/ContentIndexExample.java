package com.hunter.desktop.index;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import com.hunter.desktop.file.FileData;
import com.hunter.desktop.file.content.ContentChooser;
import com.hunter.desktop.file.content.IndexAllContentChooser;
import com.hunter.desktop.file.fetch.Chooser;
import com.hunter.desktop.file.fetch.FileFetcher;
import com.hunter.desktop.file.fetch.support.AllFile;
import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.index.IndexException;
import com.hunter.lucene.util.index.Writable;

public class ContentIndexExample {
	public static void main(String[] args) throws ConfigLoadException,
			ConfigInfoInvalideException, LuceneException, IndexException,
			IOException {
		Configuration configuration = Configuration.build("file.xml");

		Writable writer = configuration.getOrBuildWriter(true);

		File[] files = new File[] { new File("D:\\") };
		Chooser chooser = new AllFile();// 这个是对文件的选择，可以用更合适的，我这里只是用 AllFile() 代替。

		
		ContentChooser contentChooser = new IndexAllContentChooser();//这个是对内容建索引的选择，我用的是全部建索引，也就是凡是通过chooser 选择的（不是 contentChooser），就给内容建索引
		FileFetcher fetcher = new FileFetcher(files, chooser, contentChooser);
		//如果不想对内容建索引，就用这个构造方法  new FileFetcher(files, chooser），或者这样
		// new FileFetcher(files, chooser ,new NeverContentChooser());
		

		Enumeration<FileData> enumeration = fetcher.iterator();

		while (enumeration.hasMoreElements()) {
			FileData file = enumeration.nextElement();
			System.out.println(file.getAbsolutePath());

			writer.update(file);

		}

		writer.commit();

		configuration.close();
	}
}
