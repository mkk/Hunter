/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hunter.desktop.file;

/**
 * this class is used for declaring some constant string of fieldName in order
 * to index
 * 
 * @author bastengao
 */
public enum FileType {

	FILE("file"), DIRECTORY("directory");

	FileType(String type) {
		this.typeValue = type;
	}

	public final String typeValue;

}
