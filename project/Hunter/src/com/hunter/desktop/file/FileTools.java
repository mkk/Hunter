package com.hunter.desktop.file;

import org.apache.lucene.document.DateTools;

/**
 * 与文件相关的一常用方法，及数据表示的工具类
 * 
 * @author bastengao
 * 
 */
public class FileTools {
	/**
	 * 返回文件的后缀名
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getPostfix(String fileName) {
		// 没有处理第一个字符是点的情况
		String post = "";
		int index = fileName.lastIndexOf(".");
		if (index != -1) {
			post = fileName.substring(index + 1);
		}
		return post;
	}

	public static void print(FileData fileData) {
		System.out.println("fileName: " + fileData.getFileName());
		System.out.println("path: " + fileData.getAbsolutePath());
		System.out.println("longTime: " + fileData.getLastModified());
		System.out.println("dateTime: "
				+ DateTools.timeToString(fileData.getLastModified(),
						DateTools.Resolution.MINUTE));
		System.out.println("type: " + fileData.getTypeField().getValue());
		if (fileData instanceof RealFile) {
			print((RealFile) fileData);
		}
		System.out.println();
	}

	private static void print(RealFile realFile) {
		System.out.println("length: " + realFile.getLength());
		System.out.println("postfix: " + realFile.getPostfix());
	}
}
