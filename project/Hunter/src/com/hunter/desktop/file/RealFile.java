package com.hunter.desktop.file;

import java.io.Reader;

import com.hunter.lucene.util.mapping.TypeFieldResolver.Type;

/**
 * 表示一个实际的文件(不同于文件夹)
 * 
 * @author bastengao
 * 
 */
public class RealFile extends FileData {
	/**
	 * 用于TypeFieldResolver 的 Type 的存储
	 */
	public static final Type TYPE = new Type(FieldNameConstants.TYPE,
			FileType.FILE.typeValue);

	private long length;
	private String postfix;
	/**
	 * 考虑到 lucene 在 创建 field 的时候 如果是 reader ,没有提供 Field.Store 的参数,他对 reader
	 * 的处理方式是 Store.NO 也就意味着他不能被存储，这点主要影响他的高亮功能。
	 * 因为在处理高亮时，需要高亮的原文，那么就有两种方式可以提供原文给他，要么从文件读取； 要么索引时存储在索引中，高亮时再取。
	 * 其实第二种方法更好一些，虽然可能会增加索引的大小，但会提高亮的速度。 但是第二种方法没有办法实施。所以只能用个折中的方法：对内容索引，利用
	 * reader，方便搜索; 对内容的开始部分索引，利用 string 在索引中储存，方便高亮。
	 */
	private Reader readerContent;// 内容的 Reader,用于搜索
	private String stringContent;// 部分内容的 String,用于高亮

	public RealFile() {
		super();
	}

	public Reader getReaderContent() {
		return readerContent;
	}

	public void setReaderContent(Reader readerContent) {
		this.readerContent = readerContent;
	}

	public String getStringContent() {
		return stringContent;
	}

	public void setStringContent(String stringContent) {
		this.stringContent = stringContent;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	@Override
	public Type getTypeField() {
		return TYPE;
	}
}
