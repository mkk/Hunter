package com.hunter.desktop.file.mapping;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import com.hunter.desktop.file.RealDirectory;
import com.hunter.lucene.util.mapping.Mapper;

/**
 * 
 * @author bastengao
 *
 */
public class RealDirectoryMapper implements Mapper<RealDirectory> {

	private FileMapper fileMapper = new FileMapper();

	@Override
	public Document indexMap(RealDirectory e) {
		Document document = fileMapper.indexMap(e);
		Field typeField = new Field(e.getTypeField().getFieldName(), e
				.getTypeField().getValue(), Field.Store.YES,
				Field.Index.NOT_ANALYZED);
		document.add(typeField);
		return document;
	}

	@Override
	public RealDirectory searchMap(Document document) {
		RealDirectory realDirectory = new RealDirectory();
		fileMapper.searchMap(realDirectory, document);
		return realDirectory;
	}

}
