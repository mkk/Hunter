/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hunter.desktop.file.fetch;

import java.util.Enumeration;

import com.hunter.lucene.util.UserData;

/**
 * 抓取，与遍历的类
 * 
 * @author bastengao
 */
public interface Fecther<E extends UserData> {

	/**
	 * 
	 * @return 返回 iterator
	 */
	public Enumeration<E> iterator();
}
