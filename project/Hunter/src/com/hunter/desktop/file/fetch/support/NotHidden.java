package com.hunter.desktop.file.fetch.support;

import java.io.File;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 
 * @author bastengao
 * 
 */
public class NotHidden implements Chooser {

	@Override
	public boolean accept(File file) {
		return !file.isHidden();
	}

}
