package com.hunter.desktop.file.fetch.support;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 在ChooserGroup 中的一个分 选择器。
 * 
 * @author bastengao
 * 
 */
public class ChooserClause {

	private Chooser chooser;
	private Occur occur;

	public ChooserClause(Chooser chooser, Occur occor) {
		super();
		this.chooser = chooser;
		this.occur = occor;
	}

	public Chooser getChooser() {
		return chooser;
	}

	public Occur getOccur() {
		return occur;
	}

	/**
	 * 出现规则
	 * 
	 * @author bastengao
	 * 
	 */
	public static enum Occur {
		/**
		 * 应该出现，可以，或者不可以
		 */
		SHOULD,
		/**
		 * 必须出现
		 */
		MUST,
		/**
		 * 不能出现
		 */
		NOT;
	}
}
