package com.hunter.desktop.file.fetch.support;

import java.io.File;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 接受所有的文件
 * @author bastengao
 *
 */
public class AllFile implements Chooser {

	@Override
	public boolean accept(File file) {
		return true;
	}

}
