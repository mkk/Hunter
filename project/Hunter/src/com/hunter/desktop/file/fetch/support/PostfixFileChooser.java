package com.hunter.desktop.file.fetch.support;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.hunter.desktop.file.FileTools;
import com.hunter.desktop.file.fetch.Chooser;

/**
 * 
 * @author bastengao
 * 
 */
public class PostfixFileChooser implements Chooser {

	private Set<String> postfixes = null;

	public PostfixFileChooser(String... postfixes) {
		if (postfixes.length == 0) {
			throw new IllegalArgumentException("postfixes length must not be zero.");
		}
		this.postfixes = new HashSet<String>(Arrays.asList(postfixes));
	}

	public PostfixFileChooser(Collection<String> postfixes) {
		if (postfixes.size() == 0) {
			throw new IllegalArgumentException("postfixes length must not be zero.");
		}
		this.postfixes = new HashSet<String>(postfixes);
	}

	/**
	 * 判断此文件的后缀是是否在后缀集合中。
	 * 
	 * @return 如果后缀在后缀集合中，返回true,否则反之。如果是文件夹直接返回 false。
	 */
	@Override
	public boolean accept(File file) {
		if (file.isFile()) {
			String fileName = file.getName();
			String postfix = FileTools.getPostfix(fileName);
			return postfixes.contains(postfix);
		} else {
			return false;
		}
	}

}
