package com.hunter.desktop.file.fetch.support;

import java.io.File;
import java.util.Date;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 
 * @author bastengao
 * 
 */
public class TimeSpanChooser implements Chooser {

	// 起始时间
	private long startTime;
	// 终止时间
	private long endTime;

	public TimeSpanChooser(Date start, Date end) {
		startTime = start.getTime();
		endTime = end.getTime();
	}

	public TimeSpanChooser(long startTime, long endTime) {
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/**
	 * 判断此文件的最后修改时间是否在此时间区间内（包括两点）。
	 */
	@Override
	public boolean accept(File file) {
		long lastModifid = file.lastModified();
		return lastModifid >= startTime && lastModifid <= endTime;
	}

}
