package com.hunter.desktop.file.fetch.support;

import java.io.File;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 
 * @author bastengao
 */
public class DirectoryOnly implements Chooser {

	@Override
	public boolean accept(File file) {
		return valueOf(file);
	}

	private boolean valueOf(File file) {
		return file.isDirectory();
	}
}
