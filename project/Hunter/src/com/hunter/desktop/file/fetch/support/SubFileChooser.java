package com.hunter.desktop.file.fetch.support;

import java.io.File;

import com.hunter.desktop.file.fetch.Chooser;

/**
 * 选择某个目录下面的文件(含文件夹)
 * 
 * @author bastengao
 * 
 */
public class SubFileChooser implements Chooser {

	public SubFileChooser() {

	}

	@Override
	public boolean accept(File file) {
		throw new UnsupportedOperationException();
	}

}
