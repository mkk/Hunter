/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hunter.desktop.file.fetch;

import java.io.File;

/**
 * 文件选择
 * 
 * @author bastengao
 */
public interface Chooser {

	/**
	 * 决定些文件是否应该被索引
	 * 
	 * @param file
	 * @return 如果文件被选择返回true,否则返回 false
	 */
	public boolean accept(File file);
}
