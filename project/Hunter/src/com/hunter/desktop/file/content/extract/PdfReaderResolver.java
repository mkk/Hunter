package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import com.hunter.desktop.file.FileTools;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

public class PdfReaderResolver implements ReaderResolver {
	public static final String POSTFIX = "pdf";

	@Override
	public Reader read(File file) throws ReaderResolveException {
		String postfix = FileTools.getPostfix(file.getName());
		if (!POSTFIX.equals(postfix)) {
			throw new ReaderResolveException("the " + postfix
					+ " file is not supported.");
		}

		PDFTextStripper stripper;
		PDDocument doc;
		ByteOutputStream in = new ByteOutputStream();
		try {
			stripper = new PDFTextStripper();
			doc = PDDocument.load(new File(
					"E:\\java\\Java工具\\poi\\poi-3.6\\docs\\index.pdf"));
			stripper.writeText(doc, new OutputStreamWriter(in));
		} catch (IOException e) {
			throw new ReaderResolveException(e);
		}

		byte[] data = in.getBytes();
		Reader reader = new InputStreamReader(new ByteInputStream(data, 0,
				data.length));
		return reader;
	}

}
