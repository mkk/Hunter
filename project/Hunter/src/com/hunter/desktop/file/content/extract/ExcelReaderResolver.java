package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.hunter.desktop.file.FileTools;

public class ExcelReaderResolver extends OfficeReaderResolver {
	public static final String POSTFIX = "xls";

	@Override
	protected POIOLE2TextExtractor createExtractor(File file)
			throws ReaderResolveException {

		try {
			return new ExcelExtractor(new POIFSFileSystem(new FileInputStream(
					file)));
		} catch (FileNotFoundException e) {
			throw new ReaderResolveException(e);
		} catch (IOException e) {
			throw new ReaderResolveException(e);
		}
	}

	@Override
	protected boolean isSupported(File file) {
		String postfix = FileTools.getPostfix(file.getName());
		return POSTFIX.equals(postfix);
	}

}
