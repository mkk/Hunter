package com.hunter.desktop.file.content;

import java.io.File;

/**
 * 不对任何文件内容索引
 * @author bastengao
 *
 */
public class NeverContentChooser implements ContentChooser {

	@Override
	public boolean index(File file) {
		return false;
	}

}
