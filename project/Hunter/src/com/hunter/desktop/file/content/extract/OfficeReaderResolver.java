package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;

import org.apache.poi.POIOLE2TextExtractor;

import com.hunter.desktop.file.FileTools;

/**
 * 解析office文档的抽象类(不包括2007)。可支持 word(doc),powerPoint (ppt),excel(xls)
 * @author bastengao
 *
 */
public abstract class OfficeReaderResolver implements ReaderResolver {

	@Override
	public Reader read(File file) throws ReaderResolveException {
		if (!isSupported(file)) {
			String postfix = FileTools.getPostfix(file.getName());
			throw new ReaderResolveException("the " +postfix+" file is not supported.");
		}
		Reader reader = null;
		POIOLE2TextExtractor extractor = createExtractor(file);
		reader = new StringReader(extractor.getText());

		return reader;
	}

	abstract protected POIOLE2TextExtractor createExtractor(File file)
			throws ReaderResolveException;

	abstract protected boolean isSupported(File file);

}
