package com.hunter.desktop.file.content;

/**
 * 文件内容解析异常
 * 
 * @author bastengao
 * 
 */
public class ReaderResolveException extends Exception {

	private static final long serialVersionUID = 1L;

	public ReaderResolveException() {
		super();
	}

	public ReaderResolveException(String arg0) {
		super(arg0);
	}

	public ReaderResolveException(Throwable arg0) {
		super(arg0);
	}

}
