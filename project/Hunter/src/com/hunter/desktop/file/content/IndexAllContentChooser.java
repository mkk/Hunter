package com.hunter.desktop.file.content;

import java.io.File;

/**
 * 对所有文件的内容索引
 * 
 * @author bastengao
 * 
 */
public class IndexAllContentChooser implements ContentChooser {

	@Override	
	public boolean index(File file) {
		return true;
	}

}
