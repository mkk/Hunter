package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.hunter.desktop.file.FileTools;

public class PowerPointReaderResolver extends OfficeReaderResolver {
	public static final String POSTFIX = "ppt";

	@Override
	protected POIOLE2TextExtractor createExtractor(File file)
			throws ReaderResolveException {

		try {
			return new PowerPointExtractor(new POIFSFileSystem(
					new FileInputStream(file)));
		} catch (FileNotFoundException e) {
			throw new ReaderResolveException(e);
		} catch (IOException e) {
			throw new ReaderResolveException(e);
		}
	}

	@Override
	protected boolean isSupported(File file) {
		String postfix = FileTools.getPostfix(file.getName());
		return POSTFIX.equals(postfix);
	}

}
