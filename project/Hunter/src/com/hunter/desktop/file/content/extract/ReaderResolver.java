package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.Reader;

/**
 * 一个具体的解析实现
 * @author bastengao
 *
 */
public interface ReaderResolver {
	/**
	 * 返回指定文件的 Reader
	 * @param file
	 * @return
	 * @throws ReaderResolveException
	 */
	public Reader read(File file) throws ReaderResolveException;
}
