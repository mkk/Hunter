package com.hunter.desktop.file.content.extract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.hwpf.extractor.WordExtractor;

import com.hunter.desktop.file.FileTools;

public class WordReaderResolver extends OfficeReaderResolver {
	public static final String POSTFIX = "doc";

	@Override
	protected POIOLE2TextExtractor createExtractor(File file)
			throws ReaderResolveException {
		InputStream in;
		try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new ReaderResolveException(e);
		}
		try {
			return new WordExtractor(in);
		} catch (IOException e) {
			throw new ReaderResolveException(e);
		}
	}

	@Override
	protected boolean isSupported(File file) {
		String postfix = FileTools.getPostfix(file.getName());
		return POSTFIX.equals(postfix);
	}

}
