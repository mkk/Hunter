package com.hunter.desktop.file;

/**
 * 索引搜索时用到的fieldName 的存储类
 * 
 * @author bastengao
 * 
 */
public class FieldNameConstants {
	public static final String ABSOLUTE_PATH = "path";
	public static final String ABSOLUTE_PATH_ANALYZED = "analyzedPath";
	public static final String NAME = "name";
	public static final String NAME_ANALYZED = "analyzedName";
	public static final String IS_HIDDEN = "idHidden";
	public static final String LAST_MODIFY_LONG = "lastModifiedLong";
	public static final String LAST_MODIFY_DATE = "lastModifiedDate";
	public static final String TYPE = "type";
	public static final String POSTFIX = "postfix";
	public static final String LENGTH = "length";
	public static final String CONTENT = "content";
}
