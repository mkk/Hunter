package com.hunter.desktop.file;

import com.hunter.lucene.util.UserData;
import com.hunter.lucene.util.mapping.TypeFieldResolver.Type;

/**
 * 文件索引搜索中表示文件类父类
 * 
 * @author bastengao
 * 
 */
public abstract class FileData implements UserData {
	private String fileName;
	private String absolutePath;
	private long lastModified;
	private boolean hidden;

	public FileData() {
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public abstract Type getTypeField();
}
