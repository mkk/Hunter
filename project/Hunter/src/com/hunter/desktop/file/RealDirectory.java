package com.hunter.desktop.file;

import com.hunter.lucene.util.mapping.TypeFieldResolver.Type;

/**
 * 表示一个实际的文件夹
 * 
 * @author bastengao
 * 
 */
public class RealDirectory extends FileData {
	/**
	 * 用于TypeFieldResolver 的 Type 的存储
	 */
	public static final Type TYPE = new Type(FieldNameConstants.TYPE,
			FileType.DIRECTORY.typeValue);

	@Override
	public Type getTypeField() {

		return TYPE;
	}

}
