package com.hunter.desktop.search;

import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.search.SuggestionConsult;

public class SuggestionConsultExample {
	public static void main(String[] args) throws ConfigLoadException, ConfigInfoInvalideException, LuceneException {
		Configuration configuration = Configuration.build("file.xml");
		
		SuggestionConsult consult=configuration.buildSuggestionConsult("analyzedName");
		
		String [] suggestions=consult.suggest("hello", 5);
		for(String s:suggestions) {
			System.out.println(s);
		}
	}
}
