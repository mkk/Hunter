package com.hunter.desktop.search;

import java.util.Calendar;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.DateTools.Resolution;
import org.apache.lucene.search.Query;

import com.hunter.desktop.file.FileData;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.search.Queries;
import com.hunter.lucene.util.search.QuerySession;
import com.hunter.lucene.util.search.Searchable;

public class SearchExample {

	public static void main(String[] args) throws Exception {
		Configuration config = Configuration.build("file.xml");

		Searchable searcher = config.getOrBuildSearcher();
		// 基本Query////////////////////////////////////////////

		Query hiddenFile = SearchTools.createHiddenFile();// 隐藏文件

		Query realFile = SearchTools.createRealFileQuery();// 纯文件

		// Query realDirectory=SearchTools.createRealDirectory();//文件夹

		Query postfixMp3 = SearchTools.createPostfixQuery("mp3");// 后缀为mp3的文件

		Query postfixHtml = SearchTools.createPostfixQuery("html");// 后缀为html的文件

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2010);
		calendar.set(Calendar.MONTH, 9);
		//Date lower = calendar.getTime();
		calendar.set(Calendar.MONTH, 10);
		//Date upper = calendar.getTime();
		//Query duration = SearchTools.createDateRangeQuery(lower, upper);// 最后修改时间
		// 2010年10月，到2010年11月

		calendar.set(Calendar.YEAR, 2005);
		calendar.set(Calendar.MONTH, 5);
//		Date point = calendar.getTime();
//		Query datePoint = SearchTools.createDatePointQuery(point,
//				Resolution.YEAR);// resolution 设置时间点的解析精确度.如果为
		// YEAR,则代表2005这一年;如果为
		// MONTH,则代表2005年5月，这一个月。

//		Query fileName = SearchTools
//				.createFileNameQueryAnalyzedByIK("userInputFileName");// 用户输入的关键词，由IK分词器解析的文件名查询

		// 以上都是一些基本的Query，由这些基本的Query ，可以组合成一些复杂的Query

		Query hiddenRealFile = Queries.andQuery(new Query[] { hiddenFile,
				realFile });// 隐藏纯文件查询(两个查询为且的关系)

		Query mp3OrHtml = Queries
				.orQuery(new Query[] { postfixMp3, postfixHtml });// 后缀为mp3或者为html的文件(两个查询为或的关系)

		Query bigQuery = Queries.andQuery(new Query[] { hiddenRealFile,
				mp3OrHtml });// 后缀为mp3或者为html 且 为隐藏纯文件 的查询

		QuerySession session = searcher.createQuerySession(bigQuery);
		session.execute();
		while (session.hasMore()) {
			FileData file = (FileData) session.next();
			System.out.printf("%-60s %20s%n", file.getFileName(), DateTools
					.timeToString(file.getLastModified(), Resolution.DAY));
		}

		// searcher.close();

		config.close();
	}
}
