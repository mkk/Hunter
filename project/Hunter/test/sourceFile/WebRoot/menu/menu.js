
$(document).ready(function(){
    $("#menuBar li>a").each(function(){
        $(this).fadeTo("fast", 0.6);
        var $subMenu = $(this).next().hide();
    });
    
    //遍历每个div 设置他们的触发事件
    $("#menuBar div").each(function(){
        var timeOutFun = null;//延迟事件
        var $subMenu = $(this);
        $(this).children().each(function(){//遍历div下的a,
            $(this).hover(function(){
                if (timeOutFun != null) {//如果有鼠标停留，则删除延迟事件
                    clearTimeout(timeOutFun);
                }
            }, function(){//如果有鼠标移出，则启动延迟事件                
                timeOutFun = setTimeout(function(){
                    $subMenu.slideUp("fast");
                }, 200);               
            });
        });
        
        $(this).siblings().each(function(){
            $(this).hover(function(){
                if (timeOutFun != null) {
                    clearTimeout(timeOutFun);
                }
            }, function(){
                timeOutFun = setTimeout(function(){
                    $subMenu.slideUp("fast");
                }, 200);                
            });
        });
    });
    $("#menuBar li>a").hover(function(){
        $(this).stop(false, true).animate({
            opacity: 1
        }, "fast");
    }, function(){
        $(this).stop(false, true).animate({
            opacity: 0.6
        }, "fast");
    });
    
    $("#menuBar li").hover(function(){
        $(this).children("div").slideDown();
    }, function(){
        //$(this).children("div").hide();
        //alert("li mouseOut");
    });
});

