
$(document).ready(function () {
	$("#menu > li > ul").hide().click(function (e) {
		e.stopPropagation();
	});
	$("#menu > li").toggle(function () {
		$(this).find("ul").stop(true, true).slideDown();
	}, function () {
		$(this).find("ul").stop(true, true).slideUp();
	});
	$("#info").tabs();
});

