package writeTest;

import java.io.IOException;

import annotationDemo.Book;

import com.hunter.desktop.file.RealFile;
import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.index.IndexException;
import com.hunter.lucene.util.index.Writable;

public class WriterTest {
	public static void main(String[] args) throws IOException,
			ConfigLoadException, ConfigInfoInvalideException, IndexException,
			LuceneException {
		Configuration configuraiton = Configuration.build();

		Writable writer = configuraiton.getOrBuildWriter(true);

		Book book = new Book();
		book.setName("moon");
		book
				.setDescription("可以通过许多途径自定义 Windows 操作系统和 Windows NT 4.0，使计算机更易于使用。从 Windows 95 问世开始，辅助功能就已经开始被内置于 Windows 和 Windows NT 中。这些功能适用于使用键盘或鼠标有困难、轻度视力障碍或有听力障碍的人士。可以在安装过程中安装这些功能，或者以后从 Windows 安装盘添加。有关安装和使用这些功能的信息，请在 Windows 帮助索引中查找“辅助功能”。内置于 Windows 和 Windows NT 的某些辅助功能可以通过辅助软件包文件添加到这些产品的早期版本和 Microsoft MS-DOS® 中。您可以下载这些文件或向 Microsoft 订购这些文件的磁盘。（有关详细内容，请参见本节后面的“辅助功能备注和下载实用方法”。）您还可以使用控制面板和其它内置功能调整 Windows 或 Windows NT 的外观和行为，以适应不同的视力和行动能力。其中包括调整颜色和显示尺寸大小、音量以及鼠标和键盘的行为。在 Windows 98 中，多数辅助功能设置可以在辅助功能向导或控制面板中实现。辅助功能向导可按残疾程度分别提供相应的功能，以方便不同人士根据需要自定义 Windows。辅助功能向导还使您得以将设置保存到文件，该文件可在其它计算机上使用。如果使用标准 QWERTY 布局的键盘有困难，那么 Dvorak 键盘布局更易于键入键盘上最常用的字符。有三种 Dvorak 布局：一种适用于双手操作的用户，一种适用于仅用左手键入的用户，另一种适用于仅用右手键入的用户。不需购买任何特殊设备即可使用这些功能。可用的特定功能以及这些功能是已经内置还是必须另外获得，取决于您正在使用的操作系统。有关所用操作系统中提供的辅助功能的完整文档信息，请获取下列文档。在 Microsoft Windows 95 Resource Kit、Microsoft Windows 98 Resource Kit 和 Microsoft Windows NT Resource Kit 中也记录了有关辅助功能的信息。");
		writer.update(book);

		RealFile file=new RealFile();
		
		 
		writer.update(file);
		
		writer.commit();

		writer.close();
	}
}
