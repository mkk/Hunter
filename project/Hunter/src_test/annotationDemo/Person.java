package annotationDemo;

import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;

import com.hunter.lucene.util.annotation.FieldGroup;
import com.hunter.lucene.util.annotation.Discriminator;
import com.hunter.lucene.util.annotation.Document;
import com.hunter.lucene.util.annotation.Fieldable;
import com.hunter.lucene.util.annotation.Undocument;
import com.hunter.lucene.util.annotation.Unfieldable;

@Document(analyzer = "standard", discrimination = @Discriminator(name = "person", value = "type"))
@Undocument(discrimination = @Discriminator(name = "person2", value = "type"))
public class Person {

	/**
	 * 我在其中指定了两个field ，即他在document中将添加两个field。 由于Fieldable 直接作用于
	 * 字段和方法的时候，只能使用一次,如果想使用多次 @Fieldable 则需要使用 @Data 将他们包裹起来 注意：此属性必须提供相应的
	 * getter 方法
	 */
	@FieldGroup( { @Fieldable(name = "name", index = Index.NOT_ANALYZED),
			@Fieldable(name = "analyzedName", store = Store.NO) })
	/**
	 * 指明，在搜索到的 document 中且与此  discrimination 匹配的 document 中的  name 为 "name"的 field 的值，赋给此属性 
	 * 注意：此属性必须提供相应的 setter 方法
	 */
	@Unfieldable(name = "name")
	private String name;
	private int age;
	@Fieldable(name="descripton")
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 此时 @Fieldabel 将作用于 方法的返回值
	 * 
	 * @return
	 */
	@Fieldable(name = "age", index = Index.NOT_ANALYZED)
	public int getAge() {
		return age;
	}

	/**
	 * 此时 @Unfieldable 将作用于方法的参数
	 * 
	 * @param age
	 */
	@Unfieldable(name = "age")
	public void setAge(int age) {
		this.age = age;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
