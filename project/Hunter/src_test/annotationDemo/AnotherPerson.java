package annotationDemo;

import com.hunter.lucene.util.annotation.Discriminator;
import com.hunter.lucene.util.annotation.LoadStrategy;
import com.hunter.lucene.util.annotation.Undocument;
import com.hunter.lucene.util.annotation.Unfieldable;

@Undocument(discrimination = @Discriminator(name = "person", value = "type"))
public class AnotherPerson {

	private String name;

	@Unfieldable(name = "age")
	private int age;

	/**
	 * 此属性可能会比较大，所以将它延迟加载
	 */
	@Unfieldable(name = "descripton", load = LoadStrategy.LAZY_LOAD)
	private String description;
	private String gender;

	public String getName() {
		return name;
	}

	@Unfieldable(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
