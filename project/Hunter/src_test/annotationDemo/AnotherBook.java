package annotationDemo;

import org.apache.lucene.document.Field.Index;

import com.hunter.lucene.util.annotation.Discriminator;
import com.hunter.lucene.util.annotation.Document;
import com.hunter.lucene.util.annotation.FieldGroup;
import com.hunter.lucene.util.annotation.Fieldable;
import com.hunter.lucene.util.annotation.UniqueField;

@Document(analyzer = "standard", discrimination = @Discriminator(name = "type", value = "anotherBook"))
public class AnotherBook {
	@UniqueField(name = "id")
	private int id;

	@FieldGroup(value = {
			@Fieldable(name = "name", index = Index.NOT_ANALYZED),
			@Fieldable(name = "analyzedName", index = Index.ANALYZED) })
	private String name;

	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@Fieldable(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

}
