package ObjectIO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ObjectIO {

	public static void main(String[] args) throws IOException,
			ClassNotFoundException {

		byte[] object = null;
		ByteArrayOutputStream oaos = new ByteArrayOutputStream();
		ObjectOutputStream ois = new ObjectOutputStream(oaos);
		Person p = new Person();
		p.setName("liuxing");

		ois.writeObject(p);

		object = oaos.toByteArray();

		System.out.println(object.length);
		ObjectInputStream oos = new ObjectInputStream(new ByteArrayInputStream(
				object));
		Object o = oos.readObject();

		Person person = (Person) o;
		System.out.println(person.getName());
	}

	public static class Person implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}
}
