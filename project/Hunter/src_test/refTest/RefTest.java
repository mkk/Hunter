package refTest;

import java.util.Map;
import java.util.WeakHashMap;

public class RefTest {
	public static void main(String[] args) throws InterruptedException {
		Map<Key, Value> weak = new WeakHashMap<Key, Value>();
		doNothing(weak);
		System.gc();
		Thread.sleep(10000);
		
		
		new Key("k");
		System.gc();
		System.out.println(weak.size());
	}

	public static void doNothing(Map<Key, Value> map) {
		map.put(new Key("key"), new Value("value"));
	}

	public static class Key {
		String name;

		public Key(String name) {
			super();
			this.name = name;
		}

		@Override
		protected void finalize() throws Throwable {
			super.finalize();
			System.out.println("i am  finalized "+name);
		}

	}

	public static class Value {
		private String name;

		public Value(String name) {
			super();
			this.name = name;
		}

		@Override
		protected void finalize() throws Throwable {
			super.finalize();
			System.out.println("i am  finalized "+name);
		}

	}
}
