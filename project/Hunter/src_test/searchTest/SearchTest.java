package searchTest;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

import annotationDemo.Book;

import com.hunter.lucene.util.config.ConfigLoadException;
import com.hunter.lucene.util.config.Configuration;
import com.hunter.lucene.util.config.LuceneException;
import com.hunter.lucene.util.config.validation.ConfigInfoInvalideException;
import com.hunter.lucene.util.search.QuerySession;
import com.hunter.lucene.util.search.SearchException;
import com.hunter.lucene.util.search.Searchable;

public class SearchTest {
	public static void main(String[] args) throws ConfigLoadException, ConfigInfoInvalideException, LuceneException, SearchException {
		Configuration configuraiton = Configuration.build();
		
		Searchable searcher=configuraiton.getOrBuildSearcher();
		Term term=new Term("name","sun");
		Query query=new TermQuery(term);
		
		QuerySession session=searcher.createQuerySession(query);
		
		session.execute();
		
		while(session.hasMore()) {
			Object book=session.next();
			System.out.println(book);
			if(book instanceof Book) {
				Book b=(Book)book;
				System.out.println(b.getName());
				System.out.println(b.getDescription());
				
			}
		}
		
		session.close();
		
		
		
	}
}
