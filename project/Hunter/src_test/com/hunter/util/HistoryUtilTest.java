package com.hunter.util;

import static junit.framework.Assert.*;

import org.junit.Test;

/**
 * 2010-12-30 下午10:31:50
 * 
 * @author mkk
 */
public class HistoryUtilTest {

	private HistoryUtil historyUtil = HistoryUtil.getHistoryUtil();

	@Test
	public void test() throws Exception {
		String pkey = historyUtil.getPreviousKeyword();
		assertNotNull(pkey);
		String nkey = historyUtil.getNextKeyword();
		assertNotNull(nkey);
		String keyWord = "mp3";
		historyUtil.saveKeyword(keyWord);
		pkey = historyUtil.getPreviousKeyword();
		assertEquals(keyWord, pkey);
		nkey = historyUtil.getNextKeyword();
		assertEquals(keyWord, nkey);
		assertEquals("", historyUtil.getNextKeyword());
	}
}
