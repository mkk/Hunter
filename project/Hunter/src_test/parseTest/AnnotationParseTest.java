package parseTest;

import annotationDemo.AnotherBook;
import annotationDemo.AnotherPerson;

import com.hunter.lucene.util.annotation.parse.DocumentAnnotationParser;
import com.hunter.lucene.util.annotation.parse.DocumentAnnotationWrapper;
import com.hunter.lucene.util.annotation.parse.UndocumentAnnotationParser;
import com.hunter.lucene.util.annotation.parse.UndocumentAnnotationWrapper;

public class AnnotationParseTest {
	public static void main(String[] args) {
		DocumentAnnotationParser parser = new DocumentAnnotationParser();
		DocumentAnnotationWrapper wrapper = parser.parse(AnotherBook.class);
		System.out.println(wrapper);

		UndocumentAnnotationParser uparser = new UndocumentAnnotationParser();
		UndocumentAnnotationWrapper wrapper2 = uparser.parse(AnotherPerson.class);

		System.out.println(wrapper2);
	}
}
