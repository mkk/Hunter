package annotationTest;

import java.lang.reflect.Field;

public class FieldSetterTest {
	public static void main(String[] args) {
		Class<Person> personClass = Person.class;

		Field nameField = null;
		try {
			nameField = personClass.getDeclaredField("name");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		Person person = new Person();
		try {
			nameField.set(person, "luxing");
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(person.getName());
	}
}
