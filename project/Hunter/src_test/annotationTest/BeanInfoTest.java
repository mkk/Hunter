package annotationTest;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import com.hunter.desktop.file.RealFile;

public class BeanInfoTest {
	public static void main(String[] args) {
		BeanInfo personBean = null;
		try {
			personBean = Introspector.getBeanInfo(RealFile.class);

		} catch (IntrospectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MethodDescriptor[] methodDescriptors = personBean
				.getMethodDescriptors();
		for (MethodDescriptor methodDescriptor : methodDescriptors) {
			System.out.println(methodDescriptor.getDisplayName());
		}

		PropertyDescriptor[] propertyDescripters = personBean
				.getPropertyDescriptors();
		for (PropertyDescriptor propertyDescriptor : propertyDescripters) {
			System.out.println(propertyDescriptor.getName());
			//System.out.println(propertyDescriptor.getDisplayName());
			Method setMethod=propertyDescriptor.getWriteMethod();
			System.out.println(setMethod);
			Method getMethod=propertyDescriptor.getReadMethod();
			System.out.println(getMethod);
		}
	}

}
