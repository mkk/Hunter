package annotationTest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class PersonTest {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws SecurityException, NoSuchMethodException {
		Class<Person> personClazz = Person.class;

		Field[] fields = personClazz.getDeclaredFields();
		for (@SuppressWarnings("unused") Field field : fields) {
//			Data attribute = field.getAnnotation(Data.class);
//			Fieldable[] fieldAnnotations = attribute.value();
//			for (Fieldable documentField : fieldAnnotations) {
//				String name = documentField.name();
//				System.out.println(name);
//				Store store = documentField.store();
//				System.out.println(store);
//				Index index = documentField.index();
//				System.out.println(index);
//				TermVector termVector = documentField.termVector();
//				System.out.println(termVector);
//			}
		}

		Method[] methods = personClazz.getDeclaredMethods();
		for (Method method : methods) {
			Class[] paras=method.getParameterTypes();
			for(Class para:paras) {
				System.out.println(para);
			}
			/*boolean isAnnotated = method.isAnnotationPresent(Data.class);
			if(isAnnotated) {
				Data dataAnnotation=method.getAnnotation(Data.class);
				Fieldable[] fieldAnnotations = dataAnnotation.value();
				for (Fieldable documentField : fieldAnnotations) {
					String name = documentField.name();
					System.out.println(name);
					Store store = documentField.store();
					System.out.println(store);
					Index index = documentField.index();
					System.out.println(index);
					TermVector termVector = documentField.termVector();
					System.out.println(termVector);
				}
			}*/
		}
		
		Method setAge=personClazz.getMethod("setAge", int.class);
		Class[] paras=setAge.getParameterTypes();
		for(Class para:paras) {
			System.out.println(para);
		}
	}
}
