package annotationTest;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

public class PropertyDescriptorTest {
	public static void main(String[] args) throws IntrospectionException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		
		PropertyDescriptor name=new PropertyDescriptor("name1", Person.class,"getName2",null);
		
		System.out.println(name.getReadMethod());
		
		System.out.println(name.getReadMethod().invoke(new Person()));
	}
}
