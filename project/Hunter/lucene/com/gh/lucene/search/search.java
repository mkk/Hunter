package com.gh.lucene.search;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;

import com.hunter.lucene.util.Lucenes;

public class search {
	public static void main(String[] args) throws IOException {
		Directory directory = Lucenes.getSimpleFileSystemDirectory("E:\\index");
		IndexReader indexReader = IndexReader.open(directory, true);
		IndexSearcher searcher = new IndexSearcher(indexReader);

		Term term = new Term("postfix", "pdf");

		TermQuery query = new TermQuery(term);

		TopDocs docs = searcher.search(query, 1000);

		for (ScoreDoc doc : docs.scoreDocs) {
			int id = doc.doc;

			long start = System.nanoTime();
			
				Document document = searcher.doc(id);

				System.out.println(document.getField("name").stringValue());
				//System.out.println(document.getField("description")
				//		.stringValue());
				System.out.println(document.getField("path").stringValue());
			

			long end = System.nanoTime();

			System.out.println(end - start);
		}

	}
}
