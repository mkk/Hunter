package com.gh.lucene.search;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.SetBasedFieldSelector;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;

import com.hunter.lucene.util.Lucenes;

public class FieldSelectorTest {
	public static void main(String[] args) throws IOException {
		Set<String> loadFields = new HashSet<String>();
		Set<String> lazyLoadFields = new HashSet<String>();
		lazyLoadFields.add("name");
		lazyLoadFields.add("person1");
		lazyLoadFields.add("description");
		FieldSelector fieldSelector = new SetBasedFieldSelector(loadFields,
				lazyLoadFields);

		Directory directory = Lucenes.getSimpleFileSystemDirectory("E:\\index");
		IndexReader indexReader = IndexReader.open(directory, true);
		IndexSearcher searcher = new IndexSearcher(indexReader);

		Term term = new Term("name", "sun");

		TermQuery query = new TermQuery(term);

		TopDocs docs = searcher.search(query, 100);

		for (ScoreDoc doc : docs.scoreDocs) {
			int id = doc.doc;

			long start=System.nanoTime();
			for (int i = 0; i < 1000; i++) {
				Document document = searcher.doc(id, fieldSelector);

				System.out.println(document.getFieldable("name").stringValue());
				//System.out.println(document.getFieldable("description")
				//		.stringValue());
				//System.out.println(document.getFieldable("person")
				//		.stringValue());
			}
			
			long end=System.nanoTime();
			
			System.out.println(end-start);
		}

	}
}
